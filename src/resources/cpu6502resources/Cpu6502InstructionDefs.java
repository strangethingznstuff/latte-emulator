package resources.cpu6502resources;

//Imports

public class Cpu6502InstructionDefs {


    //OPCODE DEFINITIONS

    static final int OPCODE_ADC_IMMEDIATE = 0x69;
    static final int OPCODE_ADC_ZERO_PAGE = 0x65;
    static final int OPCODE_ADC_ZERO_PAGE_INDEXED_X = 0x75;
    static final int OPCODE_ADC_ABSOLUTE = 0x6D;
    static final int OPCODE_ADC_ABSOLUTE_INDEXED_X = 0x7D;
    static final int OPCODE_ADC_ABSOLUTE_INDEXED_Y = 0x79;
    static final int OPCODE_ADC_INDEXED_INDIRECT = 0x61;
    static final int OPCODE_ADC_INDIRECT_INDEXED = 0x71;

    static final int OPCODE_AND_IMMEDIATE = 0x29;
    static final int OPCODE_AND_ZERO_PAGE = 0x25;
    static final int OPCODE_AND_ZERO_PAGE_INDEXED_X = 0x35;
    static final int OPCODE_AND_ABSOLUTE = 0x2D;
    static final int OPCODE_AND_ABSOLUTE_INDEXED_X = 0x3D;
    static final int OPCODE_AND_ABSOLUTE_INDEXED_Y = 0x39;
    static final int OPCODE_AND_INDEXED_INDIRECT = 0x21;
    static final int OPCODE_AND_INDIRECT_INDEXED = 0x31;

    static final int OPCODE_ASL_ACCUMULATOR = 0x0A;
    static final int OPCODE_ASL_ZERO_PAGE = 0x06;
    static final int OPCODE_ASL_ZERO_PAGE_INDEXED_X = 0x16;
    static final int OPCODE_ASL_ABSOLUTE = 0x0E;
    static final int OPCODE_ASL_ABSOLUTE_INDEXED_X = 0x1E;

    static final int OPCODE_BIT_ZERO_PAGE = 0x24;
    static final int OPCODE_BIT_ABSOLUTE = 0x2C;

    static final int OPCODE_BPL_RELATIVE = 0x10;

    static final int OPCODE_BMI_RELATIVE = 0x30;

    static final int OPCODE_BVC_RELATIVE = 0x50;

    static final int OPCODE_BVS_RELATIVE = 0x70;

    static final int OPCODE_BCC_RELATIVE = 0x90;

    static final int OPCODE_BCS_RELATIVE = 0xB0;

    static final int OPCODE_BNE_RELATIVE = 0xD0;

    static final int OPCODE_BEQ_RELATIVE = 0xF0;

    static final int OPCODE_BRK_IMPLIED = 0x00;

    static final int OPCODE_CMP_IMMEDIATE = 0xC9;
    static final int OPCODE_CMP_ZERO_PAGE = 0xC5;
    static final int OPCODE_CMP_ZERO_PAGE_INDEXED_X = 0xD5;
    static final int OPCODE_CMP_ABSOLUTE = 0xCD;
    static final int OPCODE_CMP_ABSOLUTE_INDEXED_X = 0xDD;
    static final int OPCODE_CMP_ABSOLUTE_INDEXED_Y = 0xD9;
    static final int OPCODE_CMP_INDEXED_INDIRECT = 0xC1;
    static final int OPCODE_CMP_INDIRECT_INDEXED = 0xD1;

    static final int OPCODE_CPX_IMMEDIATE = 0xE0;
    static final int OPCODE_CPX_ZERO_PAGE = 0xE4;
    static final int OPCODE_CPX_ABSOLUTE = 0xEC;

    static final int OPCODE_CPY_IMMEDIATE = 0xC0;
    static final int OPCODE_CPY_ZERO_PAGE = 0xC4;
    static final int OPCODE_CPY_ABSOLUTE = 0xCC;

    static final int OPCODE_DEC_ZERO_PAGE = 0xC6;
    static final int OPCODE_DEC_ZERO_PAGE_INDEXED_X = 0xD6;
    static final int OPCODE_DEC_ABSOLUTE = 0xCE;
    static final int OPCODE_DEC_ABSOLUTE_INDEXED_X = 0xDE;

    static final int OPCODE_EOR_IMMEDIATE = 0x49;
    static final int OPCODE_EOR_ZERO_PAGE = 0x45;
    static final int OPCODE_EOR_ZERO_PAGE_INDEXED_X = 0x55;
    static final int OPCODE_EOR_ABSOLUTE = 0x4D;
    static final int OPCODE_EOR_ABSOLUTE_INDEXED_X = 0x5D;
    static final int OPCODE_EOR_ABSOLUTE_INDEXED_Y = 0x59;
    static final int OPCODE_EOR_INDEXED_INDIRECT = 0x41;
    static final int OPCODE_EOR_INDIRECT_INDEXED = 0x51;

    static final int OPCODE_CLC_IMPLIED = 0x18;

    static final int OPCODE_SEC_IMPLIED = 0x38;

    static final int OPCODE_CLI_IMPLIED = 0x58;

    static final int OPCODE_SEI_IMPLIED = 0x78;

    static final int OPCODE_CLV_IMPLIED = 0xB8;

    static final int OPCODE_CLD_IMPLIED = 0xD8;

    static final int OPCODE_SED_IMPLIED = 0xF8;

    static final int OPCODE_INC_ZERO_PAGE = 0xE6;
    static final int OPCODE_INC_ZERO_PAGE_INDEXED_X = 0xF6;
    static final int OPCODE_INC_ABSOLUTE = 0xEE;
    static final int OPCODE_INC_ABSOLUTE_INDEXED_X = 0xFE;

    static final int OPCODE_JMP_ABSOLUTE = 0x4C;
    static final int OPCODE_JMP_INDIRECT = 0x6C;

    static final int OPCODE_JSR_ABSOLUTE = 0x20;

    static final int OPCODE_LDA_IMMEDIATE = 0xA9;
    static final int OPCODE_LDA_ZERO_PAGE = 0xA5;
    static final int OPCODE_LDA_ZERO_PAGE_INDEXED_X = 0xB5;
    static final int OPCODE_LDA_ABSOLUTE = 0xAD;
    static final int OPCODE_LDA_ABSOLUTE_INDEXED_X = 0xBD;
    static final int OPCODE_LDA_ABSOLUTE_INDEXED_Y = 0xB9;
    static final int OPCODE_LDA_INDEXED_INDIRECT = 0xA1;
    static final int OPCODE_LDA_INDIRECT_INDEXED = 0xB1;

    static final int OPCODE_LDX_IMMEDIATE = 0xA2;
    static final int OPCODE_LDX_ZERO_PAGE = 0xA6;
    static final int OPCODE_LDX_ZERO_PAGE_INDEXED_Y = 0xB6;
    static final int OPCODE_LDX_ABSOLUTE = 0xAE;
    static final int OPCODE_LDX_ABSOLUTE_INDEXED_Y = 0xBE;

    static final int OPCODE_LDY_IMMEDIATE = 0xA0;
    static final int OPCODE_LDY_ZERO_PAGE = 0xA4;
    static final int OPCODE_LDY_ZERO_PAGE_INDEXED_X = 0xB4;
    static final int OPCODE_LDY_ABSOLUTE = 0xAC;
    static final int OPCODE_LDY_ABSOLUTE_INDEXED_X = 0xBC;

    static final int OPCODE_LSR_ACCUMULATOR = 0x4A;
    static final int OPCODE_LSR_ZERO_PAGE = 0x46;
    static final int OPCODE_LSR_ZERO_PAGE_INDEXED_X = 0x56;
    static final int OPCODE_LSR_ABSOLUTE = 0x4E;
    static final int OPCODE_LSR_ABSOLUTE_INDEXED_X = 0x5E;

    static final int OPCODE_NOP_IMPLIED = 0xEA;

    static final int OPCODE_ORA_IMMEDIATE = 0x09;
    static final int OPCODE_ORA_ZERO_PAGE = 0x05;
    static final int OPCODE_ORA_ZERO_PAGE_INDEXED_X = 0x15;
    static final int OPCODE_ORA_ABSOLUTE = 0x0D;
    static final int OPCODE_ORA_ABSOLUTE_INDEXED_X = 0x1D;
    static final int OPCODE_ORA_ABSOLUTE_INDEXED_Y = 0x19;
    static final int OPCODE_ORA_INDEXED_INDIRECT = 0x01;
    static final int OPCODE_ORA_INDIRECT_INDEXED = 0x11;

    static final int OPCODE_TAX_IMPLIED = 0xAA;

    static final int OPCODE_TXA_IMPLIED = 0x8A;

    static final int OPCODE_DEX_IMPLIED = 0xCA;

    static final int OPCODE_INX_IMPLIED = 0xE8;

    static final int OPCODE_TAY_IMPLIED = 0xA8;

    static final int OPCODE_TYA_IMPLIED = 0x98;

    static final int OPCODE_DEY_IMPLIED = 0x88;

    static final int OPCODE_INY_IMPLIED = 0xC8;

    static final int OPCODE_ROL_ACCUMULATOR = 0x2A;
    static final int OPCODE_ROL_ZERO_PAGE = 0x26;
    static final int OPCODE_ROL_ZERO_PAGE_INDEXED_X = 0x36;
    static final int OPCODE_ROL_ABSOLUTE = 0x2E;
    static final int OPCODE_ROL_ABSOLUTE_INDEXED_X = 0x3E;

    static final int OPCODE_ROR_ACCUMULATOR = 0x6A;
    static final int OPCODE_ROR_ZERO_PAGE = 0x66;
    static final int OPCODE_ROR_ZERO_PAGE_INDEXED_X = 0x76;
    static final int OPCODE_ROR_ABSOLUTE = 0x6E;
    static final int OPCODE_ROR_ABSOLUTE_INDEXED_X = 0x7E;

    static final int OPCODE_RTI_IMPLIED = 0x40;

    static final int OPCODE_RTS_IMPLIED = 0x60;

    static final int OPCODE_SBC_IMMEDIATE = 0xE9;
    static final int OPCODE_SBC_ZERO_PAGE = 0xE5;
    static final int OPCODE_SBC_ZERO_PAGE_INDEXED_X = 0xF5;
    static final int OPCODE_SBC_ABSOLUTE = 0xED;
    static final int OPCODE_SBC_ABSOLUTE_INDEXED_X = 0xFD;
    static final int OPCODE_SBC_ABSOLUTE_INDEXED_Y = 0xF9;
    static final int OPCODE_SBC_INDEXED_INDIRECT = 0xE1;
    static final int OPCODE_SBC_INDIRECT_INDEXED = 0xF1;

    static final int OPCODE_STA_ZERO_PAGE = 0x85;
    static final int OPCODE_STA_ZERO_PAGE_INDEXED_X = 0x95;
    static final int OPCODE_STA_ABSOLUTE = 0x8D;
    static final int OPCODE_STA_ABSOLUTE_INDEXED_X = 0x9D;
    static final int OPCODE_STA_ABSOLUTE_INDEXED_Y = 0x99;
    static final int OPCODE_STA_INDEXED_INDIRECT = 0x81;
    static final int OPCODE_STA_INDIRECT_INDEXED = 0x91;

    static final int OPCODE_TXS_IMPLIED = 0x9A;

    static final int OPCODE_TSX_IMPLIED = 0xBA;

    static final int OPCODE_PHA_IMPLIED = 0x48;

    static final int OPCODE_PLA_IMPLIED = 0x68;

    static final int OPCODE_PHP_IMPLIED = 0x08;

    static final int OPCODE_PLP_IMPLIED = 0x28;

    static final int OPCODE_STX_ZERO_PAGE = 0x86;
    static final int OPCODE_STX_ZERO_PAGE_INDEXED_Y = 0x96;
    static final int OPCODE_STX_ABSOLUTE = 0x8E;
    
    static final int OPCODE_STY_ZERO_PAGE = 0x84;
    static final int OPCODE_STY_ZERO_PAGE_INDEXED_X = 0x94;
    static final int OPCODE_STY_ABSOLUTE = 0x8C;



    //LISTS OF COMPATIBLE ADDRESSING MODES
    static final Cpu6502.AddressingMode[] ADC_ADDRESSING_MODES = {
        Cpu6502.AddressingMode.IMMEDIATE,
        Cpu6502.AddressingMode.ZERO_PAGE,
        Cpu6502.AddressingMode.ZERO_PAGE_INDEXED_X,
        Cpu6502.AddressingMode.ABSOLUTE,
        Cpu6502.AddressingMode.ABSOLUTE_INDEXED_X,
        Cpu6502.AddressingMode.ABSOLUTE_INDEXED_Y,
        Cpu6502.AddressingMode.INDEXED_INDIRECT,
        Cpu6502.AddressingMode.INDIRECT_INDEXED
    };

    static final Cpu6502.AddressingMode[] AND_ADDRESSING_MODES = {
        Cpu6502.AddressingMode.IMMEDIATE,
        Cpu6502.AddressingMode.ZERO_PAGE,
        Cpu6502.AddressingMode.ZERO_PAGE_INDEXED_X,
        Cpu6502.AddressingMode.ABSOLUTE,
        Cpu6502.AddressingMode.ABSOLUTE_INDEXED_X,
        Cpu6502.AddressingMode.ABSOLUTE_INDEXED_Y,
        Cpu6502.AddressingMode.INDEXED_INDIRECT,
        Cpu6502.AddressingMode.INDIRECT_INDEXED
    };

    static final Cpu6502.AddressingMode[] ASL_ADDRESSING_MODES = {
        Cpu6502.AddressingMode.ACCUMULATOR,
        Cpu6502.AddressingMode.ZERO_PAGE,
        Cpu6502.AddressingMode.ZERO_PAGE_INDEXED_X,
        Cpu6502.AddressingMode.ABSOLUTE,
        Cpu6502.AddressingMode.ABSOLUTE_INDEXED_X,
    };

    static final Cpu6502.AddressingMode[] BIT_ADDRESSING_MODES = {
        Cpu6502.AddressingMode.ZERO_PAGE,
        Cpu6502.AddressingMode.ABSOLUTE
    };

    static final Cpu6502.AddressingMode[] BPL_ADDRESSING_MODES = {
        Cpu6502.AddressingMode.RELATIVE
    };

    static final Cpu6502.AddressingMode[] BMI_ADDRESSING_MODES = {
        Cpu6502.AddressingMode.RELATIVE
    };

    static final Cpu6502.AddressingMode[] BVC_ADDRESSING_MODES = {
        Cpu6502.AddressingMode.RELATIVE
    };

    static final Cpu6502.AddressingMode[] BVS_ADDRESSING_MODES = {
        Cpu6502.AddressingMode.RELATIVE
    };

    static final Cpu6502.AddressingMode[] BCC_ADDRESSING_MODES = {
        Cpu6502.AddressingMode.RELATIVE
    };

    static final Cpu6502.AddressingMode[] BCS_ADDRESSING_MODES = {
        Cpu6502.AddressingMode.RELATIVE
    };

    static final Cpu6502.AddressingMode[] BNE_ADDRESSING_MODES = {
        Cpu6502.AddressingMode.RELATIVE
    };

    static final Cpu6502.AddressingMode[] BEQ_ADDRESSING_MODES = {
        Cpu6502.AddressingMode.RELATIVE
    };

    static final Cpu6502.AddressingMode[] BRK_ADDRESSING_MODES = {
        Cpu6502.AddressingMode.IMPLIED
    };

    static final Cpu6502.AddressingMode[] CMP_ADDRESSING_MODES = {
        Cpu6502.AddressingMode.IMMEDIATE,
        Cpu6502.AddressingMode.ZERO_PAGE,
        Cpu6502.AddressingMode.ZERO_PAGE_INDEXED_X,
        Cpu6502.AddressingMode.ABSOLUTE,
        Cpu6502.AddressingMode.ABSOLUTE_INDEXED_X,
        Cpu6502.AddressingMode.ABSOLUTE_INDEXED_Y,
        Cpu6502.AddressingMode.INDEXED_INDIRECT,
        Cpu6502.AddressingMode.INDIRECT_INDEXED
    };

    static final Cpu6502.AddressingMode[] CPX_ADDRESSING_MODES = {
        Cpu6502.AddressingMode.IMMEDIATE,
        Cpu6502.AddressingMode.ZERO_PAGE,
        Cpu6502.AddressingMode.ABSOLUTE
    };

    static final Cpu6502.AddressingMode[] CPY_ADDRESSING_MODES = {
        Cpu6502.AddressingMode.IMMEDIATE,
        Cpu6502.AddressingMode.ZERO_PAGE,
        Cpu6502.AddressingMode.ABSOLUTE
    };

    static final Cpu6502.AddressingMode[] DEC_ADDRESSING_MODES = {
        Cpu6502.AddressingMode.ZERO_PAGE,
        Cpu6502.AddressingMode.ZERO_PAGE_INDEXED_X,
        Cpu6502.AddressingMode.ABSOLUTE,
        Cpu6502.AddressingMode.ABSOLUTE_INDEXED_X
    };

    static final Cpu6502.AddressingMode[] EOR_ADDRESSING_MODES = {
        Cpu6502.AddressingMode.IMMEDIATE,
        Cpu6502.AddressingMode.ZERO_PAGE,
        Cpu6502.AddressingMode.ZERO_PAGE_INDEXED_X,
        Cpu6502.AddressingMode.ABSOLUTE,
        Cpu6502.AddressingMode.ABSOLUTE_INDEXED_X,
        Cpu6502.AddressingMode.ABSOLUTE_INDEXED_Y,
        Cpu6502.AddressingMode.INDEXED_INDIRECT,
        Cpu6502.AddressingMode.INDIRECT_INDEXED
    };

    static final Cpu6502.AddressingMode[] CLC_ADDRESSING_MODES = {
        Cpu6502.AddressingMode.IMPLIED
    };

    static final Cpu6502.AddressingMode[] SEC_ADDRESSING_MODES = {
        Cpu6502.AddressingMode.IMPLIED
    };

    static final Cpu6502.AddressingMode[] CLI_ADDRESSING_MODES = {
        Cpu6502.AddressingMode.IMPLIED
    };

    static final Cpu6502.AddressingMode[] SEI_ADDRESSING_MODES = {
        Cpu6502.AddressingMode.IMPLIED
    };

    static final Cpu6502.AddressingMode[] CLV_ADDRESSING_MODES = {
        Cpu6502.AddressingMode.IMPLIED
    };

    static final Cpu6502.AddressingMode[] CLD_ADDRESSING_MODES = {
        Cpu6502.AddressingMode.IMPLIED
    };

    static final Cpu6502.AddressingMode[] SED_ADDRESSING_MODES = {
        Cpu6502.AddressingMode.IMPLIED
    };

    static final Cpu6502.AddressingMode[] INC_ADDRESSING_MODES = {
        Cpu6502.AddressingMode.ZERO_PAGE,
        Cpu6502.AddressingMode.ZERO_PAGE_INDEXED_X,
        Cpu6502.AddressingMode.ABSOLUTE,
        Cpu6502.AddressingMode.ABSOLUTE_INDEXED_X
    };

    static final Cpu6502.AddressingMode[] JMP_ADDRESSING_MODES = {
        Cpu6502.AddressingMode.ABSOLUTE,
        Cpu6502.AddressingMode.INDIRECT
    };

    static final Cpu6502.AddressingMode[] JSR_ADDRESSING_MODES = {
        Cpu6502.AddressingMode.ABSOLUTE
    };

    static final Cpu6502.AddressingMode[] LDA_ADDRESSING_MODES = {
        Cpu6502.AddressingMode.IMMEDIATE,
        Cpu6502.AddressingMode.ZERO_PAGE,
        Cpu6502.AddressingMode.ZERO_PAGE_INDEXED_X,
        Cpu6502.AddressingMode.ABSOLUTE,
        Cpu6502.AddressingMode.ABSOLUTE_INDEXED_X,
        Cpu6502.AddressingMode.ABSOLUTE_INDEXED_Y,
        Cpu6502.AddressingMode.INDEXED_INDIRECT,
        Cpu6502.AddressingMode.INDIRECT_INDEXED
    };

    static final Cpu6502.AddressingMode[] LDX_ADDRESSING_MODES = {
        Cpu6502.AddressingMode.IMMEDIATE,
        Cpu6502.AddressingMode.ZERO_PAGE,
        Cpu6502.AddressingMode.ZERO_PAGE_INDEXED_Y,
        Cpu6502.AddressingMode.ABSOLUTE,
        Cpu6502.AddressingMode.ABSOLUTE_INDEXED_Y
    };

    static final Cpu6502.AddressingMode[] LDY_ADDRESSING_MODES = {
        Cpu6502.AddressingMode.IMMEDIATE,
        Cpu6502.AddressingMode.ZERO_PAGE,
        Cpu6502.AddressingMode.ZERO_PAGE_INDEXED_X,
        Cpu6502.AddressingMode.ABSOLUTE,
        Cpu6502.AddressingMode.ABSOLUTE_INDEXED_X
    };

    static final Cpu6502.AddressingMode[] LSR_ADDRESSING_MODES = {
        Cpu6502.AddressingMode.ACCUMULATOR,
        Cpu6502.AddressingMode.ZERO_PAGE,
        Cpu6502.AddressingMode.ZERO_PAGE_INDEXED_X,
        Cpu6502.AddressingMode.ABSOLUTE,
        Cpu6502.AddressingMode.ABSOLUTE_INDEXED_X
    };

    static final Cpu6502.AddressingMode[] NOP_ADDRESSING_MODES = {
        Cpu6502.AddressingMode.IMPLIED
    };

    static final Cpu6502.AddressingMode[] ORA_ADDRESSING_MODES = {
        Cpu6502.AddressingMode.IMMEDIATE,
        Cpu6502.AddressingMode.ZERO_PAGE,
        Cpu6502.AddressingMode.ZERO_PAGE_INDEXED_X,
        Cpu6502.AddressingMode.ABSOLUTE,
        Cpu6502.AddressingMode.ABSOLUTE_INDEXED_X,
        Cpu6502.AddressingMode.ABSOLUTE_INDEXED_Y,
        Cpu6502.AddressingMode.INDEXED_INDIRECT,
        Cpu6502.AddressingMode.INDIRECT_INDEXED
    };

    static final Cpu6502.AddressingMode[] TAX_ADDRESSING_MODES = {
        Cpu6502.AddressingMode.IMPLIED
    };

    static final Cpu6502.AddressingMode[] 
    TXA_ADDRESSING_MODES = {
        Cpu6502.AddressingMode.IMPLIED
    };

    static final Cpu6502.AddressingMode[] 
    DEX_ADDRESSING_MODES = {
        Cpu6502.AddressingMode.IMPLIED
    };

    static final Cpu6502.AddressingMode[] 
    INX_ADDRESSING_MODES = {
        Cpu6502.AddressingMode.IMPLIED
    };

    static final Cpu6502.AddressingMode[] 
    TAY_ADDRESSING_MODES = {
        Cpu6502.AddressingMode.IMPLIED
    };

    static final Cpu6502.AddressingMode[] 
    TYA_ADDRESSING_MODES = {
        Cpu6502.AddressingMode.IMPLIED
    };

    static final Cpu6502.AddressingMode[] 
    DEY_ADDRESSING_MODES = {
        Cpu6502.AddressingMode.IMPLIED
    };

    static final Cpu6502.AddressingMode[] 
    INY_ADDRESSING_MODES = {
        Cpu6502.AddressingMode.IMPLIED
    };

    static final Cpu6502.AddressingMode[] ROL_ADDRESSING_MODES = {
        Cpu6502.AddressingMode.ACCUMULATOR,
        Cpu6502.AddressingMode.ZERO_PAGE,
        Cpu6502.AddressingMode.ZERO_PAGE_INDEXED_X,
        Cpu6502.AddressingMode.ABSOLUTE,
        Cpu6502.AddressingMode.ABSOLUTE_INDEXED_X
    };

    static final Cpu6502.AddressingMode[] ROR_ADDRESSING_MODES = {
        Cpu6502.AddressingMode.ACCUMULATOR,
        Cpu6502.AddressingMode.ZERO_PAGE,
        Cpu6502.AddressingMode.ZERO_PAGE_INDEXED_X,
        Cpu6502.AddressingMode.ABSOLUTE,
        Cpu6502.AddressingMode.ABSOLUTE_INDEXED_X
    };

    static final Cpu6502.AddressingMode[] 
    RTI_ADDRESSING_MODES = {
        Cpu6502.AddressingMode.IMPLIED
    };

    static final Cpu6502.AddressingMode[] 
    RTS_ADDRESSING_MODES = {
        Cpu6502.AddressingMode.IMPLIED
    };

    static final Cpu6502.AddressingMode[] SBC_ADDRESSING_MODES = {
        Cpu6502.AddressingMode.IMMEDIATE,
        Cpu6502.AddressingMode.ZERO_PAGE,
        Cpu6502.AddressingMode.ZERO_PAGE_INDEXED_X,
        Cpu6502.AddressingMode.ABSOLUTE,
        Cpu6502.AddressingMode.ABSOLUTE_INDEXED_X,
        Cpu6502.AddressingMode.ABSOLUTE_INDEXED_Y,
        Cpu6502.AddressingMode.INDEXED_INDIRECT,
        Cpu6502.AddressingMode.INDIRECT_INDEXED
    };

    static final Cpu6502.AddressingMode[] STA_ADDRESSING_MODES = {
        Cpu6502.AddressingMode.ZERO_PAGE,
        Cpu6502.AddressingMode.ZERO_PAGE_INDEXED_X,
        Cpu6502.AddressingMode.ABSOLUTE,
        Cpu6502.AddressingMode.ABSOLUTE_INDEXED_X,
        Cpu6502.AddressingMode.ABSOLUTE_INDEXED_Y,
        Cpu6502.AddressingMode.INDEXED_INDIRECT,
        Cpu6502.AddressingMode.INDIRECT_INDEXED
    };

    static final Cpu6502.AddressingMode[] 
    TXS_ADDRESSING_MODES = {
        Cpu6502.AddressingMode.IMPLIED
    };

    static final Cpu6502.AddressingMode[] 
    TSX_ADDRESSING_MODES = {
        Cpu6502.AddressingMode.IMPLIED
    };

    static final Cpu6502.AddressingMode[] 
    PHA_ADDRESSING_MODES = {
        Cpu6502.AddressingMode.IMPLIED
    };

    static final Cpu6502.AddressingMode[] 
    PLA_ADDRESSING_MODES = {
        Cpu6502.AddressingMode.IMPLIED
    };

    static final Cpu6502.AddressingMode[] 
    PHP_ADDRESSING_MODES = {
        Cpu6502.AddressingMode.IMPLIED
    };

    static final Cpu6502.AddressingMode[] 
    PLP_ADDRESSING_MODES = {
        Cpu6502.AddressingMode.IMPLIED
    };

    static final Cpu6502.AddressingMode[] STX_ADDRESSING_MODES = {
        Cpu6502.AddressingMode.ZERO_PAGE,
        Cpu6502.AddressingMode.ZERO_PAGE_INDEXED_Y,
        Cpu6502.AddressingMode.ABSOLUTE
    };

    static final Cpu6502.AddressingMode[] STY_ADDRESSING_MODES = {
        Cpu6502.AddressingMode.ZERO_PAGE,
        Cpu6502.AddressingMode.ZERO_PAGE_INDEXED_X,
        Cpu6502.AddressingMode.ABSOLUTE
    };

    public static String opcodeToString(int opcode) {
        switch (opcode) {
            case OPCODE_ADC_IMMEDIATE:
                return "ADC";
            case OPCODE_ADC_ZERO_PAGE:
                return "ADC";
            case OPCODE_ADC_ZERO_PAGE_INDEXED_X:
                return "ADC";
            case OPCODE_ADC_ABSOLUTE:
                return "ADC";
            case OPCODE_ADC_ABSOLUTE_INDEXED_X:
                return "ADC";
            case OPCODE_ADC_ABSOLUTE_INDEXED_Y:
                return "ADC";
            case OPCODE_ADC_INDEXED_INDIRECT:
                return "ADC";
            case OPCODE_ADC_INDIRECT_INDEXED:
                return "ADC";
            
            case OPCODE_AND_IMMEDIATE:
                return "AND";
            case OPCODE_AND_ZERO_PAGE:
                return "AND";
            case OPCODE_AND_ZERO_PAGE_INDEXED_X:
                return "AND";
            case OPCODE_AND_ABSOLUTE:
                return "AND";
            case OPCODE_AND_ABSOLUTE_INDEXED_X:
                return "AND";
            case OPCODE_AND_ABSOLUTE_INDEXED_Y:
                return "AND";
            case OPCODE_AND_INDEXED_INDIRECT:
                return "AND";
            case OPCODE_AND_INDIRECT_INDEXED:
                return "AND";

            case OPCODE_ASL_ACCUMULATOR:
                return "ASL";
            case OPCODE_ASL_ZERO_PAGE:
                return "ASL";
            case OPCODE_ASL_ZERO_PAGE_INDEXED_X:
                return "ASL";
            case OPCODE_ASL_ABSOLUTE:
                return "ASL";
            case OPCODE_ASL_ABSOLUTE_INDEXED_X:
                return "ASL";

            case OPCODE_BIT_ZERO_PAGE:
                return "BIT";
            case OPCODE_BIT_ABSOLUTE:
                return "BIT";

            case OPCODE_BPL_RELATIVE:
                return "BPL";

            case OPCODE_BMI_RELATIVE:
                return "BMI";

            case OPCODE_BVC_RELATIVE:
                return "BVC";

            case OPCODE_BVS_RELATIVE:
                return "BVS";

            case OPCODE_BCC_RELATIVE:
                return "BCC";

            case OPCODE_BCS_RELATIVE:
                return "BCS";

            case OPCODE_BNE_RELATIVE:
                return "BNE";

            case OPCODE_BEQ_RELATIVE:
                return "BEQ";

            case OPCODE_BRK_IMPLIED:
                return "BRK";

            case OPCODE_CMP_IMMEDIATE:
                return "CMP";
            case OPCODE_CMP_ZERO_PAGE:
                return "CMP";
            case OPCODE_CMP_ZERO_PAGE_INDEXED_X:
                return "CMP";
            case OPCODE_CMP_ABSOLUTE:
                return "CMP";
            case OPCODE_CMP_ABSOLUTE_INDEXED_X:
                return "CMP";
            case OPCODE_CMP_ABSOLUTE_INDEXED_Y:
                return "CMP";
            case OPCODE_CMP_INDEXED_INDIRECT:
                return "CMP";
            case OPCODE_CMP_INDIRECT_INDEXED:
                return "CMP";

            case OPCODE_CPX_IMMEDIATE:
                return "CPX";
            case OPCODE_CPX_ZERO_PAGE:
                return "CPX";
            case OPCODE_CPX_ABSOLUTE:
                return "CPX";

            case OPCODE_CPY_IMMEDIATE:
                return "CPY";
            case OPCODE_CPY_ZERO_PAGE:
                return "CPY";
            case OPCODE_CPY_ABSOLUTE:
                return "CPY";
            
            case OPCODE_DEC_ZERO_PAGE:
                return "DEC";
            case OPCODE_DEC_ZERO_PAGE_INDEXED_X:
                return "DEC";
            case OPCODE_DEC_ABSOLUTE:
                return "DEC";
            case OPCODE_DEC_ABSOLUTE_INDEXED_X:
                return "DEC";
            
            case OPCODE_EOR_IMMEDIATE:
                return "EOR";
            case OPCODE_EOR_ZERO_PAGE:
                return "EOR";
            case OPCODE_EOR_ZERO_PAGE_INDEXED_X:
                return "EOR";
            case OPCODE_EOR_ABSOLUTE:
                return "EOR";
            case OPCODE_EOR_ABSOLUTE_INDEXED_X:
                return "EOR";
            case OPCODE_EOR_ABSOLUTE_INDEXED_Y:
                return "EOR";
            case OPCODE_EOR_INDEXED_INDIRECT:
                return "EOR";
            case OPCODE_EOR_INDIRECT_INDEXED:
                return "EOR";

            case OPCODE_CLC_IMPLIED:
                return "CLC";
            
            case OPCODE_SEC_IMPLIED:
                return "SEC";

            case OPCODE_CLI_IMPLIED:
                return "CLI";

            case OPCODE_SEI_IMPLIED:
                return "SEI";

            case OPCODE_CLV_IMPLIED:
                return "CLV";

            case OPCODE_CLD_IMPLIED:
                return "CLD";

            case OPCODE_SED_IMPLIED:
                return "SED";
            
            case OPCODE_INC_ZERO_PAGE:
                return "INC";
            case OPCODE_INC_ZERO_PAGE_INDEXED_X:
                return "INC";
            case OPCODE_INC_ABSOLUTE:
                return "INC";
            case OPCODE_INC_ABSOLUTE_INDEXED_X:
                return "INC";

            case OPCODE_JMP_ABSOLUTE:
                return "JMP";
            case OPCODE_JMP_INDIRECT:
                return "JMP";

            case OPCODE_JSR_ABSOLUTE:
                return "JSR";

            case OPCODE_LDA_IMMEDIATE:
                return "LDA";
            case OPCODE_LDA_ZERO_PAGE:
                return "LDA";
            case OPCODE_LDA_ZERO_PAGE_INDEXED_X:
                return "LDA";
            case OPCODE_LDA_ABSOLUTE:
                return "LDA";
            case OPCODE_LDA_ABSOLUTE_INDEXED_X:
                return "LDA";
            case OPCODE_LDA_ABSOLUTE_INDEXED_Y:
                return "LDA";
            case OPCODE_LDA_INDEXED_INDIRECT:
                return "LDA";
            case OPCODE_LDA_INDIRECT_INDEXED:
                return "LDA";

            case OPCODE_LDX_IMMEDIATE:
                return "LDX";
            case OPCODE_LDX_ZERO_PAGE:
                return "LDX";
            case OPCODE_LDX_ZERO_PAGE_INDEXED_Y:
                return "LDX";
            case OPCODE_LDX_ABSOLUTE:
                return "LDX";
            case OPCODE_LDX_ABSOLUTE_INDEXED_Y:
                return "LDX";

            case OPCODE_LDY_IMMEDIATE:
                return "LDY";
            case OPCODE_LDY_ZERO_PAGE:
                return "LDY";
            case OPCODE_LDY_ZERO_PAGE_INDEXED_X:
                return "LDY";
            case OPCODE_LDY_ABSOLUTE:
                return "LDY";
            case OPCODE_LDY_ABSOLUTE_INDEXED_X:
                return "LDY";

            case OPCODE_LSR_ACCUMULATOR:
                return "LSR";
            case OPCODE_LSR_ZERO_PAGE:
                return "LSR";
            case OPCODE_LSR_ZERO_PAGE_INDEXED_X:
                return "LSR";
            case OPCODE_LSR_ABSOLUTE:
                return "LSR";
            case OPCODE_LSR_ABSOLUTE_INDEXED_X:
                return "LSR";

            case OPCODE_NOP_IMPLIED:
                return "NOP";
            
            case OPCODE_ORA_IMMEDIATE:
                return "ORA";
            case OPCODE_ORA_ZERO_PAGE:
                return "ORA";
            case OPCODE_ORA_ZERO_PAGE_INDEXED_X:
                return "ORA";
            case OPCODE_ORA_ABSOLUTE:
                return "ORA";
            case OPCODE_ORA_ABSOLUTE_INDEXED_X:
                return "ORA";
            case OPCODE_ORA_ABSOLUTE_INDEXED_Y:
                return "ORA";
            case OPCODE_ORA_INDEXED_INDIRECT:
                return "ORA";
            case OPCODE_ORA_INDIRECT_INDEXED:
                return "ORA";

            case OPCODE_TAX_IMPLIED:
                return "TAX";

            case OPCODE_TXA_IMPLIED:
                return "TXA";

            case OPCODE_DEX_IMPLIED:
                return "DEX";

            case OPCODE_INX_IMPLIED:
                return "INX";

            case OPCODE_TAY_IMPLIED:
                return "TAY";

            case OPCODE_TYA_IMPLIED:
                return "TYA";

            case OPCODE_DEY_IMPLIED:
                return "DEY";

            case OPCODE_INY_IMPLIED:
                return "INY";

            case OPCODE_ROL_ACCUMULATOR:
                return "ROL";
            case OPCODE_ROL_ZERO_PAGE:
                return "ROL";
            case OPCODE_ROL_ZERO_PAGE_INDEXED_X:
                return "ROL";
            case OPCODE_ROL_ABSOLUTE:
                return "ROL";
            case OPCODE_ROL_ABSOLUTE_INDEXED_X:
                return "ROL";

            case OPCODE_ROR_ACCUMULATOR:
                return "ROR";
            case OPCODE_ROR_ZERO_PAGE:
                return "ROR";
            case OPCODE_ROR_ZERO_PAGE_INDEXED_X:
                return "ROR";
            case OPCODE_ROR_ABSOLUTE:
                return "ROR";
            case OPCODE_ROR_ABSOLUTE_INDEXED_X:
                return "ROR";

            case OPCODE_RTI_IMPLIED:
                return "RTI";

            case OPCODE_RTS_IMPLIED:
                return "RTS";
            
            case OPCODE_SBC_IMMEDIATE:
                return "SBC";
            case OPCODE_SBC_ZERO_PAGE:
                return "SBC";
            case OPCODE_SBC_ZERO_PAGE_INDEXED_X:
                return "SBC";
            case OPCODE_SBC_ABSOLUTE:
                return "SBC";
            case OPCODE_SBC_ABSOLUTE_INDEXED_X:
                return "SBC";
            case OPCODE_SBC_ABSOLUTE_INDEXED_Y:
                return "SBC";
            case OPCODE_SBC_INDEXED_INDIRECT:
                return "SBC";
            case OPCODE_SBC_INDIRECT_INDEXED:
                return "SBC";

            case OPCODE_STA_ZERO_PAGE:
                return "STA";
            case OPCODE_STA_ZERO_PAGE_INDEXED_X:
                return "STA";
            case OPCODE_STA_ABSOLUTE:
                return "STA";
            case OPCODE_STA_ABSOLUTE_INDEXED_X:
                return "STA";
            case OPCODE_STA_ABSOLUTE_INDEXED_Y:
                return "STA";
            case OPCODE_STA_INDEXED_INDIRECT:
                return "STA";
            case OPCODE_STA_INDIRECT_INDEXED:
                return "STA";
            
            case OPCODE_TXS_IMPLIED:
                return "TXS";
            
            case OPCODE_TSX_IMPLIED:
                return "TSX";

            case OPCODE_PHA_IMPLIED:
                return "PHA";

            case OPCODE_PLA_IMPLIED:
                return "PLA";

            case OPCODE_PHP_IMPLIED:
                return "PHP";

            case OPCODE_PLP_IMPLIED:
                return "PLP";

            case OPCODE_STX_ZERO_PAGE:
                return "STX";
            case OPCODE_STX_ZERO_PAGE_INDEXED_Y:
                return "STX";
            case OPCODE_STX_ABSOLUTE:
                return "STX";

            case OPCODE_STY_ZERO_PAGE:
                return "STY";
            case OPCODE_STY_ZERO_PAGE_INDEXED_X:
                return "STY";
            case OPCODE_STY_ABSOLUTE:
                return "STY";
            


            default:
                return "???";
        }
    }

    public static int getInstructionLength(int opcode) {
        Cpu6502.AddressingMode addressingMode = getAddressingMode(opcode);
        switch (addressingMode) {
            case ACCUMULATOR:
                return 1;
            case IMPLIED:
                return 1;
            case IMMEDIATE:
                return 2;
            case ZERO_PAGE:
                return 2;
            case ZERO_PAGE_INDEXED_X:
                return 2;
            case ZERO_PAGE_INDEXED_Y:
                return 2;
            case ABSOLUTE:
                return 3;
            case ABSOLUTE_INDEXED_X:
                return 3;
            case ABSOLUTE_INDEXED_Y:
                return 3;
            case RELATIVE:
                return 2;
            case INDEXED_INDIRECT:
                return 2;
            case INDIRECT_INDEXED:
                return 2;
            case INDIRECT:
                return 3;
            case UNKNOWN:
                return -1;
            default:
                return -1;
        }
    }

    public static int getInstructionLength(Cpu6502.AddressingMode addressingMode) {
        switch (addressingMode) {
            case ACCUMULATOR:
                return 1;
            case IMPLIED:
                return 1;
            case IMMEDIATE:
                return 2;
            case ZERO_PAGE:
                return 2;
            case ZERO_PAGE_INDEXED_X:
                return 2;
            case ZERO_PAGE_INDEXED_Y:
                return 2;
            case ABSOLUTE:
                return 3;
            case ABSOLUTE_INDEXED_X:
                return 3;
            case ABSOLUTE_INDEXED_Y:
                return 3;
            case RELATIVE:
                return 2;
            case INDEXED_INDIRECT:
                return 2;
            case INDIRECT_INDEXED:
                return 2;
            case INDIRECT:
                return 3;
            case UNKNOWN:
                return -1;
            default:
                return -1;
        }
    }

    public static Cpu6502.AddressingMode getAddressingMode(int opcode) {
        switch (opcode) {
            case OPCODE_ADC_IMMEDIATE:
                return Cpu6502.AddressingMode.IMMEDIATE;
            case OPCODE_ADC_ZERO_PAGE:
                return Cpu6502.AddressingMode.ZERO_PAGE;
            case OPCODE_ADC_ZERO_PAGE_INDEXED_X:
                return Cpu6502.AddressingMode.ZERO_PAGE_INDEXED_X;
            case OPCODE_ADC_ABSOLUTE:
                return Cpu6502.AddressingMode.ABSOLUTE;
            case OPCODE_ADC_ABSOLUTE_INDEXED_X:
                return Cpu6502.AddressingMode.ABSOLUTE_INDEXED_X;
            case OPCODE_ADC_ABSOLUTE_INDEXED_Y:
                return Cpu6502.AddressingMode.ABSOLUTE_INDEXED_Y;
            case OPCODE_ADC_INDEXED_INDIRECT:
                return Cpu6502.AddressingMode.INDEXED_INDIRECT;
            case OPCODE_ADC_INDIRECT_INDEXED:
                return Cpu6502.AddressingMode.INDIRECT_INDEXED;
            
            case OPCODE_AND_IMMEDIATE:
                return Cpu6502.AddressingMode.IMMEDIATE;
            case OPCODE_AND_ZERO_PAGE:
                return Cpu6502.AddressingMode.ZERO_PAGE;
            case OPCODE_AND_ZERO_PAGE_INDEXED_X:
                return Cpu6502.AddressingMode.ZERO_PAGE_INDEXED_X;
            case OPCODE_AND_ABSOLUTE:
                return Cpu6502.AddressingMode.ABSOLUTE;
            case OPCODE_AND_ABSOLUTE_INDEXED_X:
                return Cpu6502.AddressingMode.ABSOLUTE_INDEXED_X;
            case OPCODE_AND_ABSOLUTE_INDEXED_Y:
                return Cpu6502.AddressingMode.ABSOLUTE_INDEXED_Y;
            case OPCODE_AND_INDEXED_INDIRECT:
                return Cpu6502.AddressingMode.INDEXED_INDIRECT;
            case OPCODE_AND_INDIRECT_INDEXED:
                return Cpu6502.AddressingMode.INDIRECT_INDEXED;

            case OPCODE_ASL_ACCUMULATOR:
                return Cpu6502.AddressingMode.ACCUMULATOR;
            case OPCODE_ASL_ZERO_PAGE:
                return Cpu6502.AddressingMode.ZERO_PAGE;
            case OPCODE_ASL_ZERO_PAGE_INDEXED_X:
                return Cpu6502.AddressingMode.ZERO_PAGE_INDEXED_X;
            case OPCODE_ASL_ABSOLUTE:
                return Cpu6502.AddressingMode.ABSOLUTE;
            case OPCODE_ASL_ABSOLUTE_INDEXED_X:
                return Cpu6502.AddressingMode.ABSOLUTE_INDEXED_X;

            case OPCODE_BIT_ZERO_PAGE:
                return Cpu6502.AddressingMode.ZERO_PAGE;
            case OPCODE_BIT_ABSOLUTE:
                return Cpu6502.AddressingMode.ABSOLUTE;

            case OPCODE_BPL_RELATIVE:
                return Cpu6502.AddressingMode.RELATIVE;

            case OPCODE_BMI_RELATIVE:
                return Cpu6502.AddressingMode.RELATIVE;

            case OPCODE_BVC_RELATIVE:
                return Cpu6502.AddressingMode.RELATIVE;

            case OPCODE_BVS_RELATIVE:
                return Cpu6502.AddressingMode.RELATIVE;

            case OPCODE_BCC_RELATIVE:
                return Cpu6502.AddressingMode.RELATIVE;

            case OPCODE_BCS_RELATIVE:
                return Cpu6502.AddressingMode.RELATIVE;

            case OPCODE_BNE_RELATIVE:
                return Cpu6502.AddressingMode.RELATIVE;

            case OPCODE_BEQ_RELATIVE:
                return Cpu6502.AddressingMode.RELATIVE;

            case OPCODE_BRK_IMPLIED:
                return Cpu6502.AddressingMode.IMPLIED;

            case OPCODE_CMP_IMMEDIATE:
                return Cpu6502.AddressingMode.IMMEDIATE;
            case OPCODE_CMP_ZERO_PAGE:
                return Cpu6502.AddressingMode.ZERO_PAGE;
            case OPCODE_CMP_ZERO_PAGE_INDEXED_X:
                return Cpu6502.AddressingMode.ZERO_PAGE_INDEXED_X;
            case OPCODE_CMP_ABSOLUTE:
                return Cpu6502.AddressingMode.ABSOLUTE;
            case OPCODE_CMP_ABSOLUTE_INDEXED_X:
                return Cpu6502.AddressingMode.ABSOLUTE_INDEXED_X;
            case OPCODE_CMP_ABSOLUTE_INDEXED_Y:
                return Cpu6502.AddressingMode.ABSOLUTE_INDEXED_Y;
            case OPCODE_CMP_INDEXED_INDIRECT:
                return Cpu6502.AddressingMode.INDEXED_INDIRECT;
            case OPCODE_CMP_INDIRECT_INDEXED:
                return Cpu6502.AddressingMode.INDIRECT_INDEXED;

            case OPCODE_CPX_IMMEDIATE:
                return Cpu6502.AddressingMode.IMMEDIATE;
            case OPCODE_CPX_ZERO_PAGE:
                return Cpu6502.AddressingMode.ZERO_PAGE;
            case OPCODE_CPX_ABSOLUTE:
                return Cpu6502.AddressingMode.ABSOLUTE;

            case OPCODE_CPY_IMMEDIATE:
                return Cpu6502.AddressingMode.IMMEDIATE;
            case OPCODE_CPY_ZERO_PAGE:
                return Cpu6502.AddressingMode.ZERO_PAGE;
            case OPCODE_CPY_ABSOLUTE:
                return Cpu6502.AddressingMode.ABSOLUTE;
            
            case OPCODE_DEC_ZERO_PAGE:
                return Cpu6502.AddressingMode.ZERO_PAGE;
            case OPCODE_DEC_ZERO_PAGE_INDEXED_X:
                return Cpu6502.AddressingMode.ZERO_PAGE_INDEXED_X;
            case OPCODE_DEC_ABSOLUTE:
                return Cpu6502.AddressingMode.ABSOLUTE;
            case OPCODE_DEC_ABSOLUTE_INDEXED_X:
                return Cpu6502.AddressingMode.ABSOLUTE_INDEXED_X;
            
            case OPCODE_EOR_IMMEDIATE:
                return Cpu6502.AddressingMode.IMMEDIATE;
            case OPCODE_EOR_ZERO_PAGE:
                return Cpu6502.AddressingMode.ZERO_PAGE;
            case OPCODE_EOR_ZERO_PAGE_INDEXED_X:
                return Cpu6502.AddressingMode.ZERO_PAGE_INDEXED_X;
            case OPCODE_EOR_ABSOLUTE:
                return Cpu6502.AddressingMode.ABSOLUTE;
            case OPCODE_EOR_ABSOLUTE_INDEXED_X:
                return Cpu6502.AddressingMode.ABSOLUTE_INDEXED_X;
            case OPCODE_EOR_ABSOLUTE_INDEXED_Y:
                return Cpu6502.AddressingMode.ABSOLUTE_INDEXED_Y;
            case OPCODE_EOR_INDEXED_INDIRECT:
                return Cpu6502.AddressingMode.INDEXED_INDIRECT;
            case OPCODE_EOR_INDIRECT_INDEXED:
                return Cpu6502.AddressingMode.INDIRECT_INDEXED;

            case OPCODE_CLC_IMPLIED:
                return Cpu6502.AddressingMode.IMPLIED;
            
            case OPCODE_SEC_IMPLIED:
                return Cpu6502.AddressingMode.IMPLIED;

            case OPCODE_CLI_IMPLIED:
                return Cpu6502.AddressingMode.IMPLIED;

            case OPCODE_SEI_IMPLIED:
                return Cpu6502.AddressingMode.IMPLIED;

            case OPCODE_CLV_IMPLIED:
                return Cpu6502.AddressingMode.IMPLIED;

            case OPCODE_CLD_IMPLIED:
                return Cpu6502.AddressingMode.IMPLIED;

            case OPCODE_SED_IMPLIED:
                return Cpu6502.AddressingMode.IMPLIED;
            
            case OPCODE_INC_ZERO_PAGE:
                return Cpu6502.AddressingMode.ZERO_PAGE;
            case OPCODE_INC_ZERO_PAGE_INDEXED_X:
                return Cpu6502.AddressingMode.ZERO_PAGE_INDEXED_X;
            case OPCODE_INC_ABSOLUTE:
                return Cpu6502.AddressingMode.ABSOLUTE;
            case OPCODE_INC_ABSOLUTE_INDEXED_X:
                return Cpu6502.AddressingMode.ABSOLUTE_INDEXED_X;

            case OPCODE_JMP_ABSOLUTE:
                return Cpu6502.AddressingMode.ABSOLUTE;
            case OPCODE_JMP_INDIRECT:
                return Cpu6502.AddressingMode.INDIRECT;

            case OPCODE_JSR_ABSOLUTE:
                return Cpu6502.AddressingMode.ABSOLUTE;

            case OPCODE_LDA_IMMEDIATE:
                return Cpu6502.AddressingMode.IMMEDIATE;
            case OPCODE_LDA_ZERO_PAGE:
                return Cpu6502.AddressingMode.ZERO_PAGE;
            case OPCODE_LDA_ZERO_PAGE_INDEXED_X:
                return Cpu6502.AddressingMode.ZERO_PAGE_INDEXED_X;
            case OPCODE_LDA_ABSOLUTE:
                return Cpu6502.AddressingMode.ABSOLUTE;
            case OPCODE_LDA_ABSOLUTE_INDEXED_X:
                return Cpu6502.AddressingMode.ABSOLUTE_INDEXED_X;
            case OPCODE_LDA_ABSOLUTE_INDEXED_Y:
                return Cpu6502.AddressingMode.ABSOLUTE_INDEXED_Y;
            case OPCODE_LDA_INDEXED_INDIRECT:
                return Cpu6502.AddressingMode.INDEXED_INDIRECT;
            case OPCODE_LDA_INDIRECT_INDEXED:
                return Cpu6502.AddressingMode.INDIRECT_INDEXED;

            case OPCODE_LDX_IMMEDIATE:
                return Cpu6502.AddressingMode.IMMEDIATE;
            case OPCODE_LDX_ZERO_PAGE:
                return Cpu6502.AddressingMode.ZERO_PAGE;
            case OPCODE_LDX_ZERO_PAGE_INDEXED_Y:
                return Cpu6502.AddressingMode.ZERO_PAGE_INDEXED_Y;
            case OPCODE_LDX_ABSOLUTE:
                return Cpu6502.AddressingMode.ABSOLUTE;
            case OPCODE_LDX_ABSOLUTE_INDEXED_Y:
                return Cpu6502.AddressingMode.ABSOLUTE_INDEXED_Y;

            case OPCODE_LDY_IMMEDIATE:
                return Cpu6502.AddressingMode.IMMEDIATE;
            case OPCODE_LDY_ZERO_PAGE:
                return Cpu6502.AddressingMode.ZERO_PAGE;
            case OPCODE_LDY_ZERO_PAGE_INDEXED_X:
                return Cpu6502.AddressingMode.ZERO_PAGE_INDEXED_X;
            case OPCODE_LDY_ABSOLUTE:
                return Cpu6502.AddressingMode.ABSOLUTE;
            case OPCODE_LDY_ABSOLUTE_INDEXED_X:
                return Cpu6502.AddressingMode.ABSOLUTE_INDEXED_X;

            case OPCODE_LSR_ACCUMULATOR:
                return Cpu6502.AddressingMode.ACCUMULATOR;
            case OPCODE_LSR_ZERO_PAGE:
                return Cpu6502.AddressingMode.ZERO_PAGE;
            case OPCODE_LSR_ZERO_PAGE_INDEXED_X:
                return Cpu6502.AddressingMode.ZERO_PAGE_INDEXED_X;
            case OPCODE_LSR_ABSOLUTE:
                return Cpu6502.AddressingMode.ABSOLUTE;
            case OPCODE_LSR_ABSOLUTE_INDEXED_X:
                return Cpu6502.AddressingMode.ABSOLUTE_INDEXED_X;

            case OPCODE_NOP_IMPLIED:
                return Cpu6502.AddressingMode.IMPLIED;
            
            case OPCODE_ORA_IMMEDIATE:
                return Cpu6502.AddressingMode.IMMEDIATE;
            case OPCODE_ORA_ZERO_PAGE:
                return Cpu6502.AddressingMode.ZERO_PAGE;
            case OPCODE_ORA_ZERO_PAGE_INDEXED_X:
                return Cpu6502.AddressingMode.ZERO_PAGE_INDEXED_X;
            case OPCODE_ORA_ABSOLUTE:
                return Cpu6502.AddressingMode.ABSOLUTE;
            case OPCODE_ORA_ABSOLUTE_INDEXED_X:
                return Cpu6502.AddressingMode.ABSOLUTE_INDEXED_X;
            case OPCODE_ORA_ABSOLUTE_INDEXED_Y:
                return Cpu6502.AddressingMode.ABSOLUTE_INDEXED_Y;
            case OPCODE_ORA_INDEXED_INDIRECT:
                return Cpu6502.AddressingMode.INDEXED_INDIRECT;
            case OPCODE_ORA_INDIRECT_INDEXED:
                return Cpu6502.AddressingMode.INDIRECT_INDEXED;

            case OPCODE_TAX_IMPLIED:
                return Cpu6502.AddressingMode.IMPLIED;

            case OPCODE_TXA_IMPLIED:
                return Cpu6502.AddressingMode.IMPLIED;

            case OPCODE_DEX_IMPLIED:
                return Cpu6502.AddressingMode.IMPLIED;

            case OPCODE_INX_IMPLIED:
                return Cpu6502.AddressingMode.IMPLIED;

            case OPCODE_TAY_IMPLIED:
                return Cpu6502.AddressingMode.IMPLIED;

            case OPCODE_TYA_IMPLIED:
                return Cpu6502.AddressingMode.IMPLIED;

            case OPCODE_DEY_IMPLIED:
                return Cpu6502.AddressingMode.IMPLIED;

            case OPCODE_INY_IMPLIED:
                return Cpu6502.AddressingMode.IMPLIED;

            case OPCODE_ROL_ACCUMULATOR:
                return Cpu6502.AddressingMode.ACCUMULATOR;
            case OPCODE_ROL_ZERO_PAGE:
                return Cpu6502.AddressingMode.ZERO_PAGE;
            case OPCODE_ROL_ZERO_PAGE_INDEXED_X:
                return Cpu6502.AddressingMode.ZERO_PAGE_INDEXED_X;
            case OPCODE_ROL_ABSOLUTE:
                return Cpu6502.AddressingMode.ABSOLUTE;
            case OPCODE_ROL_ABSOLUTE_INDEXED_X:
                return Cpu6502.AddressingMode.ABSOLUTE_INDEXED_X;

            case OPCODE_ROR_ACCUMULATOR:
                return Cpu6502.AddressingMode.ACCUMULATOR;
            case OPCODE_ROR_ZERO_PAGE:
                return Cpu6502.AddressingMode.ZERO_PAGE;
            case OPCODE_ROR_ZERO_PAGE_INDEXED_X:
                return Cpu6502.AddressingMode.ZERO_PAGE_INDEXED_X;
            case OPCODE_ROR_ABSOLUTE:
                return Cpu6502.AddressingMode.ABSOLUTE;
            case OPCODE_ROR_ABSOLUTE_INDEXED_X:
                return Cpu6502.AddressingMode.ABSOLUTE_INDEXED_X;

            case OPCODE_RTI_IMPLIED:
                return Cpu6502.AddressingMode.IMPLIED;

            case OPCODE_RTS_IMPLIED:
                return Cpu6502.AddressingMode.IMPLIED;
            
            case OPCODE_SBC_IMMEDIATE:
                return Cpu6502.AddressingMode.IMMEDIATE;
            case OPCODE_SBC_ZERO_PAGE:
                return Cpu6502.AddressingMode.ZERO_PAGE;
            case OPCODE_SBC_ZERO_PAGE_INDEXED_X:
                return Cpu6502.AddressingMode.ZERO_PAGE_INDEXED_X;
            case OPCODE_SBC_ABSOLUTE:
                return Cpu6502.AddressingMode.ABSOLUTE;
            case OPCODE_SBC_ABSOLUTE_INDEXED_X:
                return Cpu6502.AddressingMode.ABSOLUTE_INDEXED_X;
            case OPCODE_SBC_ABSOLUTE_INDEXED_Y:
                return Cpu6502.AddressingMode.ABSOLUTE_INDEXED_Y;
            case OPCODE_SBC_INDEXED_INDIRECT:
                return Cpu6502.AddressingMode.INDEXED_INDIRECT;
            case OPCODE_SBC_INDIRECT_INDEXED:
                return Cpu6502.AddressingMode.INDIRECT_INDEXED;

            case OPCODE_STA_ZERO_PAGE:
                return Cpu6502.AddressingMode.ZERO_PAGE;
            case OPCODE_STA_ZERO_PAGE_INDEXED_X:
                return Cpu6502.AddressingMode.ZERO_PAGE_INDEXED_X;
            case OPCODE_STA_ABSOLUTE:
                return Cpu6502.AddressingMode.ABSOLUTE;
            case OPCODE_STA_ABSOLUTE_INDEXED_X:
                return Cpu6502.AddressingMode.ABSOLUTE_INDEXED_X;
            case OPCODE_STA_ABSOLUTE_INDEXED_Y:
                return Cpu6502.AddressingMode.ABSOLUTE_INDEXED_Y;
            case OPCODE_STA_INDEXED_INDIRECT:
                return Cpu6502.AddressingMode.INDEXED_INDIRECT;
            case OPCODE_STA_INDIRECT_INDEXED:
                return Cpu6502.AddressingMode.INDIRECT_INDEXED;
            
            case OPCODE_TXS_IMPLIED:
                return Cpu6502.AddressingMode.IMPLIED;
            
            case OPCODE_TSX_IMPLIED:
                return Cpu6502.AddressingMode.IMPLIED;

            case OPCODE_PHA_IMPLIED:
                return Cpu6502.AddressingMode.IMPLIED;

            case OPCODE_PLA_IMPLIED:
                return Cpu6502.AddressingMode.IMPLIED;

            case OPCODE_PHP_IMPLIED:
                return Cpu6502.AddressingMode.IMPLIED;

            case OPCODE_PLP_IMPLIED:
                return Cpu6502.AddressingMode.IMPLIED;

            case OPCODE_STX_ZERO_PAGE:
                return Cpu6502.AddressingMode.ZERO_PAGE;
            case OPCODE_STX_ZERO_PAGE_INDEXED_Y:
                return Cpu6502.AddressingMode.ZERO_PAGE_INDEXED_Y;
            case OPCODE_STX_ABSOLUTE:
                return Cpu6502.AddressingMode.ABSOLUTE;

            case OPCODE_STY_ZERO_PAGE:
                return Cpu6502.AddressingMode.ZERO_PAGE;
            case OPCODE_STY_ZERO_PAGE_INDEXED_X:
                return Cpu6502.AddressingMode.ZERO_PAGE_INDEXED_X;
            case OPCODE_STY_ABSOLUTE:
                return Cpu6502.AddressingMode.ABSOLUTE;
            


            default:
                return Cpu6502.AddressingMode.UNKNOWN;
        }
    }

    public static int getOpcode(String instructionString, Cpu6502.AddressingMode addressingMode) {
        instructionString = instructionString.toUpperCase();
        switch (instructionString) {
            case "ADC":
                switch (addressingMode) {
                    case IMMEDIATE:
                        return OPCODE_ADC_IMMEDIATE;
                    case ZERO_PAGE:
                        return OPCODE_ADC_ZERO_PAGE;
                    case ZERO_PAGE_INDEXED_X:
                        return OPCODE_ADC_ZERO_PAGE_INDEXED_X;
                    case ABSOLUTE:
                        return OPCODE_ADC_ABSOLUTE;
                    case ABSOLUTE_INDEXED_X:
                        return OPCODE_ADC_ABSOLUTE_INDEXED_X;
                    case ABSOLUTE_INDEXED_Y:
                        return OPCODE_ADC_ABSOLUTE_INDEXED_Y;
                    case INDEXED_INDIRECT:
                        return OPCODE_ADC_INDEXED_INDIRECT;
                    case INDIRECT_INDEXED:
                        return OPCODE_ADC_INDIRECT_INDEXED;
                    default:
                    return -1;
                }
            case "AND":
                switch (addressingMode) {
                    case IMMEDIATE:
                        return OPCODE_AND_IMMEDIATE;
                    case ZERO_PAGE:
                        return OPCODE_AND_ZERO_PAGE;
                    case ZERO_PAGE_INDEXED_X:
                        return OPCODE_AND_ZERO_PAGE_INDEXED_X;
                    case ABSOLUTE:
                        return OPCODE_AND_ABSOLUTE;
                    case ABSOLUTE_INDEXED_X:
                        return OPCODE_AND_ABSOLUTE_INDEXED_X;
                    case ABSOLUTE_INDEXED_Y:
                        return OPCODE_AND_ABSOLUTE_INDEXED_Y;
                    case INDEXED_INDIRECT:
                        return OPCODE_AND_INDEXED_INDIRECT;
                    case INDIRECT_INDEXED:
                        return OPCODE_AND_INDIRECT_INDEXED;
                    default:
                        return -1;
                }
            case "ASL":
                switch (addressingMode) {
                    case ACCUMULATOR:
                        return OPCODE_ASL_ACCUMULATOR;
                    case ZERO_PAGE:
                        return OPCODE_ASL_ZERO_PAGE;
                    case ZERO_PAGE_INDEXED_X:
                        return OPCODE_ASL_ZERO_PAGE_INDEXED_X;
                    case ABSOLUTE:
                        return OPCODE_ASL_ABSOLUTE;
                    case ABSOLUTE_INDEXED_X:
                        return OPCODE_ASL_ABSOLUTE_INDEXED_X;
                    default:
                        return -1;
                }
            case "BIT":
                switch (addressingMode) {
                    case ZERO_PAGE:
                        return OPCODE_BIT_ZERO_PAGE;
                    case ABSOLUTE:
                        return OPCODE_BIT_ABSOLUTE;
                    default:
                        return -1;
                }
            case "BPL":
                switch (addressingMode) {
                    case RELATIVE:
                        return OPCODE_BPL_RELATIVE;
                    default:
                        return -1;
                }
            case "BMI":
                switch (addressingMode) {
                    case RELATIVE:
                        return OPCODE_BMI_RELATIVE;
                    default:
                        return -1;
                }
            case "BVC":
                switch (addressingMode) {
                    case RELATIVE:
                        return OPCODE_BVC_RELATIVE;
                    default:
                        return -1;
                }
            case "BVS":
                switch (addressingMode) {
                    case RELATIVE:
                        return OPCODE_BVS_RELATIVE;
                    default:
                        return -1;
                }
            case "BCC":
                switch (addressingMode) {
                    case RELATIVE:
                        return OPCODE_BCC_RELATIVE;
                    default:
                        return -1;
                }
            case "BCS":
                switch (addressingMode) {
                    case RELATIVE:
                        return OPCODE_BCS_RELATIVE;
                    default:
                        return -1;
                }
            case "BNE":
                switch (addressingMode) {
                    case RELATIVE:
                        return OPCODE_BNE_RELATIVE;
                    default:
                        return -1;
                }
            case "BEQ":
                switch (addressingMode) {
                    case RELATIVE:
                        return OPCODE_BEQ_RELATIVE;
                    default:
                        return -1;
                }
            case "BRK":
                switch (addressingMode) {
                    case IMPLIED:
                        return OPCODE_BRK_IMPLIED;
                    default:
                        return -1;
                }
            case "CMP":
                switch (addressingMode) {
                    case IMMEDIATE:
                        return OPCODE_CMP_IMMEDIATE;
                    case ZERO_PAGE:
                        return OPCODE_CMP_ZERO_PAGE;
                    case ZERO_PAGE_INDEXED_X:
                        return OPCODE_CMP_ZERO_PAGE_INDEXED_X;
                    case ABSOLUTE:
                        return OPCODE_CMP_ABSOLUTE;
                    case ABSOLUTE_INDEXED_X:
                        return OPCODE_CMP_ABSOLUTE_INDEXED_X;
                    case ABSOLUTE_INDEXED_Y:
                        return OPCODE_CMP_ABSOLUTE_INDEXED_Y;
                    case INDEXED_INDIRECT:
                        return OPCODE_CMP_INDEXED_INDIRECT;
                    case INDIRECT_INDEXED:
                        return OPCODE_CMP_INDIRECT_INDEXED;
                    default:
                        return -1;
                }
            case "CPX":
                switch (addressingMode) {
                    case IMMEDIATE:
                        return OPCODE_CPX_IMMEDIATE;
                    case ZERO_PAGE:
                        return OPCODE_CPX_ZERO_PAGE;
                    case ABSOLUTE:
                        return OPCODE_CPX_ABSOLUTE;
                    default:
                        return -1;
                }
            case "CPY":
                switch (addressingMode) {
                    case IMMEDIATE:
                        return OPCODE_CPY_IMMEDIATE;
                    case ZERO_PAGE:
                        return OPCODE_CPY_ZERO_PAGE;
                    case ABSOLUTE:
                        return OPCODE_CPY_ABSOLUTE;
                    default:
                        return -1;
                }
            case "DEC":
                switch (addressingMode) {
                    case ZERO_PAGE:
                        return OPCODE_DEC_ZERO_PAGE;
                    case ZERO_PAGE_INDEXED_X:
                        return OPCODE_DEC_ZERO_PAGE_INDEXED_X;
                    case ABSOLUTE:
                        return OPCODE_DEC_ABSOLUTE;
                    case ABSOLUTE_INDEXED_X:
                        return OPCODE_DEC_ABSOLUTE_INDEXED_X;
                    default:
                        return -1;
                }
            case "EOR":
                switch (addressingMode) {
                    case IMMEDIATE:
                        return OPCODE_EOR_IMMEDIATE;
                    case ZERO_PAGE:
                        return OPCODE_EOR_ZERO_PAGE;
                    case ZERO_PAGE_INDEXED_X:
                        return OPCODE_EOR_ZERO_PAGE_INDEXED_X;
                    case ABSOLUTE:
                        return OPCODE_EOR_ABSOLUTE;
                    case ABSOLUTE_INDEXED_X:
                        return OPCODE_EOR_ABSOLUTE_INDEXED_X;
                    case ABSOLUTE_INDEXED_Y:
                        return OPCODE_EOR_ABSOLUTE_INDEXED_Y;
                    case INDEXED_INDIRECT:
                        return OPCODE_EOR_INDEXED_INDIRECT;
                    case INDIRECT_INDEXED:
                        return OPCODE_EOR_INDIRECT_INDEXED;
                    default:
                        return -1;
                }
            case "CLC":
                switch (addressingMode) {
                    case IMPLIED:
                        return OPCODE_CLC_IMPLIED;
                    default:
                        return -1;
                }
            case "SEC":
                switch (addressingMode) {
                    case IMPLIED:
                        return OPCODE_SEC_IMPLIED;
                    default:
                        return -1;
                }
            case "CLI":
                switch (addressingMode) {
                    case IMPLIED:
                        return OPCODE_CLI_IMPLIED;
                    default:
                        return -1;
                }
            case "SEI":
                switch (addressingMode) {
                    case IMPLIED:
                        return OPCODE_SEI_IMPLIED;
                    default:
                        return -1;
                }
            case "CLV":
                switch (addressingMode) {
                    case IMPLIED:
                        return OPCODE_CLV_IMPLIED;
                    default:
                        return -1;
                }
            case "CLD":
                switch (addressingMode) {
                    case IMPLIED:
                        return OPCODE_CLD_IMPLIED;
                    default:
                        return -1;
                }
            case "SED":
                switch (addressingMode) {
                    case IMPLIED:
                        return OPCODE_SED_IMPLIED;
                    default:
                        return -1;
                }
            case "INC":
                switch (addressingMode) {
                    case ZERO_PAGE:
                        return OPCODE_INC_ZERO_PAGE;
                    case ZERO_PAGE_INDEXED_X:
                        return OPCODE_INC_ZERO_PAGE_INDEXED_X;
                    case ABSOLUTE:
                        return OPCODE_INC_ABSOLUTE;
                    case ABSOLUTE_INDEXED_X:
                        return OPCODE_INC_ABSOLUTE_INDEXED_X;
                    default:
                        return -1;
                }
            case "JMP":
                switch (addressingMode) {
                    case ABSOLUTE:
                        return OPCODE_JMP_ABSOLUTE;
                    case INDIRECT:
                        return OPCODE_JMP_INDIRECT;
                    default:
                        return -1;
                }
            case "JSR":
                switch (addressingMode) {
                    case ABSOLUTE:
                        return OPCODE_JSR_ABSOLUTE;
                    default:
                        return -1;
                }
            case "LDA":
                switch (addressingMode) {
                    case IMMEDIATE:
                        return OPCODE_LDA_IMMEDIATE;
                    case ZERO_PAGE:
                        return OPCODE_LDA_ZERO_PAGE;
                    case ZERO_PAGE_INDEXED_X:
                        return OPCODE_LDA_ZERO_PAGE_INDEXED_X;
                    case ABSOLUTE:
                        return OPCODE_LDA_ABSOLUTE;
                    case ABSOLUTE_INDEXED_X:
                        return OPCODE_LDA_ABSOLUTE_INDEXED_X;
                    case ABSOLUTE_INDEXED_Y:
                        return OPCODE_LDA_ABSOLUTE_INDEXED_Y;
                    case INDEXED_INDIRECT:
                        return OPCODE_LDA_INDEXED_INDIRECT;
                    case INDIRECT_INDEXED:
                        return OPCODE_LDA_INDIRECT_INDEXED;
                    default:
                        return -1;
                }
            case "LDX":
                switch (addressingMode) {
                    case IMMEDIATE:
                        return OPCODE_LDX_IMMEDIATE;
                    case ZERO_PAGE:
                        return OPCODE_LDX_ZERO_PAGE;
                    case ZERO_PAGE_INDEXED_Y:
                        return OPCODE_LDX_ZERO_PAGE_INDEXED_Y;
                    case ABSOLUTE:
                        return OPCODE_LDX_ABSOLUTE;
                    case ABSOLUTE_INDEXED_Y:
                        return OPCODE_LDX_ABSOLUTE_INDEXED_Y;
                    default:
                        return -1;
                }
            case "LDY":
                switch (addressingMode) {
                    case IMMEDIATE:
                        return OPCODE_LDY_IMMEDIATE;
                    case ZERO_PAGE:
                        return OPCODE_LDY_ZERO_PAGE;
                    case ZERO_PAGE_INDEXED_X:
                        return OPCODE_LDY_ZERO_PAGE_INDEXED_X;
                    case ABSOLUTE:
                        return OPCODE_LDY_ABSOLUTE;
                    case ABSOLUTE_INDEXED_X:
                        return OPCODE_LDY_ABSOLUTE_INDEXED_X;
                    default:
                        return -1;
                }
            case "LSR":
                switch (addressingMode) {
                    case ACCUMULATOR:
                        return OPCODE_LSR_ACCUMULATOR;
                    case ZERO_PAGE:
                        return OPCODE_LSR_ZERO_PAGE;
                    case ZERO_PAGE_INDEXED_X:
                        return OPCODE_LSR_ZERO_PAGE_INDEXED_X;
                    case ABSOLUTE:
                        return OPCODE_LSR_ABSOLUTE;
                    case ABSOLUTE_INDEXED_X:
                        return OPCODE_LSR_ABSOLUTE_INDEXED_X;
                    default:
                        return -1;
                }
            case "NOP":
                switch (addressingMode) {
                    case IMPLIED:
                        return OPCODE_NOP_IMPLIED;
                    default:
                        return -1;
                }
            case "ORA":
                switch (addressingMode) {
                    case IMMEDIATE:
                        return OPCODE_ORA_IMMEDIATE;
                    case ZERO_PAGE:
                        return OPCODE_ORA_ZERO_PAGE;
                    case ZERO_PAGE_INDEXED_X:
                        return OPCODE_ORA_ZERO_PAGE_INDEXED_X;
                    case ABSOLUTE:
                        return OPCODE_ORA_ABSOLUTE;
                    case ABSOLUTE_INDEXED_X:
                        return OPCODE_ORA_ABSOLUTE_INDEXED_X;
                    case ABSOLUTE_INDEXED_Y:
                        return OPCODE_ORA_ABSOLUTE_INDEXED_Y;
                    case INDEXED_INDIRECT:
                        return OPCODE_ORA_INDEXED_INDIRECT;
                    case INDIRECT_INDEXED:
                        return OPCODE_ORA_INDIRECT_INDEXED;
                    default:
                        return -1;
                }
            case "TAX":
                switch (addressingMode) {
                    case IMPLIED:
                        return OPCODE_TAX_IMPLIED;
                    default:
                        return -1;
                }
            case "TXA":
                switch (addressingMode) {
                    case IMPLIED:
                        return OPCODE_TXA_IMPLIED;
                    default:
                        return -1;
                }
            case "DEX":
                switch (addressingMode) {
                    case IMPLIED:
                        return OPCODE_DEX_IMPLIED;
                    default:
                        return -1;
                }
            case "INX":
                switch (addressingMode) {
                    case IMPLIED:
                        return OPCODE_INX_IMPLIED;
                    default:
                        return -1;
                }
            case "TAY":
                switch (addressingMode) {
                    case IMPLIED:
                        return OPCODE_TAY_IMPLIED;
                    default:
                        return -1;
                }
            case "TYA":
                switch (addressingMode) {
                    case IMPLIED:
                        return OPCODE_TYA_IMPLIED;
                    default:
                        return -1;
                }
            case "DEY":
                switch (addressingMode) {
                    case IMPLIED:
                        return OPCODE_DEY_IMPLIED;
                    default:
                        return -1;
                }
            case "INY":
                switch (addressingMode) {
                    case IMPLIED:
                        return OPCODE_INY_IMPLIED;
                    default:
                        return -1;
                }
            case "ROL":
                switch (addressingMode) {
                    case ACCUMULATOR:
                        return OPCODE_ROL_ACCUMULATOR;
                    case ZERO_PAGE:
                        return OPCODE_ROL_ZERO_PAGE;
                    case ZERO_PAGE_INDEXED_X:
                        return OPCODE_ROL_ZERO_PAGE_INDEXED_X;
                    case ABSOLUTE:
                        return OPCODE_ROL_ABSOLUTE;
                    case ABSOLUTE_INDEXED_X:
                        return OPCODE_ROL_ABSOLUTE_INDEXED_X;
                    default:
                        return -1;
                }
            case "ROR":
                switch (addressingMode) {
                    case ACCUMULATOR:
                        return OPCODE_ROR_ACCUMULATOR;
                    case ZERO_PAGE:
                        return OPCODE_ROR_ZERO_PAGE;
                    case ZERO_PAGE_INDEXED_X:
                        return OPCODE_ROR_ZERO_PAGE_INDEXED_X;
                    case ABSOLUTE:
                        return OPCODE_ROR_ABSOLUTE;
                    case ABSOLUTE_INDEXED_X:
                        return OPCODE_ROR_ABSOLUTE_INDEXED_X;
                    default:
                        return -1;
                }
            case "RTI":
                switch (addressingMode) {
                    case IMPLIED:
                        return OPCODE_RTI_IMPLIED;
                    default:
                        return -1;
                }
            case "RTS":
                switch (addressingMode) {
                    case IMPLIED:
                        return OPCODE_RTS_IMPLIED;
                    default:
                        return -1;
                }
            case "SBC":
                switch (addressingMode) {
                    case IMMEDIATE:
                        return OPCODE_SBC_IMMEDIATE;
                    case ZERO_PAGE:
                        return OPCODE_SBC_ZERO_PAGE;
                    case ZERO_PAGE_INDEXED_X:
                        return OPCODE_SBC_ZERO_PAGE_INDEXED_X;
                    case ABSOLUTE:
                        return OPCODE_SBC_ABSOLUTE;
                    case ABSOLUTE_INDEXED_X:
                        return OPCODE_SBC_ABSOLUTE_INDEXED_X;
                    case ABSOLUTE_INDEXED_Y:
                        return OPCODE_SBC_ABSOLUTE_INDEXED_Y;
                    case INDEXED_INDIRECT:
                        return OPCODE_SBC_INDEXED_INDIRECT;
                    case INDIRECT_INDEXED:
                        return OPCODE_SBC_INDIRECT_INDEXED;
                    default:
                        return -1;
                }
            case "STA":
                switch (addressingMode) {
                    case ZERO_PAGE:
                        return OPCODE_STA_ZERO_PAGE;
                    case ZERO_PAGE_INDEXED_X:
                        return OPCODE_STA_ZERO_PAGE_INDEXED_X;
                    case ABSOLUTE:
                        return OPCODE_STA_ABSOLUTE;
                    case ABSOLUTE_INDEXED_X:
                        return OPCODE_STA_ABSOLUTE_INDEXED_X;
                    case ABSOLUTE_INDEXED_Y:
                        return OPCODE_STA_ABSOLUTE_INDEXED_Y;
                    case INDEXED_INDIRECT:
                        return OPCODE_STA_INDEXED_INDIRECT;
                    case INDIRECT_INDEXED:
                        return OPCODE_STA_INDIRECT_INDEXED;
                    default:
                        return -1;
                }
            case "TXS":
                switch (addressingMode) {
                    case IMPLIED:
                        return OPCODE_TXS_IMPLIED;
                    default:
                        return -1;
                }
            case "TSX":
                switch (addressingMode) {
                    case IMPLIED:
                        return OPCODE_TSX_IMPLIED;
                    default:
                        return -1;
                }
            case "PHA":
                switch (addressingMode) {
                    case IMPLIED:
                        return OPCODE_PHA_IMPLIED;
                    default:
                        return -1;
                }
            case "PLA":
                switch (addressingMode) {
                    case IMPLIED:
                        return OPCODE_PLA_IMPLIED;
                    default:
                        return -1;
                }
            case "PHP":
                switch (addressingMode) {
                    case IMPLIED:
                        return OPCODE_PHP_IMPLIED;
                    default:
                        return -1;
                }
            case "PLP":
                switch (addressingMode) {
                    case IMPLIED:
                        return OPCODE_PLP_IMPLIED;
                    default:
                        return -1;
                }
            case "STX":
                switch (addressingMode) {
                    case ZERO_PAGE:
                        return OPCODE_STX_ZERO_PAGE;
                    case ZERO_PAGE_INDEXED_Y:
                        return OPCODE_STX_ZERO_PAGE_INDEXED_Y;
                    case ABSOLUTE:
                        return OPCODE_STX_ABSOLUTE;
                    default:
                        return -1;
                }
            case "STY":
                switch (addressingMode) {
                    case ZERO_PAGE:
                        return OPCODE_STY_ZERO_PAGE;
                    case ZERO_PAGE_INDEXED_X:
                        return OPCODE_STY_ZERO_PAGE_INDEXED_X;
                    case ABSOLUTE:
                        return OPCODE_STY_ABSOLUTE;
                    default:
                        return -1;
                }
            default:
                return -1;
        }
    }

    static boolean isModeCompatibleWithInstruction(String instructionString, Cpu6502.AddressingMode addressingMode) {
        Cpu6502.AddressingMode[] instructionModes = getAddressingModes(instructionString);
        for (int i=0; i<instructionModes.length; i++) {
            if (instructionModes[i]==addressingMode) return true;
        }
        return false;
    }

    static Cpu6502.AddressingMode[] getAddressingModes(String instructionString) {
        switch (instructionString) {
            case "ADC":
                return ADC_ADDRESSING_MODES;
            case "AND":
                return AND_ADDRESSING_MODES;
            case "ASL":
                return ASL_ADDRESSING_MODES;
            case "BIT":
                return BIT_ADDRESSING_MODES;
            case "BPL":
                return BPL_ADDRESSING_MODES;
            case "BMI":
                return BMI_ADDRESSING_MODES;
            case "BVC":
                return BVC_ADDRESSING_MODES;
            case "BVS":
                return BVS_ADDRESSING_MODES;
            case "BCC":
                return BCC_ADDRESSING_MODES;
            case "BCS":
                return BCS_ADDRESSING_MODES;
            case "BNE":
                return BNE_ADDRESSING_MODES;
            case "BEQ":
                return BEQ_ADDRESSING_MODES;
            case "BRK":
                return BRK_ADDRESSING_MODES;
            case "CMP":
                return CMP_ADDRESSING_MODES;
            case "CPX":
                return CPX_ADDRESSING_MODES;
            case "CPY":
                return CPY_ADDRESSING_MODES;
            case "DEC":
                return DEC_ADDRESSING_MODES;
            case "EOR":
                return EOR_ADDRESSING_MODES;
            case "CLC":
                return CLC_ADDRESSING_MODES;
            case "SEC":
                return SEC_ADDRESSING_MODES;
            case "CLI":
                return CLI_ADDRESSING_MODES;
            case "SEI":
                return SEI_ADDRESSING_MODES;
            case "CLV":
                return CLV_ADDRESSING_MODES;
            case "CLD":
                return CLD_ADDRESSING_MODES;
            case "SED":
                return SED_ADDRESSING_MODES;
            case "INC":
                return INC_ADDRESSING_MODES;
            case "JMP":
                return JMP_ADDRESSING_MODES;
            case "JSR":
                return JSR_ADDRESSING_MODES;
            case "LDA":
                return LDA_ADDRESSING_MODES;
            case "LDX":
                return LDX_ADDRESSING_MODES;
            case "LDY":
                return LDY_ADDRESSING_MODES;
            case "LSR":
                return LSR_ADDRESSING_MODES;
            case "NOP":
                return NOP_ADDRESSING_MODES;
            case "ORA":
                return ORA_ADDRESSING_MODES;
            case "TAX":
                return TAX_ADDRESSING_MODES;
            case "TXA":
                return TXA_ADDRESSING_MODES;
            case "DEX":
                return DEX_ADDRESSING_MODES;
            case "INX":
                return INX_ADDRESSING_MODES;
            case "TAY":
                return TAY_ADDRESSING_MODES;
            case "TYA":
                return TYA_ADDRESSING_MODES;
            case "DEY":
                return DEY_ADDRESSING_MODES;
            case "INY":
                return INY_ADDRESSING_MODES;
            case "ROL":
                return ROL_ADDRESSING_MODES;
            case "ROR":
                return ROR_ADDRESSING_MODES;
            case "RTI":
                return RTI_ADDRESSING_MODES;
            case "RTS":
                return RTS_ADDRESSING_MODES;
            case "SBC":
                return SBC_ADDRESSING_MODES;
            case "STA":
                return STA_ADDRESSING_MODES;
            case "TXS":
                return TXS_ADDRESSING_MODES;
            case "TSX":
                return TSX_ADDRESSING_MODES;
            case "PHA":
                return PHA_ADDRESSING_MODES;
            case "PLA":
                return PLA_ADDRESSING_MODES;
            case "PHP":
                return PHP_ADDRESSING_MODES;
            case "PLP":
                return PLP_ADDRESSING_MODES;
            case "STX":
                return STX_ADDRESSING_MODES;
            case "STY":
                return STY_ADDRESSING_MODES;
            default:
                return new Cpu6502.AddressingMode[] {};
        }
    }
}