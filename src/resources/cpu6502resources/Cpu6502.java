/*___________________________________________________
 *           MOS 6502 CPU EMULATOR FOR JAVA
 *___________________________________________________
 * 
 * Description:
 * The MOS 6502 is a microprocessor used in many early
 * home computers. This emulator was created as part
 * of the Latte Emulator (currently for this CPU).
 * 
 * Date Started: 1/20/2023
 * 
 *
 * Research mainly derived from:
 * https://www.nesdev.org/6502_cpu.txt
 * http://www.6502.org/tutorials/6502opcodes.html
 *
 */
package resources.cpu6502resources;
import resources.utils.LibBaseConversion;

public class Cpu6502 {

    /**********************************************************
    **                CPU VARIABLES & INFO                   **
    **         (declarations for important data)             **
    **                                                       **
    **********************************************************/

    //Owning system
    resources.systems.EmulatorSystem emulatorSystem;

    //Masks for status flags in status register
    public final static int STATUS_FLAG_C = 0b00000001;
    public final static int STATUS_FLAG_Z = 0b00000010;
    public final static int STATUS_FLAG_I = 0b00000100;
    public final static int STATUS_FLAG_D = 0b00001000;
    public final static int STATUS_FLAG_B = 0b00010000;
    public final static int STATUS_FLAG_U = 0b00100000;
    public final static int STATUS_FLAG_V = 0b01000000;
    public final static int STATUS_FLAG_N = 0b10000000;
    
    //Registers                               | Basic Description | Short Name: | Additional info
    public int registerProgramCounter = 0;     //16-bit register    (PC)
    public int registerStackPointer = 0;       //16-bit register    (SP/S)
    public int registerAccumulator = 0;        //8-bit register     (A)
    public int registerX = 0;                  //8-bit register     (X)
    public int registerY = 0;                  //8-bit register     (Y)
    public int registerStatus = STATUS_FLAG_U; //8-bit register     (P)          (U is always 1)

    //Operation Data
    public int effectiveAddress = 0;            //address being referenced by current instruction
    public int fixedEffectiveAddress = 0;       //fixed version of effective address for specific instructions
    public int workingValue = 0;                //The value loaded from the effective address
    public int operationResult = 0;             //Used to store operation results between cycles
    public boolean addressNeedsFix = false;     //Determines if an address must be fixed
    public boolean takeBranch = false;          //If a branch is being taken on a branch instruction

    //Cpu Behavior
    public boolean allowDecimalMode = true;  //Used to enable/disable decimal mode instructions (eg. for 2A03)
    
    //Vector Addresses
    public final int VectorNMI = 0xFFFA;        //Address where NMI Vector is located
    public final int VectorRESET = 0xFFFC;      //Address where RESET Vector is  located
    public final int VectorIRQ = 0xFFFE;        //Address where IRQ Vector is located



    /* 
    ** Addressing Modes:
    ** Addressing Modes are the specific modes of an instruction. Each instruction has one or
    ** more supported addressing modes, and each opcode (legal opcode) is associated with its
    ** own combination of instruction (eg. ADC) and Addressing Mode (eg. Immediate).
    ** Addressing Modes inform where/how an instruction reads and writes its data.
    */
    public enum AddressingMode {
        ACCUMULATOR,               
        //Accesses data in the accumulator
        IMPLIED,                   
        //Accesses data in a non-general way, implied by the opcode
        IMMEDIATE,                 
        //Accesses data immediately after the opcode
        ZERO_PAGE,                 
        //Accesses data at $0000 plus the following offset byte
        ZERO_PAGE_INDEXED_X,       
        //Accesses data at $0000 plus the following offset byte plus register X
        ZERO_PAGE_INDEXED_Y,       
        //Accesses data at $0000 plus the following offset byte plus register Y
        ABSOLUTE,                  
        //Accesses data at low endian address following instruction
        ABSOLUTE_INDEXED_X,        
        //Accesses data at low endian address following instruction plus register X
        ABSOLUTE_INDEXED_Y,       
        //Accesses data at low endian address following instruction plus register Y
        RELATIVE,                  
        //Accesses data immediately after the opcode representing an offset
        INDEXED_INDIRECT,          
        //Accesses data pointed to by the vector at $0000 plus the following byte and register X
        INDIRECT_INDEXED,          
        //Accesses data pointed to by adding register Y to the vector at $0000 plus the following byte
        INDIRECT,                  
        //Accesses data pointed to by low endian address following instruction
        UNKNOWN;                   
        //Unknown addressing mode
    }

    /*
    ** Addressing Sub-Modes:
    ** When combined with Addressing Modes, Addressing Sub-Modes determine instruction timing
    ** Many instructions share practically the same timing based on what they do, this is an
    ** abstraction to cut down on repeated behavior, and further organize functionality.
    */
    public enum AddressingSubMode {
        NO_READ_WRITE,             
        //This instruction does not read nor write to an address space specified by an operand 
        READ,                      
        //This instruction reads from, but does not write to an address space specified by an operand 
        READ_MODIFY_WRITE,         
        //This instruction reads and writes to an address space specified by an operand 
        WRITE                      
        //This instruction writes to, but does not read from an address space specified by an operand 
    }

    //I/O Handling
    int dataBusID;         //ID of the data bus the CPU connects to in the system
    int addressBusID;      //ID of the address bus the CPU connects to in the system
    int addressLine = 0;   //Address being output to the address line by the CPU
    int dataLine = 0;      //Data being output to the data line by the CPU

    final static boolean RwREAD = true;
    final static boolean RwWRITE = false;

    public boolean readWriteFlag = RwREAD;   //Tells System if the CPU is requesting a read or write to address space

    //Cycles
    public long cycleCount = 0;      //Counts the total cycles the CPU has ran since start/reset
    public int instructionCycle = 1; //The current cycle of the instruction being executed

    
    public int currentOpcode = 0x00; //Current Opcode the CPU is executing

    //Constructor
    public Cpu6502(resources.systems.EmulatorSystem system, int dataBusID, int addressBusID) {
        this.dataBusID = dataBusID;
        this.addressBusID = addressBusID;
        this.emulatorSystem = system;
    }

    //Update tick
    public void update() {

    }

    //Run CPU Cycle
    public void runCycle() {
        if(instructionCycle==1) {
            readWriteFlag = RwREAD;
            currentOpcode = addressRead(registerProgramCounter);
            registerProgramCounter++;
            instructionCycle++;
        }
        else {
            runInstructionCycle();
        }
        //debugPrintStatus();
        cycleCount++;
    }

    //
    private void runInstructionCycle() {
        switch (currentOpcode) {
            //Legal Instructions
            
            //Load Instructions (LDA, LDX, LDY)
            
            //LDA
            case Cpu6502InstructionDefs.OPCODE_LDA_IMMEDIATE:
                runCycleImmediate();
                break;
            case Cpu6502InstructionDefs.OPCODE_LDA_ABSOLUTE:
                runCycleAbsoluteRead();
                break;
            case Cpu6502InstructionDefs.OPCODE_LDA_ZERO_PAGE:
                runCycleZeroPageRead();
                break;
            case Cpu6502InstructionDefs.OPCODE_LDA_ZERO_PAGE_INDEXED_X:
                runCycleZeroPageIndexedXRead();
                break;
            case Cpu6502InstructionDefs.OPCODE_LDA_ABSOLUTE_INDEXED_X:
                runCycleAbsoluteIndexedXRead();
                break;
            case Cpu6502InstructionDefs.OPCODE_LDA_ABSOLUTE_INDEXED_Y:
                runCycleAbsoluteIndexedYRead();
                break;
            case Cpu6502InstructionDefs.OPCODE_LDA_INDEXED_INDIRECT:
                runCycleIndexedIndirectRead();
                break;
            case Cpu6502InstructionDefs.OPCODE_LDA_INDIRECT_INDEXED:
                runCycleIndirectIndexedRead();
                break;
            //LDX
            case Cpu6502InstructionDefs.OPCODE_LDX_IMMEDIATE:
                runCycleImmediate();
                break;
            case Cpu6502InstructionDefs.OPCODE_LDX_ABSOLUTE:
                runCycleAbsoluteRead();
                break;
            case Cpu6502InstructionDefs.OPCODE_LDX_ZERO_PAGE:
                runCycleZeroPageRead();
                break;
            case Cpu6502InstructionDefs.OPCODE_LDX_ZERO_PAGE_INDEXED_Y:
                runCycleZeroPageIndexedYRead();
                break;
            case Cpu6502InstructionDefs.OPCODE_LDX_ABSOLUTE_INDEXED_Y:
                runCycleAbsoluteIndexedYRead();
                break;
            //LDY
            case Cpu6502InstructionDefs.OPCODE_LDY_IMMEDIATE:
                runCycleImmediate();
                break;
            case Cpu6502InstructionDefs.OPCODE_LDY_ABSOLUTE:
                runCycleAbsoluteRead();
                break;
            case Cpu6502InstructionDefs.OPCODE_LDY_ZERO_PAGE:
                runCycleZeroPageRead();
                break;
            case Cpu6502InstructionDefs.OPCODE_LDY_ZERO_PAGE_INDEXED_X:
                runCycleZeroPageIndexedXRead();
                break;
            case Cpu6502InstructionDefs.OPCODE_LDY_ABSOLUTE_INDEXED_X:
                runCycleAbsoluteIndexedXRead();
                break;
            //Store Instructions (STA, STX, STY)
            
            //STA
            case Cpu6502InstructionDefs.OPCODE_STA_ABSOLUTE:
                runCycleAbsoluteWrite();
                break;
            case Cpu6502InstructionDefs.OPCODE_STA_ZERO_PAGE:
                runCycleZeroPageWrite();
                break;
            case Cpu6502InstructionDefs.OPCODE_STA_ZERO_PAGE_INDEXED_X:
                runCycleZeroPageIndexedXWrite();
                break;
            case Cpu6502InstructionDefs.OPCODE_STA_ABSOLUTE_INDEXED_X:
                runCycleAbsoluteIndexedXWrite();
                break;
            case Cpu6502InstructionDefs.OPCODE_STA_ABSOLUTE_INDEXED_Y:
                runCycleAbsoluteIndexedYWrite();
                break;
            case Cpu6502InstructionDefs.OPCODE_STA_INDEXED_INDIRECT:
                runCycleIndexedIndirectWrite();
                break;
            case Cpu6502InstructionDefs.OPCODE_STA_INDIRECT_INDEXED:
                runCycleIndirectIndexedWrite();
                break;
            //STX
            case Cpu6502InstructionDefs.OPCODE_STX_ABSOLUTE:
                runCycleAbsoluteWrite();
                break;
            case Cpu6502InstructionDefs.OPCODE_STX_ZERO_PAGE:
                runCycleZeroPageWrite();
                break;
            case Cpu6502InstructionDefs.OPCODE_STX_ZERO_PAGE_INDEXED_Y:
                runCycleZeroPageIndexedYWrite();
                break;
            //STY
            case Cpu6502InstructionDefs.OPCODE_STY_ABSOLUTE:
                runCycleAbsoluteWrite();
                break;
            case Cpu6502InstructionDefs.OPCODE_STY_ZERO_PAGE:
                runCycleZeroPageWrite();
                break;
            case Cpu6502InstructionDefs.OPCODE_STY_ZERO_PAGE_INDEXED_X:
                runCycleZeroPageIndexedXWrite();
                break;
            //Incremental and Decremental Instructions (INC, DEC)
            
            //INC
            case Cpu6502InstructionDefs.OPCODE_INC_ABSOLUTE:
                runCycleAbsoluteReadModifyWrite();
                break;
            case Cpu6502InstructionDefs.OPCODE_INC_ZERO_PAGE:
                runCycleZeroPageReadModifyWrite();
                break;
            case Cpu6502InstructionDefs.OPCODE_INC_ZERO_PAGE_INDEXED_X:
                runCycleZeroPageIndexedXReadModifyWrite();
                break;
            case Cpu6502InstructionDefs.OPCODE_INC_ABSOLUTE_INDEXED_X:
                runCycleAbsoluteIndexedXReadModifyWrite();
                break;
            //DEC
            case Cpu6502InstructionDefs.OPCODE_DEC_ABSOLUTE:
                runCycleAbsoluteReadModifyWrite();
                break;
            case Cpu6502InstructionDefs.OPCODE_DEC_ZERO_PAGE:
                runCycleZeroPageReadModifyWrite();
                break;
            case Cpu6502InstructionDefs.OPCODE_DEC_ZERO_PAGE_INDEXED_X:
                runCycleZeroPageIndexedXReadModifyWrite();
                break;
            case Cpu6502InstructionDefs.OPCODE_DEC_ABSOLUTE_INDEXED_X:
                runCycleAbsoluteIndexedXReadModifyWrite();
                break;
            //Arithmetic Instructions (math) (ADC, SBC, AND, ORA, EOR, ASL, LSR, ROL, ROR, BIT)
            
            //ADC
            case Cpu6502InstructionDefs.OPCODE_ADC_IMMEDIATE:
                runCycleImmediate();
                break;
            case Cpu6502InstructionDefs.OPCODE_ADC_ABSOLUTE:
                runCycleAbsoluteRead();
                break;
            case Cpu6502InstructionDefs.OPCODE_ADC_ZERO_PAGE:
                runCycleZeroPageRead();
                break;
            case Cpu6502InstructionDefs.OPCODE_ADC_ZERO_PAGE_INDEXED_X:
                runCycleZeroPageIndexedXRead();
                break;
            case Cpu6502InstructionDefs.OPCODE_ADC_ABSOLUTE_INDEXED_X:
                runCycleAbsoluteIndexedXRead();
                break;
            case Cpu6502InstructionDefs.OPCODE_ADC_ABSOLUTE_INDEXED_Y:
                runCycleAbsoluteIndexedYRead();
                break;
            case Cpu6502InstructionDefs.OPCODE_ADC_INDEXED_INDIRECT:
                runCycleIndexedIndirectRead();
                break;
            case Cpu6502InstructionDefs.OPCODE_ADC_INDIRECT_INDEXED:
                runCycleIndirectIndexedRead();
                break;  
            //SBC
            case Cpu6502InstructionDefs.OPCODE_SBC_IMMEDIATE:
                runCycleImmediate();
                break;
            case Cpu6502InstructionDefs.OPCODE_SBC_ABSOLUTE:
                runCycleAbsoluteRead();
                break;
            case Cpu6502InstructionDefs.OPCODE_SBC_ZERO_PAGE:
                runCycleZeroPageRead();
                break;
            case Cpu6502InstructionDefs.OPCODE_SBC_ZERO_PAGE_INDEXED_X:
                runCycleZeroPageIndexedXRead();
                break;
            case Cpu6502InstructionDefs.OPCODE_SBC_ABSOLUTE_INDEXED_X:
                runCycleAbsoluteIndexedXRead();
                break;
            case Cpu6502InstructionDefs.OPCODE_SBC_ABSOLUTE_INDEXED_Y:
                runCycleAbsoluteIndexedYRead();
                break;
            case Cpu6502InstructionDefs.OPCODE_SBC_INDEXED_INDIRECT:
                runCycleIndexedIndirectRead();
                break;
            case Cpu6502InstructionDefs.OPCODE_SBC_INDIRECT_INDEXED:
                runCycleIndirectIndexedRead();
                break;
            //AND
            case Cpu6502InstructionDefs.OPCODE_AND_IMMEDIATE:
                runCycleImmediate();
                break;
            case Cpu6502InstructionDefs.OPCODE_AND_ABSOLUTE:
                runCycleAbsoluteRead();
                break;
            case Cpu6502InstructionDefs.OPCODE_AND_ZERO_PAGE:
                runCycleZeroPageRead();
                break;
            case Cpu6502InstructionDefs.OPCODE_AND_ZERO_PAGE_INDEXED_X:
                runCycleZeroPageIndexedXRead();
                break;
            case Cpu6502InstructionDefs.OPCODE_AND_ABSOLUTE_INDEXED_X:
                runCycleAbsoluteIndexedXRead();
                break;
            case Cpu6502InstructionDefs.OPCODE_AND_ABSOLUTE_INDEXED_Y:
                runCycleAbsoluteIndexedYRead();
                break;
            case Cpu6502InstructionDefs.OPCODE_AND_INDEXED_INDIRECT:
                runCycleIndexedIndirectRead();
                break;
            case Cpu6502InstructionDefs.OPCODE_AND_INDIRECT_INDEXED:
                runCycleIndirectIndexedRead();
                break;
            //ORA
            case Cpu6502InstructionDefs.OPCODE_ORA_IMMEDIATE:
                runCycleImmediate();
                break;
            case Cpu6502InstructionDefs.OPCODE_ORA_ABSOLUTE:
                runCycleAbsoluteRead();
                break;
            case Cpu6502InstructionDefs.OPCODE_ORA_ZERO_PAGE:
                runCycleZeroPageRead();
                break;
            case Cpu6502InstructionDefs.OPCODE_ORA_ZERO_PAGE_INDEXED_X:
                runCycleZeroPageIndexedXRead();
                break;
            case Cpu6502InstructionDefs.OPCODE_ORA_ABSOLUTE_INDEXED_X:
                runCycleAbsoluteIndexedXRead();
                break;
            case Cpu6502InstructionDefs.OPCODE_ORA_ABSOLUTE_INDEXED_Y:
                runCycleAbsoluteIndexedYRead();
                break;
            case Cpu6502InstructionDefs.OPCODE_ORA_INDEXED_INDIRECT:
                runCycleIndexedIndirectRead();
                break;
            case Cpu6502InstructionDefs.OPCODE_ORA_INDIRECT_INDEXED:
                runCycleIndirectIndexedRead();
                break;
            //EOR
            case Cpu6502InstructionDefs.OPCODE_EOR_IMMEDIATE:
                runCycleImmediate();
                break;
            case Cpu6502InstructionDefs.OPCODE_EOR_ABSOLUTE:
                runCycleAbsoluteRead();
                break;
            case Cpu6502InstructionDefs.OPCODE_EOR_ZERO_PAGE:
                runCycleZeroPageRead();
                break;
            case Cpu6502InstructionDefs.OPCODE_EOR_ZERO_PAGE_INDEXED_X:
                runCycleZeroPageIndexedXRead();
                break;
            case Cpu6502InstructionDefs.OPCODE_EOR_ABSOLUTE_INDEXED_X:
                runCycleAbsoluteIndexedXRead();
                break;
            case Cpu6502InstructionDefs.OPCODE_EOR_ABSOLUTE_INDEXED_Y:
                runCycleAbsoluteIndexedYRead();
                break;
            case Cpu6502InstructionDefs.OPCODE_EOR_INDEXED_INDIRECT:
                runCycleIndexedIndirectRead();
                break;
            case Cpu6502InstructionDefs.OPCODE_EOR_INDIRECT_INDEXED:
                runCycleIndirectIndexedRead();
                break;
            //ASL
            case Cpu6502InstructionDefs.OPCODE_ASL_ACCUMULATOR:
                runCycleImplied();
                break;
            case Cpu6502InstructionDefs.OPCODE_ASL_ABSOLUTE:
                runCycleAbsoluteReadModifyWrite();
                break;
            case Cpu6502InstructionDefs.OPCODE_ASL_ZERO_PAGE:
                runCycleZeroPageReadModifyWrite();
                break;
            case Cpu6502InstructionDefs.OPCODE_ASL_ZERO_PAGE_INDEXED_X:
                runCycleZeroPageIndexedXReadModifyWrite();
                break;
            case Cpu6502InstructionDefs.OPCODE_ASL_ABSOLUTE_INDEXED_X:
                runCycleAbsoluteIndexedXReadModifyWrite();
                break;
            //LSR
            case Cpu6502InstructionDefs.OPCODE_LSR_ACCUMULATOR:
                runCycleImplied();
                break;
            case Cpu6502InstructionDefs.OPCODE_LSR_ABSOLUTE:
                runCycleAbsoluteReadModifyWrite();
                break;
            case Cpu6502InstructionDefs.OPCODE_LSR_ZERO_PAGE:
                runCycleZeroPageReadModifyWrite();
                break;
            case Cpu6502InstructionDefs.OPCODE_LSR_ZERO_PAGE_INDEXED_X:
                runCycleZeroPageIndexedXReadModifyWrite();
                break;
            case Cpu6502InstructionDefs.OPCODE_LSR_ABSOLUTE_INDEXED_X:
                runCycleAbsoluteIndexedXReadModifyWrite();
                break;
            //ROL
            case Cpu6502InstructionDefs.OPCODE_ROL_ACCUMULATOR:
                runCycleImplied();
                break;
            case Cpu6502InstructionDefs.OPCODE_ROL_ABSOLUTE:
                runCycleAbsoluteReadModifyWrite();
                break;
            case Cpu6502InstructionDefs.OPCODE_ROL_ZERO_PAGE:
                runCycleZeroPageReadModifyWrite();
                break;
            case Cpu6502InstructionDefs.OPCODE_ROL_ZERO_PAGE_INDEXED_X:
                runCycleZeroPageIndexedXReadModifyWrite();
                break; 
            case Cpu6502InstructionDefs.OPCODE_ROL_ABSOLUTE_INDEXED_X:
                runCycleAbsoluteIndexedXReadModifyWrite();
                break; 
            //ROR
            case Cpu6502InstructionDefs.OPCODE_ROR_ACCUMULATOR:
                runCycleImplied();
                break;
            case Cpu6502InstructionDefs.OPCODE_ROR_ABSOLUTE:
                runCycleAbsoluteReadModifyWrite();
                break;
            case Cpu6502InstructionDefs.OPCODE_ROR_ZERO_PAGE:
                runCycleZeroPageReadModifyWrite();
                break;
            case Cpu6502InstructionDefs.OPCODE_ROR_ZERO_PAGE_INDEXED_X:
                runCycleZeroPageIndexedXReadModifyWrite();
                break;
            case Cpu6502InstructionDefs.OPCODE_ROR_ABSOLUTE_INDEXED_X:
                runCycleAbsoluteIndexedXReadModifyWrite();
                break;
            //Jump Instructions (JMP, JSR)
            
            //JMP
            case Cpu6502InstructionDefs.OPCODE_JMP_ABSOLUTE:
                runCycleJumpAbsolute();
                break;
            case Cpu6502InstructionDefs.OPCODE_JMP_INDIRECT:
                runCycleJumpIndirect();
                break;
            //JSR
            case Cpu6502InstructionDefs.OPCODE_JSR_ABSOLUTE:
                runCycleJumpSubroutineAbsolute();
                break;
            
            //Return Instructions (implied mode) (RTI, RTS)

            //RTI 
            //TODO: IMPL RTI!

            //RTS
            case Cpu6502InstructionDefs.OPCODE_RTS_IMPLIED:
                runCycleImpliedReturnFromSubroutine();
                break;

            //Branch Instructions (relative mode) (BPL, BMI, BVC, BVS, BCC, BCS, BNE, BEQ)
            
            case Cpu6502InstructionDefs.OPCODE_BPL_RELATIVE:
                runCycleRelative();
                break;
            case Cpu6502InstructionDefs.OPCODE_BMI_RELATIVE:
                runCycleRelative();
                break;
            case Cpu6502InstructionDefs.OPCODE_BVC_RELATIVE:
                runCycleRelative();
                break;
            case Cpu6502InstructionDefs.OPCODE_BVS_RELATIVE:
                runCycleRelative();
                break;
            case Cpu6502InstructionDefs.OPCODE_BCC_RELATIVE:
                runCycleRelative();
                break;
            case Cpu6502InstructionDefs.OPCODE_BCS_RELATIVE:
                runCycleRelative();
                break;
            case Cpu6502InstructionDefs.OPCODE_BNE_RELATIVE:
                runCycleRelative();
                break;
            case Cpu6502InstructionDefs.OPCODE_BEQ_RELATIVE:
                runCycleRelative();
                break;

            //Compare Instructions (CMP, CPX, CPY, BIT)
            
            //CMP
            case Cpu6502InstructionDefs.OPCODE_CMP_IMMEDIATE:
                runCycleImmediate();
                break;
            case Cpu6502InstructionDefs.OPCODE_CMP_ABSOLUTE:
                runCycleAbsoluteRead();
                break;
            case Cpu6502InstructionDefs.OPCODE_CMP_ZERO_PAGE:
                runCycleZeroPageRead();
                break;
            case Cpu6502InstructionDefs.OPCODE_CMP_ZERO_PAGE_INDEXED_X:
                runCycleZeroPageIndexedXRead();
                break;
            case Cpu6502InstructionDefs.OPCODE_CMP_ABSOLUTE_INDEXED_X:
                runCycleAbsoluteIndexedXRead();
                break;
            case Cpu6502InstructionDefs.OPCODE_CMP_ABSOLUTE_INDEXED_Y:
                runCycleAbsoluteIndexedYRead();
                break;
            case Cpu6502InstructionDefs.OPCODE_CMP_INDEXED_INDIRECT:
                runCycleIndexedIndirectRead();
                break;
            case Cpu6502InstructionDefs.OPCODE_CMP_INDIRECT_INDEXED:
                runCycleIndirectIndexedRead();
                break;
            //CPX
            case Cpu6502InstructionDefs.OPCODE_CPX_IMMEDIATE:
                runCycleImmediate();
                break;
            case Cpu6502InstructionDefs.OPCODE_CPX_ABSOLUTE:
                runCycleAbsoluteRead();
                break;
            case Cpu6502InstructionDefs.OPCODE_CPX_ZERO_PAGE:
                runCycleZeroPageRead();
                break;
            //CPY
            case Cpu6502InstructionDefs.OPCODE_CPY_IMMEDIATE:
                runCycleImmediate();
                break;    
            case Cpu6502InstructionDefs.OPCODE_CPY_ABSOLUTE:
                runCycleAbsoluteRead();
                break; 
            case Cpu6502InstructionDefs.OPCODE_CPY_ZERO_PAGE:
                runCycleZeroPageRead();
                break; 
            //BIT
            case Cpu6502InstructionDefs.OPCODE_BIT_ABSOLUTE:
                runCycleAbsoluteRead();
                break;
            case Cpu6502InstructionDefs.OPCODE_BIT_ZERO_PAGE:
                runCycleZeroPageRead();
                break;
            //Processor Status Instructions (implied mode) (CLC, SEC, CLI, SEI, CLV, CLD, SED)
            
            //CLC
            case Cpu6502InstructionDefs.OPCODE_CLC_IMPLIED:
                runCycleImplied();
                break;
            //SEC
            case Cpu6502InstructionDefs.OPCODE_SEC_IMPLIED:
                runCycleImplied();
                break;
            //CLI
            case Cpu6502InstructionDefs.OPCODE_CLI_IMPLIED:
                runCycleImplied();
                break;
            //SEI
            case Cpu6502InstructionDefs.OPCODE_SEI_IMPLIED:
                runCycleImplied();
                break;
            //CLV
            case Cpu6502InstructionDefs.OPCODE_CLV_IMPLIED:
                runCycleImplied();
                break;
            //CLD
            case Cpu6502InstructionDefs.OPCODE_CLD_IMPLIED:
                runCycleImplied();
                break;
            //SED
            case Cpu6502InstructionDefs.OPCODE_SED_IMPLIED:
                runCycleImplied();
                break;
            //Register Instructions (implied mode) (TAX, TXA, TAY, TYA, DEX, INX, DEY, INY)
            case Cpu6502InstructionDefs.OPCODE_TAX_IMPLIED:
                runCycleImplied();
                break;
            case Cpu6502InstructionDefs.OPCODE_TXA_IMPLIED:
                runCycleImplied();
                break;
            case Cpu6502InstructionDefs.OPCODE_TAY_IMPLIED:
                runCycleImplied();
                break;
            case Cpu6502InstructionDefs.OPCODE_TYA_IMPLIED:
                runCycleImplied();
                break;
            case Cpu6502InstructionDefs.OPCODE_DEX_IMPLIED:
                runCycleImplied();
                break;
            case Cpu6502InstructionDefs.OPCODE_INX_IMPLIED:
                runCycleImplied();
                break;
            case Cpu6502InstructionDefs.OPCODE_DEY_IMPLIED:
                runCycleImplied();
                break;
            case Cpu6502InstructionDefs.OPCODE_INY_IMPLIED:
                runCycleImplied();
                break;
            
            //Stack Instructions (implied mode) (TXS, TSX, PHA, PHP, PLA, PLP)
            case Cpu6502InstructionDefs.OPCODE_TXS_IMPLIED:
                runCycleImplied();
                break;
            case Cpu6502InstructionDefs.OPCODE_TSX_IMPLIED:
                runCycleImplied();
                break; 
            case Cpu6502InstructionDefs.OPCODE_PHA_IMPLIED:
                runCycleImpliedPush();
                break;
            case Cpu6502InstructionDefs.OPCODE_PHP_IMPLIED:
                runCycleImpliedPush();
                break;
            case Cpu6502InstructionDefs.OPCODE_PLA_IMPLIED:
                runCycleImpliedPull();
                break;
            case Cpu6502InstructionDefs.OPCODE_PLP_IMPLIED:
                runCycleImpliedPull();
                break;

            //Miscellaneous Implied Mode Instructions (BRK, NOP)
            //BRK
            //TODO: IMPL BRK!

            //NOP
            case Cpu6502InstructionDefs.OPCODE_NOP_IMPLIED:
                runCycleImplied();
                break;

            //Default, NOT GIVEN VALID INSTRUCTION
            default:
                System.out.println("Opcode: "+LibBaseConversion.toHexFixed(currentOpcode, 2));
                System.out.println("Opcode not implemented (or illegal)!");
                //registerProgramCounter++;
        }
    }

    /**********************************************************
    **                 INSTRUCTION METHODS                   **
    **   (called for timing/opcode specific functionality)   **
    **                                                       **
    **********************************************************/


    private void runCycleJumpAbsolute() {
        switch (instructionCycle) {
            case 2:
                //Fetch low byte, increment PC
                workingValue = addressRead(registerProgramCounter);
                registerProgramCounter++;
                instructionCycle++;
                break;
            case 3:
                //Fetch high byte, write to PC
                workingValue += addressRead(registerProgramCounter)*0x100;
                registerProgramCounter = workingValue;
                instructionCycle = 1;
                break;
        }
    }

    private void runCycleJumpIndirect() {
        switch (instructionCycle) {
            case 2:
                //Load ptr address low
                workingValue = addressRead(registerProgramCounter);
                registerProgramCounter++;
                instructionCycle++;
                break;
            case 3:
                //Load ptr address high
                workingValue += addressRead(registerProgramCounter)*0x100;
                registerProgramCounter = workingValue;
                registerProgramCounter++;
                instructionCycle++;
                System.out.println(workingValue);
                break;
            case 4:
                //fetch low address to latch
                effectiveAddress = addressRead(workingValue);
                instructionCycle++;
                break;
            case 5:
                //fetch PCH, copy latch to PCL (Same page, boundary crossing not impl, intended)
                effectiveAddress += addressRead((workingValue&0xFF00)+((workingValue+1)&0x00FF))*0x100;
                System.out.println(effectiveAddress);
                registerProgramCounter = effectiveAddress;
                instructionCycle = 1;
                break; 
        }
    }

    private void runCycleJumpSubroutineAbsolute() {
        switch (instructionCycle) {
            case 2:
                //Fetch low byte, increment PC
                workingValue = addressRead(registerProgramCounter);
                registerProgramCounter++;
                instructionCycle++;
                break;
            case 3:
                //internal operation (predecrement S?)
                workingValue += addressRead(registerProgramCounter)*0x100;
                //registerProgramCounter = workingValue;
                instructionCycle++;
                break;
            case 4:
                //push PCH on stack, decrement S
                pushStack(registerProgramCounter>>8);
                instructionCycle++;
                break;
            case 5:
                //push PCL on stack, decrement S
                pushStack(registerProgramCounter&0xFF);
                instructionCycle++;
                break;
            case 6:
                //copy low address byte to PCL, fetch high address byte to PCH
                registerProgramCounter = workingValue;
                instructionCycle = 1;
                break;
        }
    }

    private void runCycleImpliedReturnFromSubroutine() {
        switch (instructionCycle) {
            case 2:
                //Read and throw away next instruction
                addressRead(registerProgramCounter);
                instructionCycle++;
                break;
            case 3:
                //increment stack pointer
                registerStackPointer = ((registerStackPointer+1)&0xFF);
                instructionCycle++;
                break;
            case 4:
                //pull PCL from stack, increment stack pointer
                effectiveAddress = addressRead(0x100+registerStackPointer);
                registerProgramCounter = (registerProgramCounter&0xFF00) + effectiveAddress;
                registerStackPointer = ((registerStackPointer+1)&0xFF);
                instructionCycle++;
                break;
            case 5:
                //pull PCH from stack
                effectiveAddress = ((effectiveAddress + (addressRead(0x100+registerStackPointer)<<8))&0xFFFF);
                registerProgramCounter = effectiveAddress;
                instructionCycle++;
                break;
            case 6:
                //increment PC (as to not loop JSR)
                registerProgramCounter = ((registerProgramCounter+1)&0xFFFF);
                instructionCycle = 1;
                break;
        }
    }

    private void runCycleImpliedBreak() { //TODO: IMPLE PROPER!
        switch (instructionCycle) {
            case 2:
                //Read and throw away next instruction, increment PC
                addressRead(registerProgramCounter);
                registerProgramCounter = ((registerProgramCounter+1)&0xFFFF);
                instructionCycle++;
                break;
            case 3:
                //increment stack pointer
                registerStackPointer = ((registerStackPointer+1)&0xFF);
                instructionCycle++;
                break;
            case 4:
                //pull PCL from stack, increment stack pointer
                effectiveAddress = addressRead(0x100+registerStackPointer);
                registerProgramCounter = (registerProgramCounter&0xFF00) + effectiveAddress;
                registerStackPointer = ((registerStackPointer+1)&0xFF);
                instructionCycle++;
                break;
            case 5:
                //pull PCH from stack
                effectiveAddress = ((effectiveAddress + (addressRead(0x100+registerStackPointer)<<8))&0xFFFF);
                registerProgramCounter = effectiveAddress;
                instructionCycle++;
                break;
            case 6:
                //increment PC (as to not loop JSR)
                registerProgramCounter = ((registerProgramCounter+1)&0xFFFF);
                instructionCycle = 1;
                break;
        }
    }

    private void runCycleImplied() {
        switch (instructionCycle) {
            case 2:
                //Read next instruction byte
                addressRead(registerProgramCounter);
                switch (currentOpcode) {
                    case Cpu6502InstructionDefs.OPCODE_CLC_IMPLIED:
                        //Clear Carry Flag
                        setFlag(false, STATUS_FLAG_C);
                        break;
                    case Cpu6502InstructionDefs.OPCODE_SEC_IMPLIED:
                        //Set Carry Flag
                        setFlag(true, STATUS_FLAG_C);
                        break;
                    case Cpu6502InstructionDefs.OPCODE_CLI_IMPLIED:
                        //Clear Interrupt Flag
                        setFlag(false, STATUS_FLAG_I);
                        break;
                    case Cpu6502InstructionDefs.OPCODE_SEI_IMPLIED:
                        //Set Interrupt Flag
                        setFlag(true, STATUS_FLAG_I);
                        break;
                    case Cpu6502InstructionDefs.OPCODE_CLV_IMPLIED:
                        //Clear Overflow Flag
                        setFlag(false, STATUS_FLAG_V);
                        break;
                    case Cpu6502InstructionDefs.OPCODE_CLD_IMPLIED:
                        //Clear Decimal Flag
                        setFlag(false, STATUS_FLAG_D);
                        break;
                    case Cpu6502InstructionDefs.OPCODE_SED_IMPLIED:
                        //Set Decimal Flag
                        setFlag(true, STATUS_FLAG_D);
                        break;
                    case Cpu6502InstructionDefs.OPCODE_NOP_IMPLIED:
                        //No Operation (do nothing)
                        break;
                    case Cpu6502InstructionDefs.OPCODE_TAX_IMPLIED:
                        //Transfer A to X
                        registerX = registerAccumulator;
                        setFlag((registerX>=0x80), STATUS_FLAG_N);
                        setFlag((registerX==0), STATUS_FLAG_Z);
                        break;
                    case Cpu6502InstructionDefs.OPCODE_TXA_IMPLIED:
                        //Transfer X to A
                        registerAccumulator = registerX;
                        setFlag((registerAccumulator>=0x80), STATUS_FLAG_N);
                        setFlag((registerAccumulator==0), STATUS_FLAG_Z);
                        break;
                    case Cpu6502InstructionDefs.OPCODE_DEX_IMPLIED:
                        //Decrement X
                        registerX = (registerX-1)&0xFF;
                        setFlag((registerX>=0x80), STATUS_FLAG_N);
                        setFlag((registerX==0), STATUS_FLAG_Z);
                        break;
                    case Cpu6502InstructionDefs.OPCODE_INX_IMPLIED:
                        //Increment X
                        registerX = (registerX+1)&0xFF;
                        setFlag((registerX>=0x80), STATUS_FLAG_N);
                        setFlag((registerX==0), STATUS_FLAG_Z);
                        break;
                    case Cpu6502InstructionDefs.OPCODE_TAY_IMPLIED:
                        //Transfer A to Y
                        registerY = registerAccumulator;
                        setFlag((registerY>=0x80), STATUS_FLAG_N);
                        setFlag((registerY==0), STATUS_FLAG_Z);
                        break;
                    case Cpu6502InstructionDefs.OPCODE_TYA_IMPLIED:
                        //Transfer Y to A
                        registerAccumulator = registerY;
                        setFlag((registerAccumulator>=0x80), STATUS_FLAG_N);
                        setFlag((registerAccumulator==0), STATUS_FLAG_Z);
                        break;
                    case Cpu6502InstructionDefs.OPCODE_DEY_IMPLIED:
                        //Decrement Y
                        registerY = (registerY-1)&0xFF;
                        setFlag((registerY>=0x80), STATUS_FLAG_N);
                        setFlag((registerY==0), STATUS_FLAG_Z);
                        break;
                    case Cpu6502InstructionDefs.OPCODE_INY_IMPLIED:
                        //Increment Y
                        registerY = (registerY+1)&0xFF;
                        setFlag((registerY>=0x80), STATUS_FLAG_N);
                        setFlag((registerY==0), STATUS_FLAG_Z);
                        break;
                    case Cpu6502InstructionDefs.OPCODE_TXS_IMPLIED:
                        //Transfer X to S
                        registerStackPointer = registerX;
                        break;
                    case Cpu6502InstructionDefs.OPCODE_TSX_IMPLIED:
                        //Transfer S to X
                        registerX = registerStackPointer;
                        setFlag((registerX>=0x80), STATUS_FLAG_N);
                        setFlag((registerX==0), STATUS_FLAG_Z);
                        break;
                    case Cpu6502InstructionDefs.OPCODE_ROL_ACCUMULATOR:
                        //Rotate Accumulator Left
                        {
                            int carryVal = (((registerStatus&STATUS_FLAG_C)==STATUS_FLAG_C) ? 0x01:0);
                            setFlag(((registerAccumulator&0x80)==0x80), STATUS_FLAG_C); //Put Bit 7 Into Carry
                            registerAccumulator = (registerAccumulator<<1)&0xFF; //Left shift Accumulator
                            registerAccumulator += carryVal; //Shift in carry bit
                            setFlag((registerAccumulator>=0x80), STATUS_FLAG_N);
                            setFlag((registerAccumulator==0), STATUS_FLAG_Z);
                            break;
                        }
                    case Cpu6502InstructionDefs.OPCODE_ROR_ACCUMULATOR:
                        {
                            int carryVal = (((registerStatus&STATUS_FLAG_C)==STATUS_FLAG_C) ? 0x80:0);
                            setFlag(((registerAccumulator&0x01)==0x01), STATUS_FLAG_C); //Put Bit 0 Into Carry
                            registerAccumulator = (registerAccumulator>>1)&0xFF; //Right shift Accumulator
                            registerAccumulator += carryVal; //Shift in carry bit
                            setFlag((registerAccumulator>=0x80), STATUS_FLAG_N);
                            setFlag((registerAccumulator==0), STATUS_FLAG_Z);
                            break;
                        }
                    case Cpu6502InstructionDefs.OPCODE_ASL_ACCUMULATOR:
                        {
                            setFlag(((registerAccumulator&0x80)==0x80), STATUS_FLAG_C); //Put Bit 7 Into Carry
                            registerAccumulator = (registerAccumulator<<1)&0xFF; //Left shift Accumulator
                            setFlag((registerAccumulator>=0x80), STATUS_FLAG_N);
                            setFlag((registerAccumulator==0), STATUS_FLAG_Z);
                            break;
                        }
                    case Cpu6502InstructionDefs.OPCODE_LSR_ACCUMULATOR:
                        {
                            setFlag(((registerAccumulator&0x01)==0x01), STATUS_FLAG_C); //Put Bit 0 Into Carry
                            registerAccumulator = (registerAccumulator>>1)&0xFF; //Right shift Accumulator
                            setFlag((registerAccumulator>=0x80), STATUS_FLAG_N);
                            setFlag((registerAccumulator==0), STATUS_FLAG_Z);
                            break;
                        }
                }
                instructionCycle = 1;
                break;
        }
    }
    
    private void runCycleImpliedPush() {
        switch (instructionCycle) {
            case 2:
                //Read next instruction byte, throw it away
                addressRead(registerProgramCounter);
                instructionCycle++;
                break;
            case 3:
                //push register on stack, decrement S
                readWriteFlag = RwWRITE;
                switch (currentOpcode) {
                    case Cpu6502InstructionDefs.OPCODE_PHA_IMPLIED:
                        //Push A onto stack
                        workingValue = registerAccumulator;
                        break;
                    case Cpu6502InstructionDefs.OPCODE_PHP_IMPLIED:
                        //Push P onto stack
                        workingValue = registerStatus;
                        break;
                }
                pushStack(workingValue);
                instructionCycle=1;
                break;
        }
    }
    private void runCycleImpliedPull() {
        switch (instructionCycle) {
            case 2:
                //Read next instruction byte, throw it away
                addressRead(registerProgramCounter);
                instructionCycle++;
                break;
            case 3:
                //Increment S
                registerStackPointer = (registerStackPointer+1)&0xFF;
                instructionCycle++;
                break;  
            case 4:
                //pull register from stack
                workingValue = addressRead(0x100+registerStackPointer);
                switch (currentOpcode) {
                    case Cpu6502InstructionDefs.OPCODE_PLA_IMPLIED:
                        //Pull A from stack
                        registerAccumulator = workingValue;
                        break;
                    case Cpu6502InstructionDefs.OPCODE_PLP_IMPLIED:
                        //Pull P from stack
                        registerStatus = workingValue|STATUS_FLAG_U;
                        break;
                }
                instructionCycle = 1;
                break;
        }
    }

    
    private void runCycleImmediate() {
        switch (instructionCycle) {
            case 2:
                //Fetch value, increment PC
                workingValue = addressRead(registerProgramCounter);
                switch (currentOpcode) {
                    case Cpu6502InstructionDefs.OPCODE_ADC_IMMEDIATE:
                        runADC();
                        break;
                    case Cpu6502InstructionDefs.OPCODE_AND_IMMEDIATE:
                        runAND();
                        break;
                    case Cpu6502InstructionDefs.OPCODE_CMP_IMMEDIATE:
                        runCMP();
                        break;
                    case Cpu6502InstructionDefs.OPCODE_CPX_IMMEDIATE:
                        runCPX();
                        break;
                    case Cpu6502InstructionDefs.OPCODE_CPY_IMMEDIATE:
                        runCPY();
                        break;
                    case Cpu6502InstructionDefs.OPCODE_EOR_IMMEDIATE:
                        runEOR();
                        break;
                    case Cpu6502InstructionDefs.OPCODE_LDA_IMMEDIATE:
                        runLDA();
                        break;
                    case Cpu6502InstructionDefs.OPCODE_LDX_IMMEDIATE:
                        runLDX();
                        break;
                    case Cpu6502InstructionDefs.OPCODE_LDY_IMMEDIATE:
                        runLDY();
                        break;
                    case Cpu6502InstructionDefs.OPCODE_ORA_IMMEDIATE:
                        runORA();
                        break;
                    case Cpu6502InstructionDefs.OPCODE_SBC_IMMEDIATE:
                        runSBC();
                        break;
                }
            registerProgramCounter++;
            instructionCycle = 1;
            break;
        }
    }
    private void runCycleAbsoluteRead() {
        switch (instructionCycle) {
            case 2:
                //Fetch address low, increment PC
                effectiveAddress = addressRead(registerProgramCounter);
                registerProgramCounter++;
                instructionCycle++;
                break;
            case 3:
                //Fetch address high, increment PC
                effectiveAddress += addressRead(registerProgramCounter)<<8;
                registerProgramCounter++;
                instructionCycle++;
                break;
            case 4:
                //Read from effective address
                workingValue = addressRead(effectiveAddress);
                switch (currentOpcode) {
                    case Cpu6502InstructionDefs.OPCODE_ADC_ABSOLUTE:
                        runADC();
                        break;
                    case Cpu6502InstructionDefs.OPCODE_AND_ABSOLUTE:
                        runAND();
                        break;
                    case Cpu6502InstructionDefs.OPCODE_BIT_ABSOLUTE:
                        runBIT();
                        break;
                    case Cpu6502InstructionDefs.OPCODE_CMP_ABSOLUTE:
                        runCMP();
                        break;
                    case Cpu6502InstructionDefs.OPCODE_CPX_ABSOLUTE:
                        runCPX();
                        break;
                    case Cpu6502InstructionDefs.OPCODE_CPY_ABSOLUTE:
                        runCPY();
                        break;
                    case Cpu6502InstructionDefs.OPCODE_EOR_ABSOLUTE:
                        runEOR();
                        break;
                    case Cpu6502InstructionDefs.OPCODE_LDA_ABSOLUTE:
                        runLDA();
                        break;
                    case Cpu6502InstructionDefs.OPCODE_LDX_ABSOLUTE:
                        runLDX();
                        break;
                    case Cpu6502InstructionDefs.OPCODE_LDY_ABSOLUTE:
                        runLDY();
                        break;
                    case Cpu6502InstructionDefs.OPCODE_ORA_ABSOLUTE:
                        runORA();
                        break;
                    case Cpu6502InstructionDefs.OPCODE_SBC_ABSOLUTE:
                        runSBC();
                        break;
                }
                instructionCycle = 1;
                break;
        }
    }
    private void runCycleAbsoluteReadModifyWrite() {
        switch (instructionCycle) {
            case 2:
                //Fetch address low, increment PC
                effectiveAddress = addressRead(registerProgramCounter);
                registerProgramCounter++;
                instructionCycle++;
                break;
            case 3:
                //Fetch address high, increment PC
                effectiveAddress += addressRead(registerProgramCounter)<<8;
                registerProgramCounter++;
                instructionCycle++;
                break;
            case 4:
                //Read from effective address
                workingValue = addressRead(effectiveAddress);
                instructionCycle++;
                break;
            case 5:
                //Write value to effective address, then perform operation
                addressWrite(effectiveAddress, workingValue);
                switch (currentOpcode) {
                    case Cpu6502InstructionDefs.OPCODE_ASL_ABSOLUTE:
                        runASL();
                        break;
                    case Cpu6502InstructionDefs.OPCODE_LSR_ABSOLUTE:
                        runLSR();
                        break;
                    case Cpu6502InstructionDefs.OPCODE_ROL_ABSOLUTE:
                        runROL();
                        break;
                    case Cpu6502InstructionDefs.OPCODE_ROR_ABSOLUTE:
                        runROR();
                        break;
                    case Cpu6502InstructionDefs.OPCODE_INC_ABSOLUTE:
                        runINC();
                        break;
                    case Cpu6502InstructionDefs.OPCODE_DEC_ABSOLUTE:
                        runDEC();
                        break;
                }
                instructionCycle++;
                break;
            case 6:
                //Write result to effective address
                addressWrite(effectiveAddress, operationResult);
                instructionCycle = 1;
                break;
        }
    }
    private void runCycleAbsoluteWrite() {
        switch (instructionCycle) {
            case 2:
                //Fetch address low, increment PC
                effectiveAddress = addressRead(registerProgramCounter);
                registerProgramCounter++;
                instructionCycle++;
                break;
            case 3:
                //Fetch address high, increment PC
                effectiveAddress += addressRead(registerProgramCounter)<<8;
                registerProgramCounter++;
                instructionCycle++;
                break;
            case 4:
                //Write to effective address
                readWriteFlag = RwWRITE;
                workingValue = addressRead(effectiveAddress);
                switch (currentOpcode) {
                    case Cpu6502InstructionDefs.OPCODE_STA_ABSOLUTE:
                        runSTA();
                        break;
                    case Cpu6502InstructionDefs.OPCODE_STX_ABSOLUTE:
                        runSTX();
                        break;
                    case Cpu6502InstructionDefs.OPCODE_STY_ABSOLUTE:
                        runSTY();
                        break;
                }
                instructionCycle = 1;
                break;
        }
    }
    private void runCycleZeroPageRead() {
        switch (instructionCycle) {
            case 2:
                //Fetch address, increment PC
                effectiveAddress = addressRead(registerProgramCounter);
                registerProgramCounter++;
                instructionCycle++;
                break;
            case 3:
                //Read from effective address
                workingValue = addressRead(effectiveAddress);
                switch (currentOpcode) {
                    case Cpu6502InstructionDefs.OPCODE_LDA_ZERO_PAGE:
                        runLDA();
                        break;
                    case Cpu6502InstructionDefs.OPCODE_LDX_ZERO_PAGE:
                        runLDX();
                        break;
                    case Cpu6502InstructionDefs.OPCODE_LDY_ZERO_PAGE:
                        runLDY();
                        break;
                    case Cpu6502InstructionDefs.OPCODE_EOR_ZERO_PAGE:
                        runEOR();
                        break;
                    case Cpu6502InstructionDefs.OPCODE_AND_ZERO_PAGE:
                        runAND();
                        break;
                    case Cpu6502InstructionDefs.OPCODE_ORA_ZERO_PAGE:
                        runORA();
                        break;
                    case Cpu6502InstructionDefs.OPCODE_ADC_ZERO_PAGE:
                        runADC();
                        break;
                    case Cpu6502InstructionDefs.OPCODE_SBC_ZERO_PAGE:
                        runSBC();
                        break;
                    case Cpu6502InstructionDefs.OPCODE_CMP_ZERO_PAGE:
                        runCMP();
                        break;
                    case Cpu6502InstructionDefs.OPCODE_CPX_ZERO_PAGE:
                        runCPX();
                        break;
                    case Cpu6502InstructionDefs.OPCODE_CPY_ZERO_PAGE:
                        runCPY();
                        break;
                    case Cpu6502InstructionDefs.OPCODE_BIT_ZERO_PAGE:
                        runBIT();
                        break;
                    
                }
                instructionCycle = 1;
                break;
        }
    }
    private void runCycleZeroPageReadModifyWrite() {
        switch (instructionCycle) {
            case 2:
                //Fetch address, increment PC
                effectiveAddress = addressRead(registerProgramCounter);
                registerProgramCounter++;
                instructionCycle++;
                break;
            case 3:
                //Read from effective address
                workingValue = addressRead(effectiveAddress);
                instructionCycle++;
                break;
            case 4:
                //Write value to effective address, then perform operation
                addressWrite(effectiveAddress, workingValue);
                switch (currentOpcode) {
                    case Cpu6502InstructionDefs.OPCODE_ASL_ZERO_PAGE:
                        runASL();
                        break;
                    case Cpu6502InstructionDefs.OPCODE_LSR_ZERO_PAGE:
                        runLSR();
                        break;
                    case Cpu6502InstructionDefs.OPCODE_ROL_ZERO_PAGE:
                        runROL();
                        break;
                    case Cpu6502InstructionDefs.OPCODE_ROR_ZERO_PAGE:
                        runROR();
                        break;
                    case Cpu6502InstructionDefs.OPCODE_INC_ZERO_PAGE:
                        runINC();
                        break;
                    case Cpu6502InstructionDefs.OPCODE_DEC_ZERO_PAGE:
                        runDEC();
                        break;
                }
                instructionCycle++;
                break;
            case 5:
                //Write result to effective address
                addressWrite(effectiveAddress, operationResult);
                instructionCycle = 1;
                break;
        }
    }
    private void runCycleZeroPageWrite() {
        switch (instructionCycle) {
            case 2:
                //Fetch address, increment PC
                effectiveAddress = addressRead(registerProgramCounter);
                registerProgramCounter++;
                instructionCycle++;
                break;
            case 3:
                //Write to effective address
                readWriteFlag = RwWRITE;
                switch (currentOpcode) {
                    case Cpu6502InstructionDefs.OPCODE_STA_ZERO_PAGE:
                        runSTA();
                        break;
                    case Cpu6502InstructionDefs.OPCODE_STX_ZERO_PAGE:
                        runSTX();
                        break;
                    case Cpu6502InstructionDefs.OPCODE_STY_ZERO_PAGE:
                        runSTY();
                        break;
                }
                instructionCycle = 1;
                break;
        }
    }
    private void runCycleZeroPageIndexedXRead() {
        switch (instructionCycle) {
            case 2:
                //Fetch address, increment PC
                effectiveAddress = addressRead(registerProgramCounter);
                registerProgramCounter++;
                instructionCycle++;
                break;
            case 3:
                //read from address, add register X to it (ignores high byte)
                addressRead(effectiveAddress);
                effectiveAddress = (effectiveAddress&0xFF00)+((effectiveAddress+registerX)&0x00FF);
                instructionCycle++;
                break;
            case 4:
                //Read from effective address
                workingValue = addressRead(effectiveAddress);
                switch (currentOpcode) {
                    case Cpu6502InstructionDefs.OPCODE_LDA_ZERO_PAGE_INDEXED_X:
                        runLDA();
                        break;
                    case Cpu6502InstructionDefs.OPCODE_LDY_ZERO_PAGE_INDEXED_X:
                        runLDY();
                        break;
                    case Cpu6502InstructionDefs.OPCODE_EOR_ZERO_PAGE_INDEXED_X:
                        runEOR();
                        break;
                    case Cpu6502InstructionDefs.OPCODE_AND_ZERO_PAGE_INDEXED_X:
                        runAND();
                        break;
                    case Cpu6502InstructionDefs.OPCODE_ORA_ZERO_PAGE_INDEXED_X:
                        runORA();
                        break;
                    case Cpu6502InstructionDefs.OPCODE_ADC_ZERO_PAGE_INDEXED_X:
                        runADC();
                        break;
                    case Cpu6502InstructionDefs.OPCODE_SBC_ZERO_PAGE_INDEXED_X:
                        runSBC();
                        break;
                    case Cpu6502InstructionDefs.OPCODE_CMP_ZERO_PAGE_INDEXED_X:
                        runCMP();
                        break;  
                }
                instructionCycle = 1;
                break;
        }
    }
    private void runCycleZeroPageIndexedXReadModifyWrite() {
        switch (instructionCycle) {
            case 2:
                //Fetch address, increment PC
                effectiveAddress = addressRead(registerProgramCounter);
                registerProgramCounter++;
                instructionCycle++;
                break;
            case 3:
                //read from address, add register X to it (ignores high byte)
                addressRead(effectiveAddress);
                effectiveAddress = (effectiveAddress&0xFF00)+((effectiveAddress+registerX)&0x00FF);
                instructionCycle++;
                break;
            case 4:
                //Read from effective address
                workingValue = addressRead(effectiveAddress);
                instructionCycle++;
                break;
            case 5:
                //Write value to effective address, then perform operation
                addressWrite(effectiveAddress, workingValue);
                switch (currentOpcode) {
                    case Cpu6502InstructionDefs.OPCODE_ASL_ZERO_PAGE_INDEXED_X:
                        runASL();
                        break;
                    case Cpu6502InstructionDefs.OPCODE_LSR_ZERO_PAGE_INDEXED_X:
                        runLSR();
                        break;
                    case Cpu6502InstructionDefs.OPCODE_ROL_ZERO_PAGE_INDEXED_X:
                        runROL();
                        break;
                    case Cpu6502InstructionDefs.OPCODE_ROR_ZERO_PAGE_INDEXED_X:
                        runROR();
                        break;
                    case Cpu6502InstructionDefs.OPCODE_INC_ZERO_PAGE_INDEXED_X:
                        runINC();
                        break;
                    case Cpu6502InstructionDefs.OPCODE_DEC_ZERO_PAGE_INDEXED_X:
                        runDEC();
                        break;
                }
                instructionCycle++;
                break;
            case 6:
                //Write result to effective address
                addressWrite(effectiveAddress, operationResult);
                instructionCycle = 1;
                break;
        }
    }
    private void runCycleZeroPageIndexedXWrite() {
        switch (instructionCycle) {
            case 2:
                //Fetch address, increment PC
                effectiveAddress = addressRead(registerProgramCounter);
                registerProgramCounter++;
                instructionCycle++;
                break;
            case 3:
                //read from address, add register X to it (ignores high byte)
                addressRead(effectiveAddress);
                effectiveAddress = (effectiveAddress&0xFF00)+((effectiveAddress+registerX)&0x00FF);
                instructionCycle++;
                break;
            case 4:
                //Write to effective address
                readWriteFlag = RwWRITE;
                switch (currentOpcode) {
                    case Cpu6502InstructionDefs.OPCODE_STA_ZERO_PAGE_INDEXED_X:
                        runSTA();
                        break;
                    case Cpu6502InstructionDefs.OPCODE_STY_ZERO_PAGE_INDEXED_X:
                        runSTY();
                        break;
                }
                instructionCycle = 1;
                break;
        }
    }
    private void runCycleZeroPageIndexedYRead() {
        switch (instructionCycle) {
            case 2:
                //Fetch address, increment PC
                effectiveAddress = addressRead(registerProgramCounter);
                registerProgramCounter++;
                instructionCycle++;
                break;
            case 3:
                //read from address, add register X to it (ignores high byte)
                addressRead(effectiveAddress);
                effectiveAddress = (effectiveAddress&0xFF00)+((effectiveAddress+registerY)&0x00FF);
                instructionCycle++;
                break;
            case 4:
                //Read from effective address
                workingValue = addressRead(effectiveAddress);
                switch (currentOpcode) {
                    case Cpu6502InstructionDefs.OPCODE_LDX_ZERO_PAGE_INDEXED_Y:
                        runLDX();
                        break;
                }
                instructionCycle = 1;
                break;
        }
    }
    private void runCycleZeroPageIndexedYWrite() {
        switch (instructionCycle) {
            case 2:
                //Fetch address, increment PC
                effectiveAddress = addressRead(registerProgramCounter);
                registerProgramCounter++;
                instructionCycle++;
                break;
            case 3:
                //read from address, add register X to it (ignores high byte)
                addressRead(effectiveAddress);
                effectiveAddress = (effectiveAddress&0xFF00)+((effectiveAddress+registerY)&0x00FF);
                instructionCycle++;
                break;
            case 4:
                //Write to effective address
                readWriteFlag = RwWRITE;
                switch (currentOpcode) {
                    case Cpu6502InstructionDefs.OPCODE_STX_ZERO_PAGE_INDEXED_Y:
                        runSTX();
                        break;
                }
                instructionCycle = 1;
                break;
        }
    }
    private void runCycleAbsoluteIndexedXRead() {
        switch (instructionCycle) {
            case 2:
                //Fetch address low, increment PC
                effectiveAddress = addressRead(registerProgramCounter);
                registerProgramCounter++;
                instructionCycle++;
                break;
            case 3:
                //Fetch address high, increment PC
                effectiveAddress += addressRead(registerProgramCounter)<<8;
                //index address
                fixedEffectiveAddress = ((effectiveAddress+registerX)&0xFFFF);
                effectiveAddress = (((effectiveAddress&0xFF00)+((effectiveAddress+registerX)&0x00FF))&0xFFFF);
                addressNeedsFix = (effectiveAddress != fixedEffectiveAddress);
                
                registerProgramCounter++;
                if (!addressNeedsFix)
                    instructionCycle++;
                instructionCycle++;
                break;
            case 4:
                workingValue = addressRead(effectiveAddress);
                instructionCycle++;
                break;
            case 5:
                //Read from effective address
                workingValue = addressRead(fixedEffectiveAddress);
                
                switch (currentOpcode) {
                    case Cpu6502InstructionDefs.OPCODE_ADC_ABSOLUTE_INDEXED_X:
                        runADC();
                        break;
                    case Cpu6502InstructionDefs.OPCODE_AND_ABSOLUTE_INDEXED_X:
                        runAND();
                        break;
                    case Cpu6502InstructionDefs.OPCODE_CMP_ABSOLUTE_INDEXED_X:
                        runCMP();
                        break;
                    case Cpu6502InstructionDefs.OPCODE_EOR_ABSOLUTE_INDEXED_X:
                        runEOR();
                        break;
                    case Cpu6502InstructionDefs.OPCODE_LDA_ABSOLUTE_INDEXED_X:
                        runLDA();
                        break;
                    case Cpu6502InstructionDefs.OPCODE_LDY_ABSOLUTE_INDEXED_X:
                        runLDY();
                        break;
                    case Cpu6502InstructionDefs.OPCODE_ORA_ABSOLUTE_INDEXED_X:
                        runORA();
                        break;
                    case Cpu6502InstructionDefs.OPCODE_SBC_ABSOLUTE_INDEXED_X:
                        runSBC();
                        break;
                }
                instructionCycle = 1;
                break;
        }
    }
    private void runCycleAbsoluteIndexedXReadModifyWrite() {
        switch (instructionCycle) {
            case 2:
                //Fetch address low, increment PC
                effectiveAddress = addressRead(registerProgramCounter);
                registerProgramCounter++;
                instructionCycle++;
                break;
            case 3:
                //Fetch address high, increment PC
                effectiveAddress += addressRead(registerProgramCounter)<<8;
                //index address
                fixedEffectiveAddress = ((effectiveAddress+registerX)&0xFFFF);
                effectiveAddress = (((effectiveAddress&0xFF00)+((effectiveAddress+registerX)&0x00FF))&0xFFFF);
                registerProgramCounter++;
                instructionCycle++;
                break;
            case 4:
                //read from effective address, fix high byte
                workingValue = addressRead(effectiveAddress);
                instructionCycle++;
                break;
            case 5:
                //read from fixed effective address
                workingValue = addressRead(fixedEffectiveAddress);
                instructionCycle++;
                break;
            case 6:
                //Write value to effective address, then perform operation
                addressWrite(fixedEffectiveAddress, workingValue);
                switch (currentOpcode) {
                    case Cpu6502InstructionDefs.OPCODE_ASL_ABSOLUTE_INDEXED_X:
                        runASL();
                        break;
                    case Cpu6502InstructionDefs.OPCODE_LSR_ABSOLUTE_INDEXED_X:
                        runLSR();
                        break;
                    case Cpu6502InstructionDefs.OPCODE_ROL_ABSOLUTE_INDEXED_X:
                        runROL();
                        break;
                    case Cpu6502InstructionDefs.OPCODE_ROR_ABSOLUTE_INDEXED_X:
                        runROR();
                        break;
                    case Cpu6502InstructionDefs.OPCODE_INC_ABSOLUTE_INDEXED_X:
                        runINC();
                        break;
                    case Cpu6502InstructionDefs.OPCODE_DEC_ABSOLUTE_INDEXED_X:
                        runDEC();
                        break;
                }
                instructionCycle++;
                break;
            case 7:
                //Write result to effective address
                addressWrite(fixedEffectiveAddress, operationResult);
                instructionCycle = 1;
                break;
        }
    }
    private void runCycleAbsoluteIndexedXWrite() {
        switch (instructionCycle) {
            case 2:
                //Fetch address low, increment PC
                effectiveAddress = addressRead(registerProgramCounter);
                registerProgramCounter++;
                instructionCycle++;
                break;
            case 3:
                //Fetch address high, increment PC
                effectiveAddress += addressRead(registerProgramCounter)<<8;
                //index address
                fixedEffectiveAddress = ((effectiveAddress+registerX)&0xFFFF);
                effectiveAddress = (((effectiveAddress&0xFF00)+((effectiveAddress+registerX)&0x00FF))&0xFFFF);
                registerProgramCounter++;
                instructionCycle++;
                break;
            case 4:
                //read from effective address, fix high byte
                addressRead(effectiveAddress);
                effectiveAddress = fixedEffectiveAddress;
                instructionCycle++;
                break;
            case 5:
                //Write to effective address
                readWriteFlag = RwWRITE;
                switch (currentOpcode) {
                    case Cpu6502InstructionDefs.OPCODE_STA_ABSOLUTE_INDEXED_X:
                        runSTA();
                        break;
                }
                instructionCycle = 1;
                break;
        }
    }
    private void runCycleAbsoluteIndexedYRead() {
        switch (instructionCycle) {
            case 2:
                //Fetch address low, increment PC
                effectiveAddress = addressRead(registerProgramCounter);
                registerProgramCounter++;
                instructionCycle++;
                break;
            case 3:
                //Fetch address high, increment PC
                effectiveAddress += addressRead(registerProgramCounter)<<8;
                //index address
                fixedEffectiveAddress = ((effectiveAddress+registerY)&0xFFFF);
                effectiveAddress = (((effectiveAddress&0xFF00)+((effectiveAddress+registerY)&0x00FF))&0xFFFF);
                addressNeedsFix = (effectiveAddress != fixedEffectiveAddress);
                
                registerProgramCounter++;
                if (!addressNeedsFix)
                    instructionCycle++;
                instructionCycle++;
                break;
            case 4:
                workingValue = addressRead(effectiveAddress);
                instructionCycle++;
                break;
            case 5:
                //Read from effective address
                workingValue = addressRead(fixedEffectiveAddress);
                
                switch (currentOpcode) {
                    case Cpu6502InstructionDefs.OPCODE_ADC_ABSOLUTE_INDEXED_Y:
                        runADC();
                        break;
                    case Cpu6502InstructionDefs.OPCODE_AND_ABSOLUTE_INDEXED_Y:
                        runAND();
                        break;
                    case Cpu6502InstructionDefs.OPCODE_CMP_ABSOLUTE_INDEXED_Y:
                        runCMP();
                        break;
                    case Cpu6502InstructionDefs.OPCODE_EOR_ABSOLUTE_INDEXED_Y:
                        runEOR();
                        break;
                    case Cpu6502InstructionDefs.OPCODE_LDA_ABSOLUTE_INDEXED_Y:
                        runLDA();
                        break;
                    case Cpu6502InstructionDefs.OPCODE_LDX_ABSOLUTE_INDEXED_Y:
                        runLDX();
                        break;
                    case Cpu6502InstructionDefs.OPCODE_ORA_ABSOLUTE_INDEXED_Y:
                        runORA();
                        break;
                    case Cpu6502InstructionDefs.OPCODE_SBC_ABSOLUTE_INDEXED_Y:
                        runSBC();
                        break;
                }
                instructionCycle = 1;
                break;
        }
    }
    private void runCycleAbsoluteIndexedYReadModifyWrite() {
        switch (instructionCycle) {
            case 2:
                //Fetch address low, increment PC
                effectiveAddress = addressRead(registerProgramCounter);
                registerProgramCounter++;
                instructionCycle++;
                break;
            case 3:
                //Fetch address high, increment PC
                effectiveAddress += addressRead(registerProgramCounter)<<8;
                //index address
                fixedEffectiveAddress = ((effectiveAddress+registerY)&0xFFFF);
                effectiveAddress = (((effectiveAddress&0xFF00)+((effectiveAddress+registerY)&0x00FF))&0xFFFF);
                registerProgramCounter++;
                instructionCycle++;
                break;
            case 4:
                //read from effective address, fix high byte
                workingValue = addressRead(effectiveAddress);
                instructionCycle++;
                break;
            case 5:
                //read from fixed effective address
                workingValue = addressRead(fixedEffectiveAddress);
                instructionCycle++;
                break;
            case 6:
                //Write value to effective address, then perform operation
                addressWrite(fixedEffectiveAddress, workingValue);
                /*
                switch (currentOpcode) {
                    //No Legal Opcodes
                }
                */
                instructionCycle++;
                break;
            case 7:
                //Write result to effective address
                addressWrite(fixedEffectiveAddress, operationResult);
                instructionCycle = 1;
                break;
        }
    }
    private void runCycleAbsoluteIndexedYWrite() {
        switch (instructionCycle) {
            case 2:
                //Fetch address low, increment PC
                effectiveAddress = addressRead(registerProgramCounter);
                registerProgramCounter++;
                instructionCycle++;
                break;
            case 3:
                //Fetch address high, increment PC
                effectiveAddress += addressRead(registerProgramCounter)<<8;
                //index address
                fixedEffectiveAddress = ((effectiveAddress+registerY)&0xFFFF);
                effectiveAddress = (((effectiveAddress&0xFF00)+((effectiveAddress+registerY)&0x00FF))&0xFFFF);
                registerProgramCounter++;
                instructionCycle++;
                break;
            case 4:
                //read from effective address, fix high byte
                addressRead(effectiveAddress);
                effectiveAddress = fixedEffectiveAddress;
                instructionCycle++;
                break;
            case 5:
                //Write to effective address
                readWriteFlag = RwWRITE;
                switch (currentOpcode) {
                    case Cpu6502InstructionDefs.OPCODE_STA_ABSOLUTE_INDEXED_Y:
                        runSTA();
                        break;
                }
                instructionCycle = 1;
                break;
        }
    }

    private void runCycleIndexedIndirectRead() {
        switch (instructionCycle) {
            case 2:
                //Fetch address, increment PC
                effectiveAddress = addressRead(registerProgramCounter);
                registerProgramCounter++;
                instructionCycle++;
                break;
            case 3:
                //index address
                effectiveAddress = ((effectiveAddress+registerX)&0x00FF); //no hight byte fixing (intended)
                instructionCycle++;
                break;
            case 4:
                fixedEffectiveAddress = addressRead(effectiveAddress);
                instructionCycle++;
                break;
            case 5:
                fixedEffectiveAddress += addressRead(effectiveAddress+1)<<8;
                instructionCycle++;
                break;
            case 6:
                //Read from effective address
                workingValue = addressRead(fixedEffectiveAddress);
                
                switch (currentOpcode) {
                    case Cpu6502InstructionDefs.OPCODE_ADC_INDEXED_INDIRECT:
                        runADC();
                        break;
                    case Cpu6502InstructionDefs.OPCODE_AND_INDEXED_INDIRECT:
                        runAND();
                        break;
                    case Cpu6502InstructionDefs.OPCODE_CMP_INDEXED_INDIRECT:
                        runCMP();
                        break;
                    case Cpu6502InstructionDefs.OPCODE_EOR_INDEXED_INDIRECT:
                        runEOR();
                        break;
                    case Cpu6502InstructionDefs.OPCODE_LDA_INDEXED_INDIRECT:
                        runLDA();
                        break;
                    case Cpu6502InstructionDefs.OPCODE_ORA_INDEXED_INDIRECT:
                        runORA();
                        break;
                    case Cpu6502InstructionDefs.OPCODE_SBC_INDEXED_INDIRECT:
                        runSBC();
                        break;
                }
                instructionCycle = 1;
                break;
        }
    }
    private void runCycleIndexedIndirectReadModifyWrite() {
        switch (instructionCycle) {
            case 2:
                //Fetch address, increment PC
                effectiveAddress = addressRead(registerProgramCounter);
                registerProgramCounter++;
                instructionCycle++;
                break;
            case 3:
                //index address
                effectiveAddress = ((effectiveAddress+registerX)&0x00FF); //no hight byte fixing (intended)
                instructionCycle++;
                break;
            case 4:
                fixedEffectiveAddress = addressRead(effectiveAddress);
                instructionCycle++;
                break;
            case 5:
                fixedEffectiveAddress += addressRead(effectiveAddress+1)<<8;
                instructionCycle++;
                break;
            case 6:
                //Read from effective address
                addressRead(fixedEffectiveAddress);
                instructionCycle++;
                break;
            case 7:
                //Write value to effective address, then perform operation
                addressWrite(fixedEffectiveAddress, workingValue);
                /*
                switch (currentOpcode) {
                    //No Legal Opcodes
                }
                */
                instructionCycle++;
                break;
            case 8:
                //Write result to effective address
                addressWrite(fixedEffectiveAddress, operationResult);
                instructionCycle = 1;
                break;
        }
    }
    private void runCycleIndexedIndirectWrite() {
        switch (instructionCycle) {
            case 2:
                //Fetch address, increment PC
                effectiveAddress = addressRead(registerProgramCounter);
                registerProgramCounter++;
                instructionCycle++;
                break;
            case 3:
                //index address
                effectiveAddress = ((effectiveAddress+registerX)&0x00FF); //no hight byte fixing (intended)
                instructionCycle++;
                break;
            case 4:
                fixedEffectiveAddress = addressRead(effectiveAddress);
                instructionCycle++;
                break;
            case 5:
                fixedEffectiveAddress += addressRead(effectiveAddress+1)<<8;
                instructionCycle++;
                break;
            case 6:
                //Write to effective address
                readWriteFlag = RwWRITE;
                switch (currentOpcode) {
                    case Cpu6502InstructionDefs.OPCODE_STA_INDEXED_INDIRECT:
                        runSTA();
                        break;
                }
                instructionCycle = 1;
                break;
        }
    }

    private void runCycleIndirectIndexedRead() { //TODO: IMPLEMENT PROPER
        switch (instructionCycle) {
            case 2:
                //Fetch address, increment PC
                effectiveAddress = addressRead(registerProgramCounter); //load ptr into effective address
                registerProgramCounter++;
                instructionCycle++;
                break;
            case 3:
                //load low byte of address
                fixedEffectiveAddress = addressRead(effectiveAddress); //use fixed effective address to not destroy ptr (ugly, change in future?)
                instructionCycle++;
                break;
            
            case 4:
                //load high byte w/ no fix
                fixedEffectiveAddress += addressRead(effectiveAddress+1)<<8;
                //add Y register
                fixedEffectiveAddress = ((fixedEffectiveAddress+registerY)&0xFFFF);
                effectiveAddress = (((fixedEffectiveAddress&0xFF00)+((fixedEffectiveAddress+registerY)&0x00FF))&0xFFFF);
                addressNeedsFix = (effectiveAddress != fixedEffectiveAddress);

                registerProgramCounter++;
                if (!addressNeedsFix)
                    instructionCycle++;
                instructionCycle++;
                break;
            case 5:
                //fix high byte
                addressRead(effectiveAddress);
                instructionCycle++;
                break;
            case 6:
                //Read from effective address
                workingValue = addressRead(fixedEffectiveAddress);
                
                switch (currentOpcode) {
                    case Cpu6502InstructionDefs.OPCODE_ADC_INDIRECT_INDEXED:
                        runADC();
                        break;
                    case Cpu6502InstructionDefs.OPCODE_AND_INDIRECT_INDEXED:
                        runAND();
                        break;
                    case Cpu6502InstructionDefs.OPCODE_CMP_INDIRECT_INDEXED:
                        runCMP();
                        break;
                    case Cpu6502InstructionDefs.OPCODE_EOR_INDIRECT_INDEXED:
                        runEOR();
                        break;
                    case Cpu6502InstructionDefs.OPCODE_LDA_INDIRECT_INDEXED:
                        runLDA();
                        break;
                    case Cpu6502InstructionDefs.OPCODE_ORA_INDIRECT_INDEXED:
                        runORA();
                        break;
                    case Cpu6502InstructionDefs.OPCODE_SBC_INDIRECT_INDEXED:
                        runSBC();
                        break;
                }
                instructionCycle = 1;
                break;
        }
    }
    private void runCycleIndirectIndexedReadModifyWrite() { //TODO: IMPLEMENT PROPER
        switch (instructionCycle) {
            case 2:
                //Fetch address, increment PC
                effectiveAddress = addressRead(registerProgramCounter); //load ptr into effective address
                registerProgramCounter++;
                instructionCycle++;
                break;
            case 3:
                //load low byte of address
                fixedEffectiveAddress = addressRead(effectiveAddress); //use fixed effective address to not destroy ptr (ugly, change in future?)
                instructionCycle++;
                break;
            
            case 4:
                //load high byte w/ no fix
                fixedEffectiveAddress += addressRead(effectiveAddress+1)<<8;
                //add Y register
                fixedEffectiveAddress = ((fixedEffectiveAddress+registerY)&0xFFFF);
                effectiveAddress = (((fixedEffectiveAddress&0xFF00)+((fixedEffectiveAddress+registerY)&0x00FF))&0xFFFF);
                addressNeedsFix = (effectiveAddress != fixedEffectiveAddress);

                registerProgramCounter++;
                instructionCycle++;
                break;
            case 5:
                //fix high byte
                addressRead(effectiveAddress);
                instructionCycle++;
                break;
            case 6:
                //Read from effective address
                addressRead(fixedEffectiveAddress);
                instructionCycle++;
                break;
            case 7:
                //Write value to effective address, then perform operation
                addressWrite(fixedEffectiveAddress, workingValue);
                /*
                switch (currentOpcode) {
                    //No Legal Opcodes
                }
                */
                instructionCycle++;
                break;
            case 8:
                //Write result to effective address
                addressWrite(fixedEffectiveAddress, operationResult);
                instructionCycle = 1;
                break;
        }
    }
    private void runCycleIndirectIndexedWrite() { //TODO: IMPLEMENT PROPER
        switch (instructionCycle) {
            case 2:
                //Fetch address, increment PC
                effectiveAddress = addressRead(registerProgramCounter); //load ptr into effective address
                registerProgramCounter++;
                instructionCycle++;
                break;
            case 3:
                //load low byte of address
                fixedEffectiveAddress = addressRead(effectiveAddress); //use fixed effective address to not destroy ptr (ugly, change in future?)
                instructionCycle++;
                break;
            
            case 4:
                //load high byte w/ no fix
                fixedEffectiveAddress += addressRead(effectiveAddress+1)<<8;
                //add Y register
                fixedEffectiveAddress = ((fixedEffectiveAddress+registerY)&0xFFFF);
                effectiveAddress = (((fixedEffectiveAddress&0xFF00)+((fixedEffectiveAddress+registerY)&0x00FF))&0xFFFF);
                addressNeedsFix = (effectiveAddress != fixedEffectiveAddress);

                registerProgramCounter++;
                instructionCycle++;
                break;
            case 5:
                //fix high byte
                addressRead(effectiveAddress);
                effectiveAddress = fixedEffectiveAddress;
                instructionCycle++;
                break;
            case 6:
                //Write to effective address
                readWriteFlag = RwWRITE;
                switch (currentOpcode) {
                    case Cpu6502InstructionDefs.OPCODE_STA_INDIRECT_INDEXED:
                        runSTA();
                        break;
                }
                instructionCycle = 1;
                break;
        }
    }

    private void runCycleRelative() {
        switch (instructionCycle) {
            case 2:
                //Fetch operand, increment PC
                workingValue = addressRead(registerProgramCounter);
                workingValue = ((workingValue&0x80)*-1)+(workingValue&0x7F); //signed 8 bit int to java signed int
                registerProgramCounter++;
                instructionCycle++;
                break;
            case 3:
                //Determine if branch is taken, 
                switch (currentOpcode) {
                    case Cpu6502InstructionDefs.OPCODE_BPL_RELATIVE:
                        takeBranch = ((registerStatus&STATUS_FLAG_N)==0);
                        break;
                    case Cpu6502InstructionDefs.OPCODE_BMI_RELATIVE:
                        takeBranch = ((registerStatus&STATUS_FLAG_N)!=0);
                        break;
                    case Cpu6502InstructionDefs.OPCODE_BVC_RELATIVE:
                        takeBranch = ((registerStatus&STATUS_FLAG_V)==0);
                        break;
                    case Cpu6502InstructionDefs.OPCODE_BVS_RELATIVE:
                        takeBranch = ((registerStatus&STATUS_FLAG_V)!=0);
                        break;
                    case Cpu6502InstructionDefs.OPCODE_BCC_RELATIVE:
                        takeBranch = ((registerStatus&STATUS_FLAG_C)==0);
                        break;
                    case Cpu6502InstructionDefs.OPCODE_BCS_RELATIVE:
                        takeBranch = ((registerStatus&STATUS_FLAG_C)!=0);
                        break;
                    case Cpu6502InstructionDefs.OPCODE_BNE_RELATIVE:
                        takeBranch = ((registerStatus&STATUS_FLAG_Z)==0);
                        break;
                    case Cpu6502InstructionDefs.OPCODE_BEQ_RELATIVE:
                        takeBranch = ((registerStatus&STATUS_FLAG_Z)!=0);
                        break; 
                    default:
                        takeBranch = false;
                }
                if (takeBranch) {
                    fixedEffectiveAddress = ((registerProgramCounter+workingValue)&0xFFFF);
                    effectiveAddress = ((registerProgramCounter+workingValue)&0x00FF)+(registerProgramCounter&0xFF00);
                    registerProgramCounter = effectiveAddress;
                    instructionCycle++;
                } else {
                //don't take branch
                    instructionCycle = 1;
                }
                break;
            case 4:
                if (effectiveAddress!=fixedEffectiveAddress) {
                    //taking branch, address needs fix
                    effectiveAddress = fixedEffectiveAddress;
                    instructionCycle++;
                }
                else {
                    //taking branch, no fix
                    instructionCycle = 1;
                }
                break;
            case 5:
                instructionCycle = 1;
                break;
        }
    }
    
    //General Instruction Behavior
    
    //Loading Instructions
    public void runLDA() {
        registerAccumulator = workingValue;
        setFlag((registerAccumulator>=0x80), STATUS_FLAG_N);
        setFlag((registerAccumulator==0), STATUS_FLAG_Z);
    }
    
    public void runLDX() {
        registerX = workingValue;
        setFlag((registerX>=0x80), STATUS_FLAG_N);
        setFlag((registerX==0), STATUS_FLAG_Z);
    }
    
    public void runLDY() {
        registerY = workingValue;
        setFlag((registerY>=0x80), STATUS_FLAG_N);
        setFlag((registerY==0), STATUS_FLAG_Z);
    }
    
    //Storing Instructions
    
    public void runSTA() {
        addressWrite(effectiveAddress, registerAccumulator);
    }
    public void runSTX() {
        addressWrite(effectiveAddress, registerX);
    }
    public void runSTY() {
        addressWrite(effectiveAddress, registerY);
    }
    
    //Bitwise Instructions
    public void runAND() {
        registerAccumulator = registerAccumulator&workingValue;
        setFlag((registerAccumulator>=0x80), STATUS_FLAG_N);
        setFlag((registerAccumulator==0), STATUS_FLAG_Z);
    }
    
    public void runEOR() {
        registerAccumulator = registerAccumulator^workingValue;
        setFlag((registerAccumulator>=0x80), STATUS_FLAG_N);
        setFlag((registerAccumulator==0), STATUS_FLAG_Z);
    }
    
    public void runORA() {
        registerAccumulator = registerAccumulator|workingValue;
        setFlag((registerAccumulator>=0x80), STATUS_FLAG_N);
        setFlag((registerAccumulator==0), STATUS_FLAG_Z);
    }
    
    //Math Instructions
    
    public void runASL() {
        setFlag(((workingValue&0x80)==0x80), STATUS_FLAG_C); //Put Bit 7 Into Carry
        operationResult = (workingValue<<1)&0xFF; //Left shift Accumulator
        setFlag((operationResult>=0x80), STATUS_FLAG_N);
        setFlag((operationResult==0), STATUS_FLAG_Z);
    }
    
    public void runLSR() {
        setFlag(((workingValue&0x01)==0x01), STATUS_FLAG_C); //Put Bit 0 Into Carry
        operationResult = (workingValue>>1)&0xFF; //Right shift Accumulator
        setFlag((operationResult>=0x80), STATUS_FLAG_N);
        setFlag((operationResult==0), STATUS_FLAG_Z);
    }
    
    public void runROL() {
        int carryVal = (((registerStatus&STATUS_FLAG_C)==STATUS_FLAG_C) ? 0x01:0);
        setFlag(((workingValue&0x80)==0x80), STATUS_FLAG_C); //Put Bit 7 Into Carry
        operationResult = (workingValue<<1)&0xFF; //Left shift Accumulator
        operationResult += carryVal; //Shift in carry bit
        setFlag((operationResult>=0x80), STATUS_FLAG_N);
        setFlag((operationResult==0), STATUS_FLAG_Z);
    }
    
    public void runROR() {
        int carryVal = (((registerStatus&STATUS_FLAG_C)==STATUS_FLAG_C) ? 0x80:0);
        setFlag(((workingValue&0x01)==0x01), STATUS_FLAG_C); //Put Bit 0 Into Carry
        operationResult = (workingValue>>1)&0xFF; //Right shift Accumulator
        operationResult += carryVal; //Shift in carry bit
        setFlag((operationResult>=0x80), STATUS_FLAG_N);
        setFlag((operationResult==0), STATUS_FLAG_Z);
    }
    
    public void runINC() {
        operationResult = ((workingValue+1)&0xFF);
        setFlag((operationResult>=0x80), STATUS_FLAG_N);
        setFlag((operationResult==0), STATUS_FLAG_Z);
    }
    
    public void runDEC() {
        operationResult = ((workingValue-1)&0xFF);
        setFlag((operationResult>=0x80), STATUS_FLAG_N);
        setFlag((operationResult==0), STATUS_FLAG_Z);
    }
    
    public void runADC() {
        int carryVal = (((registerStatus&STATUS_FLAG_C)==STATUS_FLAG_C) ? 1:0);
        if ((registerStatus&STATUS_FLAG_D)==STATUS_FLAG_D) {
        	//DECIMAL MODE
        	int	sum	= (registerAccumulator&0x0F) + (workingValue&0x0F) + carryVal;
        	if (sum>0x09) sum =	sum+0x06; //(sum-0x0A+0x10)
        	sum	= sum +	(registerAccumulator&0xF0) + (workingValue&0xF0);
        	setFlag(((((sum&0xF0)^registerAccumulator)&0x80)==0x80 && !(((registerAccumulator^workingValue)&0x80)==0x80)), STATUS_FLAG_V);
        	if (sum>0x99) {
        		setFlag(true, STATUS_FLAG_C);
        		sum	= sum-0xA0;
        	}
        	else {
        		setFlag(false, STATUS_FLAG_C);
        	}
        	registerAccumulator	= (sum&0xFF);
            setFlag((registerAccumulator>=0x80), STATUS_FLAG_N);
        }
        else {
        	//BINARY MODE
            
        	int	sum	= registerAccumulator +	workingValue + carryVal;
        	setFlag((sum>255), STATUS_FLAG_C);
        	int	sumVal1	= (registerAccumulator&0x7F) + ((registerAccumulator&0x80)==0x80 ? -128:0);
        	int	sumVal2	= (workingValue&0x7F) +	((workingValue&0x80)==0x80 ? -128:0);
        	int	signedSum =	sumVal1	+ sumVal2 + carryVal;
        	setFlag((signedSum < -128 || signedSum > 127), STATUS_FLAG_V);
        	registerAccumulator	= (sum&0xFF);
        	setFlag((registerAccumulator>=0x80), STATUS_FLAG_N);
        }
        setFlag((registerAccumulator==0), STATUS_FLAG_Z);
    }
    
    public void runSBC() {
        int carryVal = (((registerStatus&STATUS_FLAG_C)==STATUS_FLAG_C) ? 1:0);
        if ((registerStatus&STATUS_FLAG_D)==STATUS_FLAG_D) {
        	//DECIMAL MODE
            int	dif	= (registerAccumulator&0x0F) - (workingValue&0x0F) - 1 + carryVal;
        	if (dif<0x00) dif =	dif-0x06; //(sum-0x0A+0x10)
        	dif	= dif +	(registerAccumulator&0xF0) - (workingValue&0xF0);
        	if (dif<0x00) {
        		setFlag(false, STATUS_FLAG_C);
        		dif	= dif-0x60;
        	}
        	else {
        		setFlag(true, STATUS_FLAG_C);
        	}
            int flippedCarryVal = (((registerStatus&STATUS_FLAG_C)==STATUS_FLAG_C) ? 1:0);
            setFlag(
                (((registerAccumulator-workingValue-flippedCarryVal)&0x80)==0x80 
                && ((registerAccumulator^workingValue)&0x80)==0x80), 
                STATUS_FLAG_V);
        	registerAccumulator	= (dif&0xFF);
            setFlag((registerAccumulator>=0x80), STATUS_FLAG_N);
        }
        else {
        	//BINARY MODE
        	int	dif	= registerAccumulator -	workingValue -1 + carryVal;
        	setFlag((dif>=0), STATUS_FLAG_C);
        	int	sumVal1	= (registerAccumulator&0x7F) + ((registerAccumulator&0x80)==0x80 ? -128:0);
        	int	sumVal2	= (workingValue&0x7F) +	((workingValue&0x80)==0x80 ? -128:0);
        	int	signedSum =	sumVal1	- sumVal2 -1 + carryVal;
        	setFlag((signedSum < -128 || signedSum > 127), STATUS_FLAG_V);
        	registerAccumulator	= (dif&0xFF);
        	setFlag((registerAccumulator>=0x80), STATUS_FLAG_N);
        }
        setFlag((registerAccumulator==0), STATUS_FLAG_Z);
    }
    
    //Comparison Instructions
    
    public void runBIT() {
        int andVal = registerAccumulator & workingValue;
        setFlag(((andVal&0x40)==0x40), STATUS_FLAG_V);
        setFlag(((andVal&0x80)==0x80), STATUS_FLAG_N);
        setFlag((andVal==0), STATUS_FLAG_Z);
        
    }
    
    public void runCMP() {
    	int	dif	= registerAccumulator -	workingValue;
    	setFlag((dif>=0), STATUS_FLAG_C); //Effectively "Accumulator is more than or equal to"
    	setFlag(((dif&0x80)==0x80), STATUS_FLAG_N);
        setFlag((dif==0), STATUS_FLAG_Z); //Effectively "Accumulator equals"
    }
    
    public void runCPX() {
    	int	dif	= registerX - workingValue;
    	setFlag((dif>=0), STATUS_FLAG_C); //Effectively "X is more than or equal to"
    	setFlag(((dif&0x80)==0x80), STATUS_FLAG_N);
        setFlag((dif==0), STATUS_FLAG_Z); //Effectively "X equals"
    }
    
    public void runCPY() {
    	int	dif	= registerY - workingValue;
    	setFlag((dif>=0), STATUS_FLAG_C); //Effectively "Y is more than or equal to"
    	setFlag(((dif&0x80)==0x80), STATUS_FLAG_N);
        setFlag((dif==0), STATUS_FLAG_Z); //Effectively "Y equals"
    }
    
    
    

    /**********************************************************
    **              INTERNAL OPERATION METHODS               **
    **        (used for repeated CPU functionality)          **
    **                                                       **
    **********************************************************/

    public void pushStack(int value) {
        addressWrite(0x100+registerStackPointer, value);
        registerStackPointer = (registerStackPointer-1)&0xFF;
    }
    
    public int addressRead(int address) {
        addressLine = address;
        return emulatorSystem.readBus(dataBusID, addressBusID, address);
    }
    
    public void addressWrite(int address, int value) {
        emulatorSystem.writeBus(dataBusID, addressBusID, address, value);
        addressLine = address;
        dataLine = value;
    }
    
    public void setFlag(boolean state, int flag) {
        if (flag!=STATUS_FLAG_U) {
            if (state==true) {
                registerStatus = registerStatus|flag;
            }
            else {
                registerStatus = (registerStatus|flag)-flag;
            }
        }
    }

    /**********************************************************
    **                     I/O METHODS                       **
    **     (methods for communication with the system)       **
    **                                                       **
    **********************************************************/
    public void interruptIRQ() { //Method for when a maskable interrupt occurs (Incomplete)
        registerProgramCounter = addressRead(VectorIRQ)+(addressRead(VectorIRQ+1)<<8);
        debugPrintStatus();
    }
    public void interruptRESET() { //Method for when a reset occurs (Incomplete)
        registerProgramCounter = addressRead(VectorRESET)+(addressRead(VectorRESET+1)<<8);
        instructionCycle = 1;
        registerAccumulator = 0;
        registerX = 0;
        registerY = 0;
        registerStackPointer = 0xFF;
        registerStatus = (registerStatus&STATUS_FLAG_B)+STATUS_FLAG_U; //Maintain B flag, U always set
        cycleCount = 0;
        
        debugPrintStatus();
    }
    public void interruptNMI() { //Method for when a non-maskable interrupt occurs (Incomplete)
        registerProgramCounter = addressRead(VectorNMI)+(addressRead(VectorNMI+1)<<8);

        debugPrintStatus();
    }

    /**********************************************************
    **                     DEBUG METHODS                     **
    **            (used for debugging and testing)           **
    **                                                       **
    **********************************************************/

    public void debugPrintStatus() {
        System.out.print("Cycle: ");
        System.out.print(LibBaseConversion.toHexFixed((int) cycleCount, 8));
        System.out.print("  Next Op-Cycle: ");
        System.out.print(instructionCycle);
        System.out.print("  Instruction: ");
        System.out.print(LibBaseConversion.toHexFixed(currentOpcode, 2));
        System.out.print("  regPC: ");
        System.out.print(LibBaseConversion.toHexFixed(registerProgramCounter, 4));
        System.out.print("  regA: ");
        System.out.print(LibBaseConversion.toHexFixed(registerAccumulator, 2));
        System.out.print("  regX: ");
        System.out.print(LibBaseConversion.toHexFixed(registerX, 2));
        System.out.print("  regY: ");
        System.out.print(LibBaseConversion.toHexFixed(registerY, 2));
        System.out.print("  regP: ");
        System.out.print(LibBaseConversion.toBinaryFixed(registerStatus, 8));
        System.out.print("  regS: ");
        System.out.print(LibBaseConversion.toHexFixed(registerStackPointer, 2));
        System.out.print("  R/W: ");
        if (readWriteFlag) System.out.print("R");
        else System.out.print("W");
        System.out.println();
    }
    public int getCurrentOpcode() {return currentOpcode;}
}
