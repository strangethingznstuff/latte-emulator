package resources;

public class App {
   public static void main(String[] args) throws Exception {
      LatteEmulator emulatorContainer = new LatteEmulator();
      emulatorContainer.init();
      emulatorContainer.start();
   }
}