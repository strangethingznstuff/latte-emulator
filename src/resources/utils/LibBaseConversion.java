//Library for converting numbers to 

package resources.utils;

public class LibBaseConversion {
    

    public static String toHexFixed(int number, int length) {
        String strPart = Integer.toHexString(number);
        if (length<=strPart.length()) {
            return strPart.substring(strPart.length()-length).toUpperCase();
        }
        else {
            String resultStr = "";
            for (int i=0;i<length-strPart.length();i++) {resultStr += "0";}
            resultStr += strPart;
            return resultStr.toUpperCase();
        }
    }


    public static String toBinaryFixed(int number, int length) {
        String strPart = Integer.toBinaryString(number);
        if (length<=strPart.length()) {
            return strPart.substring(strPart.length()-length);
        }
        else {
            String resultStr = "";
            for (int i=0;i<length-strPart.length();i++) {resultStr += "0";}
            resultStr += strPart;
            return resultStr;
        }
    }

    public static int getAssemblyNumberFromText(String text) {

        switch (text.charAt(0)) {
            case '$':
                text = text.substring(1);
                return Integer.valueOf(text, 16);
            case '%':
                text = text.substring(1);
                return Integer.valueOf(text, 2);
            default:
                return Integer.valueOf(text);
        }
    }
}
