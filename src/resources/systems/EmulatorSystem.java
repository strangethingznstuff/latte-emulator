package resources.systems;

public abstract class EmulatorSystem {
    abstract public int readBus(int dataBusID, int addressBusID, int address);
    abstract public void writeBus(int dataBusID, int addressBusID, int address, int data);
    abstract public void test();
    abstract public void update();
    abstract public void restartSystem();
}
