package resources.systems.system_easy6502;

//Imports
import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;
import java.awt.image.BufferedImage;
import javax.swing.*;

import resources.LatteEmulator;
import resources.cpu6502resources.*;
import resources.frontend.LatteMenu;
import resources.frontend.UserInputHandler;
import resources.utils.LibBaseConversion;

public class Easy6502Menu extends LatteMenu implements ActionListener {
    LatteEmulator emulator;
    Easy6502Assembler assembler = new Easy6502Assembler();
    Easy6502System system;
    

    int ramViewerStartAddress = 0x0000;
    int ramViewerLength = 0x100;

    //UI

    final String MESSAGE_WARNING_INVALID_RAM_VIEWER_PARAMS = "Warning: Ram viewer params are empty or invalid";
    final String MESSAGE_RAM_VIEWER_PARAMS_FIXED = "";

    JLabel cycleCounterLabel = new JLabel();
    JLabel programCounterLabel = new JLabel();
    JLabel programInstructionLabel = new JLabel();
    JLabel dataRegistersLabel = new JLabel();
    JLabel statusReferenceLabel = new JLabel();
    JLabel statusRegisterLabel = new JLabel();
    JButton assembleButton = new JButton("Assemble");
    JButton cycleButton = new JButton("Run Cycle");
    JButton resetButton = new JButton("Reset");
    JButton runButton = new JButton("Run");
    JTextArea assemblyTextArea = new JTextArea();
    JScrollPane assemblyScrollPane = new JScrollPane(assemblyTextArea, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
    JTextArea ramViewerTextArea = new JTextArea();
    JScrollPane ramViewerScrollPane = new JScrollPane(ramViewerTextArea, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
    JLabel ramViewerStartAddressLabel = new JLabel("Start: $");
    JTextField ramViewerStartAddressTextField = new JTextField(Integer.toHexString(ramViewerStartAddress).toUpperCase());
    JLabel ramViewerLengthLabel = new JLabel("Length: $");
    JTextField ramViewerLengthTextField = new JTextField(Integer.toHexString(ramViewerLength).toUpperCase());
    JTextField statusMessageTextField = new JTextField("Based on Easy6502 by Nick Morgan AKA skilldrick on Github");
    Easy6502Display display = new Easy6502Display();


    


    Easy6502Menu (LatteEmulator emulator, Easy6502System system) {
        //Fetch emulator and system object
        this.emulator = emulator;
        this.system = (Easy6502System) emulator.emulatorSystem;
        //Create Window
        emulator.window = new JFrame("Latte 6502");
        emulator.window.pack();
        emulator.setMainWindowRes(640, 480);
        emulator.window.setResizable(false);
        emulator.window.setLocationRelativeTo(null);
        emulator.window.setLayout(null);
        emulator.window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        emulator.window.setVisible(true);
        //Add Input Handler
        emulator.userInputHandler = new UserInputHandler(emulator);
        //Create User Interface
        emulator.window.add(cycleCounterLabel);
        emulator.window.add(programCounterLabel);
        emulator.window.add(programInstructionLabel);
        emulator.window.add(dataRegistersLabel);
        emulator.window.add(statusReferenceLabel);
        emulator.window.add(statusRegisterLabel);
        emulator.window.add(assembleButton);
        emulator.window.add(cycleButton);
        emulator.window.add(resetButton);
        emulator.window.add(assemblyScrollPane);
        emulator.window.add(ramViewerScrollPane);
        emulator.window.add(ramViewerStartAddressTextField);
        emulator.window.add(ramViewerStartAddressLabel);
        emulator.window.add(ramViewerLengthLabel);
        emulator.window.add(ramViewerLengthTextField);
        emulator.window.add(statusMessageTextField);
        emulator.window.add(runButton);
        emulator.window.add(display);
        //emulator.window.pack();
        
        assembleButton.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                assembleCode();
            }
        });
        assembleButton.setBounds(50,20,95,30);

        cycleButton.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                system.cpu.runCycle();
            }
        });
        cycleButton.setBounds(155,20,95,30);

        resetButton.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                system.cpu.setFlag(false, Cpu6502.STATUS_FLAG_B);
                system.cpu.interruptRESET();
            }
        });
        resetButton.setBounds(260,20,95,30);

        runButton.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                system.processorRunning = true;
            }
        });
        runButton.setBounds(365,20,95,30);
        

        assemblyScrollPane.setBounds(50,60, 380,200);
        assemblyTextArea.setFont(new Font("Courier", Font.PLAIN,  12));

        ramViewerScrollPane.setBounds(50,300, 390,80);
        ramViewerTextArea.setEditable(false);
        ramViewerTextArea.setBackground(Color.LIGHT_GRAY);
        ramViewerStartAddressLabel.setBounds(50,270, 60,20);
        ramViewerStartAddressTextField.setBounds(100,270, 60,20);
        ramViewerLengthLabel.setBounds(170,270, 60,20);
        ramViewerLengthTextField.setBounds(230,270, 60,20);
        statusMessageTextField.setBounds(50,390, 390,20);
        statusMessageTextField.setEditable(false);
        statusMessageTextField.setBackground(Color.LIGHT_GRAY);

        cycleCounterLabel.setBounds((int) 450,(int) 300,(int) 160,(int) 20);
        programCounterLabel.setBounds((int) 450,(int) 320,(int) 160,(int) 20);
        programInstructionLabel.setBounds((int) 450,(int) 340,(int) 160,(int) 20);
        dataRegistersLabel.setBounds((int) 450,(int) 360,(int) 160,(int) 20);
        statusReferenceLabel.setBounds((int) 506,(int) 380,(int) 160,(int) 20);
        statusRegisterLabel.setBounds((int) 450,(int) 394,(int) 160,(int) 20);
    }

    public void update() {
        Easy6502System system = (Easy6502System) emulator.emulatorSystem;
        
        //Register and System status Labels
        String cycleCounterText = "Cycle Count: ";
        cycleCounterText +=  LibBaseConversion.toHexFixed((int) system.cpu.cycleCount, 8);
        cycleCounterLabel.setText(cycleCounterText);
        

        String programCounterText = "PC: ";
        programCounterText +=  LibBaseConversion.toHexFixed(system.cpu.registerProgramCounter, 4);
        programCounterLabel.setText(programCounterText);
        

        String instructionLabelText = "Instruction: ";
        instructionLabelText += Cpu6502InstructionDefs.opcodeToString(system.cpu.currentOpcode);
        programInstructionLabel.setText(instructionLabelText);
        

        String dataRegistersText = "A: ";
        dataRegistersText +=  LibBaseConversion.toHexFixed(system.cpu.registerAccumulator, 2);
        dataRegistersText += "  X: ";
        dataRegistersText +=  LibBaseConversion.toHexFixed(system.cpu.registerX, 2);
        dataRegistersText += "  Y: ";
        dataRegistersText +=  LibBaseConversion.toHexFixed(system.cpu.registerY, 2);
        dataRegistersText += "  S: ";
        dataRegistersText +=  LibBaseConversion.toHexFixed(system.cpu.registerStackPointer, 2);
        dataRegistersLabel.setText(dataRegistersText);
        
        String statusReferenceText = "NV-BDIZC";
        statusReferenceLabel.setText(statusReferenceText);
        statusReferenceLabel.setFont(new Font("Courier", Font.PLAIN,  12));

        String statusRegisterText = "Status: ";
        statusRegisterText +=  LibBaseConversion.toBinaryFixed(system.cpu.registerStatus, 8);
        statusRegisterLabel.setText(statusRegisterText);
        statusRegisterLabel.setFont(new Font("Courier", Font.PLAIN,  12));
        

        try {
            ramViewerStartAddress = Integer.parseInt(ramViewerStartAddressTextField.getText(),16);
            ramViewerLength = Integer.parseInt(ramViewerLengthTextField.getText(),16);
            if (statusMessageTextField.getText().equals(MESSAGE_WARNING_INVALID_RAM_VIEWER_PARAMS)) {
                statusMessageTextField.setText(MESSAGE_RAM_VIEWER_PARAMS_FIXED);
            }
        }
        catch(Exception e) {
            statusMessageTextField.setText(MESSAGE_WARNING_INVALID_RAM_VIEWER_PARAMS);
        }
        ramViewerTextArea.setFont(new Font("Consolas", Font.PLAIN,  12));
        ramViewerTextArea.setText(getRamViewerText());
        
        //Update Image Display
        Graphics g = emulator.window.getGraphics();
        //display.update(g);
        //display.setEnabled(true);
        display.repaint();
        display.setOpaque(true);
    }

    public void destroyMenu() {
        
    }

    private String getRamViewerText() {
        Easy6502System system = (Easy6502System) emulator.emulatorSystem;

        if (ramViewerStartAddress<0) {
            return "Invalid RAM range";
        }
        if (ramViewerStartAddress+ramViewerLength>0x10000) {
            return "Invalid RAM range";
        }
        
        String ramString = LibBaseConversion.toHexFixed(ramViewerStartAddress, 4) + ":";
        int bytePerLineCounter = 0;
        for (int byteCount = 0; byteCount<ramViewerLength; byteCount++) {
            if (bytePerLineCounter==16) {
                ramString += "\n" + LibBaseConversion.toHexFixed(ramViewerStartAddress+byteCount, 4) + ":";
                bytePerLineCounter = 0;
            }
            ramString += LibBaseConversion.toHexFixed(system.readBusEasy6502(ramViewerStartAddress+byteCount), 2) + " ";
            bytePerLineCounter++;
        }
        return ramString;
        //return "Unable to get RAM";
    }

    private void assembleCode() {
        Easy6502System system = (Easy6502System) emulator.emulatorSystem;
        system.restartSystem();
        int[] codeData = Easy6502Assembler.assembleCode(assemblyTextArea.getText(), 0x600);
        for (int i=0; i<codeData.length; i++) {
            system.writeBusEasy6502(0x600+i, codeData[i]);
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        // Just here to make Java happy
        
    }
}
