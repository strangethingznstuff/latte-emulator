package resources.systems.system_easy6502;

import resources.systems.EmulatorSystem;
import resources.LatteEmulator;
import resources.components.ComponentDRAM;

//Original Easy6502 by Nick Morgan AKA skilldrick on Github 
//Github Link: https://github.com/skilldrick/easy6502
//Features adapted based roughly on behavior

//Spec
//  -64kb RAM
//  -MOS 6502 CPU
import resources.cpu6502resources.*;
import resources.frontend.EmulatorMenuState;

public class Easy6502System extends EmulatorSystem {
    //Components
    LatteEmulator emulator;
    Cpu6502 cpu;
    ComponentDRAM systemRam;
    boolean processorRunning = false;
    
    //For more advanced emulation, improve timing system!
    int cyclesPerUpdate; //Cpu cycles ran per update call
    int cyclesPerSecond = 2000000; //Cycles that should be executed each second

    public Easy6502System(LatteEmulator emulator) {
        this.emulator = emulator;
        cpu = new Cpu6502(this,0,0);
        systemRam = new ComponentDRAM(0x10000);

        //Set Menu
        emulator.menuState = EmulatorMenuState.EASY6502;
        emulator.menu = new Easy6502Menu(emulator, this);
        cyclesPerUpdate = cyclesPerSecond/(1000/emulator.frameDeltaMilliseconds);
    }

    public void restartSystem() {
        cpu = new Cpu6502(this,0,0);
        systemRam = new ComponentDRAM(0x10000);
        systemRam.writeByte(cpu.VectorRESET, 0x00);
        systemRam.writeByte(cpu.VectorRESET+1, 0x06);
        cpu.interruptRESET();
    }
    
    //Update
    public void update() {
        if (processorRunning){
            for (int cycleIndex=0; cycleIndex<cyclesPerUpdate; cycleIndex++) {
                cpu.runCycle();
                if (cpu.currentOpcode==0) {
                    processorRunning = false;
                    System.out.println("Status on BRK");
                    cpu.debugPrintStatus();
                    break;
                }
            }
        }
    }
    
    //Test
    public void test() {
        //Set some data in DRAM
        systemRam.writeByte(0x0600, 0xA9);
        systemRam.writeByte(0x0601, 0x96);

        //Set Reset vector
        systemRam.writeByte(cpu.VectorRESET, 0x00);
        systemRam.writeByte(cpu.VectorRESET+1, 0x06);

        cpu.interruptRESET();
        for (int i=0; i<0; i++) {
            cpu.runCycle();
        };
    }



    //Bus Handling
    public int readBus(int dataBusID, int addressBusID, int address) {
        switch (dataBusID) {
            case 0:
                return readBusEasy6502(address);
        }
        return 0;
    }
    public void writeBus(int dataBusID, int addressBusID, int address, int data) {
        switch (dataBusID) {
            case 0:
                writeBusEasy6502(address, data);
        }
    }

    //Virtual Easy6502 bus
    public int readBusEasy6502(int address) {
        return systemRam.readByte(address);
    }
    public void writeBusEasy6502(int address, int data) {
        systemRam.writeByte(address, data);
    }
}
