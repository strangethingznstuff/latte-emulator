package resources.systems.system_easy6502;

import java.util.*;

import resources.cpu6502resources.*;
import resources.utils.LibBaseConversion;


public class Easy6502Assembler {
    
    public enum LineType {
        INSTRUCTION,
        LABEL,
        DEFINITION,
        BLANK,
        WRONG
    }


    static int[] assembleCode(String codeString, int codeStartAddress) {
        //Split lines -into separate strings
        try {
            String[] codeLines = separateLines(codeString);
            codeLines = simplifyLines(codeLines);
            LineType[] lineTypes = categorizeLines(codeLines);
            Map<String, String> definitionMap = getDefinitionMap(codeLines, lineTypes);
            Cpu6502.AddressingMode[] addressingModes = getAddressingModes(codeLines, lineTypes, definitionMap);
            Map<String, String> labelAddressMap = getAddressMap(codeLines, lineTypes, addressingModes, codeStartAddress);
            int[] codeData = getCodeData(codeLines, lineTypes, addressingModes, definitionMap, labelAddressMap, codeStartAddress);
            return codeData; //Replace with end result
        }
        catch (Exception e) {
            e.printStackTrace();
            return new int[]{};
        }
    }

    private static int[] getCodeData(
    String[] codeLines, 
    Easy6502Assembler.LineType[] lineTypes,
    Cpu6502.AddressingMode[] addressingModes, 
    Map<String, String> definitionMap,
    Map<String, String> addressMap,
    int codeStartAddress
        ) {
        System.out.println("Combine info to make code data:");
        ArrayList<Integer> codeData = new ArrayList<Integer>();
        int nextCodeAddressOffset = codeStartAddress;
        for (int lineIndex=0; lineIndex<codeLines.length; lineIndex++) {
            if (lineTypes[lineIndex]==LineType.INSTRUCTION) {
                String line = codeLines[lineIndex];
                int instructionOpcode = Cpu6502InstructionDefs.getOpcode(line.substring(0,3), addressingModes[lineIndex]);
                nextCodeAddressOffset += Cpu6502InstructionDefs.getInstructionLength(addressingModes[lineIndex]);
                System.out.print("Instruction Opcode: $");
                System.out.println(LibBaseConversion.toHexFixed(instructionOpcode, 2));
                codeData.add(instructionOpcode);
                if (
                    Cpu6502InstructionDefs.getAddressingMode(instructionOpcode) == Cpu6502.AddressingMode.ACCUMULATOR ||
                    Cpu6502InstructionDefs.getAddressingMode(instructionOpcode) == Cpu6502.AddressingMode.IMPLIED
                    ) {
                    continue;
                }
                //Get and covert opperand
                String operandString = getOperand(line);
                String operandValueString = operandString;
                if (Character.isLetter(operandString.charAt(0))) {
                    operandValueString = replaceOperandDefinitionAndLabel(operandString, definitionMap, addressMap);
                    System.out.print("Operand (Converted): ");
                    System.out.println(operandValueString);
                }
                int byteLongData;
                int twoByteDataLow;
                int twoByteDataHigh;
                switch (Cpu6502InstructionDefs.getAddressingMode(instructionOpcode)) {
                    case ACCUMULATOR:
                        //Don't add extra data
                        break;
                    case IMPLIED:
                        //Don't add extra data
                        break;
                    case IMMEDIATE:
                        byteLongData = LibBaseConversion.getAssemblyNumberFromText(operandValueString) & 0xFF;
                        System.out.print("Immediate data: $");
                        System.out.println(LibBaseConversion.toHexFixed(byteLongData, 2));
                        codeData.add(byteLongData);
                        break;
                    case ZERO_PAGE:
                        byteLongData = LibBaseConversion.getAssemblyNumberFromText(operandValueString) & 0xFF;
                        System.out.print("Offset data: $");
                        System.out.println(LibBaseConversion.toHexFixed(byteLongData, 2));
                        codeData.add(byteLongData);
                        break;
                    case ZERO_PAGE_INDEXED_X:
                        byteLongData = LibBaseConversion.getAssemblyNumberFromText(operandValueString) & 0xFF;
                        System.out.print("Offset data: $");
                        System.out.println(LibBaseConversion.toHexFixed(byteLongData, 2));
                        codeData.add(byteLongData);
                        break;
                    case ZERO_PAGE_INDEXED_Y:
                        byteLongData = LibBaseConversion.getAssemblyNumberFromText(operandValueString) & 0xFF;
                        System.out.print("Offset data: $");
                        System.out.println(LibBaseConversion.toHexFixed(byteLongData, 2));
                        codeData.add(byteLongData);
                        break;
                    case ABSOLUTE:
                        twoByteDataLow = LibBaseConversion.getAssemblyNumberFromText(operandValueString) & 0x00FF;
                        System.out.print("Address data Low: $");
                        System.out.println(LibBaseConversion.toHexFixed(twoByteDataLow, 2));
                        codeData.add(twoByteDataLow);
                        twoByteDataHigh = (LibBaseConversion.getAssemblyNumberFromText(operandValueString) & 0xFF00) >> 8;
                        System.out.print("Address data High: $");
                        System.out.println(LibBaseConversion.toHexFixed(twoByteDataHigh, 2));
                        codeData.add(twoByteDataHigh);
                        break;
                    case ABSOLUTE_INDEXED_X:
                        twoByteDataLow = LibBaseConversion.getAssemblyNumberFromText(operandValueString) & 0x00FF;
                        System.out.print("Address data Low: $");
                        System.out.println(LibBaseConversion.toHexFixed(twoByteDataLow, 2));
                        codeData.add(twoByteDataLow);
                        twoByteDataHigh = (LibBaseConversion.getAssemblyNumberFromText(operandValueString) & 0xFF00) >> 8;
                        System.out.print("Address data High: $");
                        System.out.println(LibBaseConversion.toHexFixed(twoByteDataHigh, 2));
                        codeData.add(twoByteDataHigh);
                        break;
                    case ABSOLUTE_INDEXED_Y:
                        twoByteDataLow = LibBaseConversion.getAssemblyNumberFromText(operandValueString) & 0x00FF;
                        System.out.print("Address data Low: $");
                        System.out.println(LibBaseConversion.toHexFixed(twoByteDataLow, 2));
                        codeData.add(twoByteDataLow);
                        twoByteDataHigh = (LibBaseConversion.getAssemblyNumberFromText(operandValueString) & 0xFF00) >> 8;
                        System.out.print("Address data High: $");
                        System.out.println(LibBaseConversion.toHexFixed(twoByteDataHigh, 2));
                        codeData.add(twoByteDataHigh);
                        break;
                    case RELATIVE:   //IMPLEMENT THIS!!!!
                        int desiredAddress = LibBaseConversion.getAssemblyNumberFromText(operandValueString);
                        int addressDifference = desiredAddress-nextCodeAddressOffset;
                        System.out.print("Branch offset (decimal): ");
                        System.out.println(addressDifference);
                        if (addressDifference<-128||addressDifference>127) System.err.println("WARNING: Branch too far apart!");
                        byteLongData = addressDifference;
                        if (addressDifference<0) byteLongData = 256+addressDifference;
                        System.out.print("Offset data: $");
                        System.out.println(LibBaseConversion.toHexFixed(byteLongData, 2));
                        codeData.add(byteLongData);
                        break;
                    case INDEXED_INDIRECT:
                        byteLongData = LibBaseConversion.getAssemblyNumberFromText(operandValueString) & 0xFF;
                        System.out.print("Offset data: $");
                        System.out.println(LibBaseConversion.toHexFixed(byteLongData, 2));
                        codeData.add(byteLongData);
                        break;
                    case INDIRECT_INDEXED:
                        byteLongData = LibBaseConversion.getAssemblyNumberFromText(operandValueString) & 0xFF;
                        System.out.print("Offset data: $");
                        System.out.println(LibBaseConversion.toHexFixed(byteLongData, 2));
                        codeData.add(byteLongData);
                        break;
                    case INDIRECT:
                        twoByteDataLow = LibBaseConversion.getAssemblyNumberFromText(operandValueString) & 0x00FF;
                        System.out.print("Address data Low: $");
                        System.out.println(LibBaseConversion.toHexFixed(twoByteDataLow, 2));
                        codeData.add(twoByteDataLow);
                        twoByteDataHigh = (LibBaseConversion.getAssemblyNumberFromText(operandValueString) & 0xFF00) >> 8;
                        System.out.print("Address data High: $");
                        System.out.println(LibBaseConversion.toHexFixed(twoByteDataHigh, 2));
                        codeData.add(twoByteDataHigh);
                        break;
                    case UNKNOWN:
                        //Don't add extra data
                        break;
                }
            }
        }
        int[] codeDataArray = new int[codeData.size()];
        for (int i=0; i<codeDataArray.length; i++) {
            codeDataArray[i] = codeData.get(i);
        }
        
        return codeDataArray;
    }

    static String[] separateLines(String string) {
        int lineCount = 1;
        for (int charIndex=0; charIndex<string.length(); charIndex++) {
            if (string.charAt(charIndex)=='\n') {
                lineCount++;
            }
        }

        String[] resultLines = new String[lineCount];
        int lineIndex = 0;
        String currentString = "";

        for (int charIndex=0; charIndex<string.length(); charIndex++) {
            if (string.charAt(charIndex)=='\n') {
                resultLines[lineIndex] = currentString;
                currentString = "";
                lineIndex++;
            }
            else {
                currentString += string.charAt(charIndex);
            }
        }
        resultLines[resultLines.length-1] = currentString;
        return resultLines;
    }
    static String[] simplifyLines(String[] lines) {
        System.out.println("Remove whitespace and comments from both ends:");
        for (int lineIndex=0; lineIndex<lines.length; lineIndex++) {
            String currentString = "";
            boolean inComment = false;
            for (int charIndex=0; charIndex<lines[lineIndex].length(); charIndex++) {
                if (lines[lineIndex].charAt(charIndex)==';') inComment = true;
                if (!inComment) currentString += lines[lineIndex].charAt(charIndex);
            }
            currentString = currentString.trim();
            lines[lineIndex] = currentString;
            System.out.println(currentString);
        }
        return lines;
    }

    /*
     * func simplifyLines(_ lines: [String]) -> [String] {
     *      lines
     *          .filter { "" != $0.trimmingCharacters(in: .whitespacesAndNewlines) }
     *          .filter { !$0.trimmingCharacters(in: .whitespacesAndNewlines).hasPrefix(";") }
     * }
     */

    static LineType[] categorizeLines(String[] lines) {
        System.out.println("Assign type to each line:");
        LineType[] resultTypes = new LineType[lines.length];
        for (int lineIndex=0; lineIndex<lines.length; lineIndex++) {
            resultTypes[lineIndex] = getLineType(lines[lineIndex]);
            System.out.println(resultTypes[lineIndex]);
        }
        return resultTypes;
    }

    static LineType getLineType(String line) {
        if (line.equals("")) {
            return LineType.BLANK;
        }
        if (
            line.charAt(line.length()-1)==':'
        ) {
            return LineType.LABEL;
        }
        if (
            line.length()>=7 &&
            line.substring(0, 7).toLowerCase().equals("define ")
        ) {
            return LineType.DEFINITION;
        }
        if (
            line.length() == 3 &&
            Character.isLetter(line.charAt(0)) &&
            Character.isLetter(line.charAt(1)) &&
            Character.isLetter(line.charAt(2))

        ) {
            return LineType.INSTRUCTION;
        }
        if (
            line.length() >= 4 &&
            Character.isLetter(line.charAt(0)) &&
            Character.isLetter(line.charAt(1)) &&
            Character.isLetter(line.charAt(2)) &&
            line.charAt(3) == ' '
        ) {
            return LineType.INSTRUCTION;
        }
        return LineType.WRONG;
    }

    static Map<String, String> getDefinitionMap(String[] lines, LineType[] lineTypes) {
        System.out.println("Add definitions to map of values:");
        Map<String, String> definitionMap = new HashMap<String, String>();
        for (int lineIndex=0; lineIndex<lines.length; lineIndex++) {
            if (lineTypes[lineIndex]==LineType.DEFINITION) {
                String currentLine = lines[lineIndex];
                currentLine = currentLine.substring(7).trim();
                String definitionName = currentLine.substring(0,currentLine.indexOf(' '));
                System.out.println(definitionName);
                currentLine = currentLine.substring(currentLine.indexOf(' ')).trim();
                String definitionValue = currentLine;
                System.out.println(definitionValue);
                definitionMap.put(definitionName, definitionValue);
            }
        }
        return definitionMap;
    }

    static Map<String, String> getAddressMap(String[] lines, LineType[] lineTypes, Cpu6502.AddressingMode[] addressingModes, int startAddress) {
        System.out.println("Add labels to map of values:");
        Map<String, String> labelMap = new HashMap<String, String>();
        int addressCounter = startAddress;
        for (int lineIndex=0; lineIndex<lines.length; lineIndex++) {
            if (lineTypes[lineIndex]==LineType.INSTRUCTION) {
                addressCounter += Cpu6502InstructionDefs.getInstructionLength(addressingModes[lineIndex]);
            }
            if (lineTypes[lineIndex]==LineType.LABEL) {
                String labelName = lines[lineIndex].substring(0,lines[lineIndex].length()-1);
                System.out.print("Label: ");
                System.out.println(labelName);
                String labelAddressString = "$"+LibBaseConversion.toHexFixed(addressCounter, 4);
                System.out.print("Address: ");
                System.out.println(labelAddressString);
                labelMap.put(labelName, labelAddressString);
            }
        }
        return labelMap;
    }


    static Cpu6502.AddressingMode[] getAddressingModes(String[] lines, LineType[] lineTypes, Map<String, String> definitionMap) {
        System.out.println("Get addressing modes:");
        Cpu6502.AddressingMode[] addressingModes = new Cpu6502.AddressingMode[lines.length];
        System.out.println(lines.length);
        for (int lineIndex=0; lineIndex<lines.length; lineIndex++) {
            Cpu6502.AddressingMode currentAddressingMode = Cpu6502.AddressingMode.UNKNOWN;
            System.out.print("Line #");
            System.out.println(lineIndex);
            if (lineTypes[lineIndex]==LineType.INSTRUCTION) {
                currentAddressingMode = getAddressingModeFromLine(lines[lineIndex], definitionMap);
            }
            System.out.println(currentAddressingMode);
            addressingModes[lineIndex] = currentAddressingMode;
        }
        return addressingModes;
    }

    static Cpu6502.AddressingMode getAddressingModeFromLine(String line, Map<String, String> definitionMap) {
        System.out.println("CALL Get Addressing Mode");
        if (
            line.length()==3
        ) {
            if (Cpu6502InstructionDefs.getOpcode(line, Cpu6502.AddressingMode.ACCUMULATOR)!=-1) {
                return Cpu6502.AddressingMode.ACCUMULATOR;
            }
            if (Cpu6502InstructionDefs.getOpcode(line, Cpu6502.AddressingMode.IMPLIED)!=-1) {
                return Cpu6502.AddressingMode.IMPLIED;
            }
        }
        if (Cpu6502InstructionDefs.getOpcode(line.substring(0,3), Cpu6502.AddressingMode.RELATIVE)!=-1) {
            return Cpu6502.AddressingMode.RELATIVE;
        }
        if (line.contains("#")) {
            return Cpu6502.AddressingMode.IMMEDIATE;
        }
        if (line.contains("(") &&
            (!line.contains(","))
        ) {
            return Cpu6502.AddressingMode.INDIRECT;
        }
        if (line.contains("(") &&
            (line.contains(",")) &&
            line.substring(line.indexOf(',')+1).trim().toUpperCase().charAt(0)=='X'
        ) {
            return Cpu6502.AddressingMode.INDEXED_INDIRECT;
        }
        if (line.contains("(") &&
            (line.contains(",")) &&
            line.substring(line.indexOf(',')+1).trim().toUpperCase().charAt(0)=='Y'
        ) {
            return Cpu6502.AddressingMode.INDIRECT_INDEXED;
        }
        //
        String operandString = getOperand(line);

        String operandValueString = operandString;
        if (Character.isLetter(operandString.charAt(0))) {
            operandValueString = replaceOperandDefinitionOnly(operandString, definitionMap); //problem line
        }
        System.out.println(operandValueString);
        int operandValue = LibBaseConversion.getAssemblyNumberFromText(operandValueString);
        //
        System.out.println(line);
        if (operandValue>255||operandString.length()>3) {
            if (!line.contains(",")) {
                return Cpu6502.AddressingMode.ABSOLUTE;
            }
            if (
                (line.contains(",")) &&
                line.substring(line.indexOf(',')+1).trim().toUpperCase().charAt(0)=='X'
            ) {
                return Cpu6502.AddressingMode.ABSOLUTE_INDEXED_X;
            }
            if (
                (line.contains(",")) &&
                line.substring(line.indexOf(',')+1).trim().toUpperCase().charAt(0)=='Y'
            ) {
                return Cpu6502.AddressingMode.ABSOLUTE_INDEXED_Y;
            }
        }
        else {
            if (!line.contains(",")) {
                return Cpu6502.AddressingMode.ZERO_PAGE;
            }
            if (
                (line.contains(",")) &&
                line.substring(line.indexOf(',')+1).trim().toUpperCase().charAt(0)=='X'
            ) {
                return Cpu6502.AddressingMode.ZERO_PAGE_INDEXED_X;
            }
            if (
                (line.contains(",")) &&
                line.substring(line.indexOf(',')+1).trim().toUpperCase().charAt(0)=='Y'
            ) {
                return Cpu6502.AddressingMode.ZERO_PAGE_INDEXED_Y;
            }
        }
        return Cpu6502.AddressingMode.UNKNOWN;
    }
    static String getOperand(String line) {
        String operandString = line.substring(3).trim();
        if (operandString.contains(" ")) operandString = operandString.substring(0,operandString.indexOf(' '));
        if (operandString.contains(",")) operandString = operandString.substring(0,operandString.indexOf(','));
        operandString = operandString.replace('(',' ').replace(')',' ').replace('#',' ').trim();
        System.out.print("Line: ");
        System.out.println(line);
        System.out.print("Operand: ");
        System.out.println(operandString);

        return operandString;
    }
    static String replaceOperandDefinitionOnly(String operandString, Map<String, String> definitionMap) {
        if (Character.isLetter(operandString.charAt(0))) {
            try {
                String valueString  = definitionMap.get(operandString);
                if (valueString!=null) return valueString;
                return "$FFFF";
            }
            catch (NullPointerException e) {
                return "$FFFF";
            }
        }
        return operandString;
    }
    static String replaceOperandDefinitionAndLabel(
        String operandString, 
        Map<String, String> definitionMap, 
        Map<String, String> labelAddressMap) {
        if (Character.isLetter(operandString.charAt(0))) {
            try {
                String valueString  = definitionMap.get(operandString);
                if (valueString!=null) return valueString;
                valueString  = labelAddressMap.get(operandString);
                if (valueString!=null) return valueString;
                return "$FFFF";
            }
            catch (NullPointerException e) {
                return "$FFFF";
            }
        }
        return operandString;
    }
}
