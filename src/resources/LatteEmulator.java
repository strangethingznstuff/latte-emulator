/*
** Latte 6502 Emulator
** Date Started: 1/20/2023
*/

package resources;

//Imports
import javax.swing.*;

import resources.frontend.EmulatorMenuState;
import resources.frontend.LatteMenu;
import resources.frontend.UserInputHandler;
import resources.systems.EmulatorSystem;
import resources.systems.system_easy6502.Easy6502System;

public class LatteEmulator implements Runnable
{
   //Runtime
   Thread thread = new Thread(this);
   public boolean running = true;
   public int frameDeltaMilliseconds = 17;

   //Graphics
   public JFrame window;
   int mainWindowResX = 640;
   int mainWindowResY = 480;
   double mainWindowAspectRatio = (double) mainWindowResX / mainWindowResY;
   public EmulatorMenuState menuState;
   public LatteMenu menu;
   
   //Input
   public UserInputHandler userInputHandler;
      
   //System
   public EmulatorSystem emulatorSystem;


   public LatteEmulator() {
      
   }

   public void init() {
      //Create System
      emulatorSystem = new Easy6502System(this);
      emulatorSystem.restartSystem();
   }
   public void start() {
      running = true;
      thread.start();
      
   }
   public void destroy() {
      running = false;
   }
   public void stop() {
      running = false;
   }
   public void run() {
      while(running) {
      try {
         userInputHandler.update(this);
         update();
         //window.show(); //change to setVisible(boolean)?
         window.setVisible(true);
         Thread.sleep(frameDeltaMilliseconds); //17ms = Roughly 60fps
      }
      catch(InterruptedException e) {
         System.out.print("oops :/");
      }
         
      }
   }
   public void update() {
      //Emulation
      emulatorSystem.update();

      //Graphics
      window.repaint();
      menu.update();
   }
   public void setMainWindowRes(int widthX, int heightY) {
      mainWindowResX = widthX;
      mainWindowResY = heightY;
      mainWindowAspectRatio = (double) mainWindowResX / mainWindowResY;
      window.setSize(widthX,heightY);
   }
}