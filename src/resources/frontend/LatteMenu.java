package resources.frontend;

public abstract class LatteMenu {
    abstract public void destroyMenu();
    abstract public void update();
}
