package resources.components;

public class ComponentDRAM {
    char[] ramData;

    public ComponentDRAM(int size) {
        ramData = new char[size];
    }

    public int readByte(int address) {
        return (int) ramData[address];
    }

    public void writeByte(int address, int data) {
        ramData[address] = (char) data;
    }
}