# Latte Emulator

Written by Nasario Cruz (Soulite)

## Description

Latte Emulator is an emulation framework written in Java, and currently only supporting the 6502 CPU. Functionally, unless expanded upon in the future, it can just be conisdered as a 6502 CPU emulator.

The emulator is written entirely from scratch, with reference to documentation and knowledge from the creator's own experience writing 6502 assembly. While an attempt was made to make the external behavior of the CPU cycle accurate, with limited resources to test from the developer, there's no garuntee. However, on an instruction by instruction basis, Latte's implementation of the 6502 official instruction set is 99% compatible with real hardware behavior.

The interface was inspired by Nick Morgan's wonderful Easy6502. Please do note that only the user interface, not any of the emulation itself, is based off of Easy6502. The emulator itself is written completely from scratch.

## Running

Run App.java to initiate a GUI for writing and running 6502 assembly.

## Additional Notes / Attribution

Instruction compatibility and links to the resources referenced in this emulator's creation are in the docs folder.
